#ifndef PROFILE_H
#define PROFILE_H

#include <QObject>
#include <QString>
#include <QSettings>
#include <QApplication>
#include <QStringList>

class ProfileManager : public QObject
{
    Q_OBJECT

    Q_ENUMS(CampaignProgress)
    Q_ENUMS(CatfightDifficulty)

    Q_PROPERTY(QString profileName READ profileName WRITE loadProfile NOTIFY profileChanged)
    Q_PROPERTY(QStringList profileList READ profileList NOTIFY profileListChanged)
    Q_PROPERTY(qreal musicVolume READ musicVolume WRITE setMusicVolume NOTIFY musicVolumeChanged)
    Q_PROPERTY(bool musicEnabled READ musicEnabled WRITE setMusicEnabled NOTIFY musicEnabledChanged)
    Q_PROPERTY(qreal effectsVolume READ effectsVolume WRITE setEffectsVolume NOTIFY effectsVolumeChanged)
    Q_PROPERTY(bool effectsEnabled READ effectsEnabled WRITE setEffectsEnabled NOTIFY effectsEnabledChanged)
    Q_PROPERTY(qreal voiceVolume READ voiceVolume WRITE setVoiceVolume NOTIFY voiceVolumeChanged)
    Q_PROPERTY(bool voiceEnabled READ voiceEnabled WRITE setVoiceEnabled NOTIFY voiceEnabledChanged)
    Q_PROPERTY(int easyWins READ easyWins WRITE addEasyWin NOTIFY easyWinsChanged)
    Q_PROPERTY(int easyLosses READ easyLosses WRITE addEasyLoss NOTIFY easyLossesChanged)
    Q_PROPERTY(int mediumWins READ mediumWins WRITE addMediumWin NOTIFY mediumWinsChanged)
    Q_PROPERTY(int mediumLosses READ mediumLosses WRITE addMediumLoss NOTIFY mediumLossesChanged)
    Q_PROPERTY(int hardWins READ hardWins WRITE addHardWin NOTIFY hardWinsChanged)
    Q_PROPERTY(int hardLosses READ hardLosses WRITE addHardLoss NOTIFY hardLossesChanged)
    Q_PROPERTY(int networkWins READ networkWins WRITE addNetworkWin NOTIFY networkWinsChanged)
    Q_PROPERTY(int networkLosses READ networkLosses WRITE addNetworkLoss NOTIFY networkLossesChanged)
    Q_PROPERTY(int campaignLevel READ campaignLevel WRITE setCampaignLevel NOTIFY campaignLevelChanged)
    Q_PROPERTY(int catfightDifficulty READ catfightDifficulty WRITE setCatfightDifficulty NOTIFY catfightDifficultyChanged)
    
public:
    enum CampaignProgress { FirstLevel, SecondLevel, ThirdLevel, CampaignComplete };
    enum CatfightDifficulty { EasyCat, MediumCat, HardCat };

    /**
     * @brief Retrieve the profile instance
     * @return A pointer to the profile instance
     */
    static ProfileManager *instance();

signals:
    void newProfileCreated();
    void profileChanged(QString);
    void profileListChanged(QStringList);
    void musicVolumeChanged(qreal);
    void musicEnabledChanged(bool);
    void effectsVolumeChanged(qreal);
    void effectsEnabledChanged(bool);
    void voiceVolumeChanged(qreal);
    void voiceEnabledChanged(bool);
    void easyWinsChanged(int);
    void easyLossesChanged(int);
    void mediumWinsChanged(int);
    void mediumLossesChanged(int);
    void hardWinsChanged(int);
    void hardLossesChanged(int);
    void networkWinsChanged(int);
    void networkLossesChanged(int);
    void campaignLevelChanged(int);
    void catfightDifficultyChanged(int);

public slots:
    /**
     * @brief Get the current profile name
     * @return The name of the currently loaded profile; QString::null if no profile is selected.
     */
    QString profileName();
    /**
     * @brief Select a different profile
     * @param ProfileName The name of the profile to load
     */
    void loadProfile(const QString& profileName);

    void deleteProfile(const QString& profileName);

    QStringList profileList();

    qreal musicVolume();
    void setMusicVolume(qreal volume);
    bool musicEnabled();
    void setMusicEnabled(bool enabled);
    
    qreal effectsVolume();
    void setEffectsVolume(qreal volume);
    bool effectsEnabled();
    void setEffectsEnabled(bool enabled);
    
    qreal voiceVolume();
    void setVoiceVolume(qreal volume);
    bool voiceEnabled();
    void setVoiceEnabled(bool enabled);

    int easyWins();
    void addEasyWin(int wins = 1);

    int easyLosses();
    void addEasyLoss(int losses = 1);

    int mediumWins();
    void addMediumWin(int wins = 1);

    int mediumLosses();
    void addMediumLoss(int losses = 1);

    int hardWins();
    void addHardWin(int wins = 1);

    int hardLosses();
    void addHardLoss(int losses = 1);

    int networkWins();
    void addNetworkWin(int wins = 1);

    int networkLosses();
    void addNetworkLoss(int losses = 1);

    int campaignLevel();
    void setCampaignLevel(int level);

    int catfightDifficulty();
    void setCatfightDifficulty(int difficulty);
    
private:
    QSettings settings;
    QString pProfileName;
    static ProfileManager *theProfile;
    
    const static QString strMusicVolume;
    const static QString strMusicEnabled;
    const static QString strEffectsVolume;
    const static QString strEffectsEnabled;
    const static QString strVoiceVolume;
    const static QString strVoiceEnabled;
    const static QString strNewProfile;
    const static QString strEasyWins;
    const static QString strEasyLosses;
    const static QString strMediumWins;
    const static QString strMediumLosses;
    const static QString strHardWins;
    const static QString strHardLosses;
    const static QString strNetworkWins;
    const static QString strNetworkLosses;
    const static QString strCampaignLevel;
    const static QString strCatfightDifficulty;
    
    const static QString PROFILE_GROUP;

    void leaveAllGroups();
    void createNewProfile(const QString &profileName);
    ProfileManager();
    virtual ~ProfileManager();
};

#endif // PROFILE_H
