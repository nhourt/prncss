#include <QtTest/QtTest>
#include <board.h>

class TestBoard : public QObject {
    Q_OBJECT
private slots:
    void Comparisons();
    void Moves();
    void Rotation();
    void FindMoves();
    void BoardPositions();
    void FindMovesBench();
    void LockedHomeRow();
};

void TestBoard::Comparisons()
{
    Board b1;
    Board b2;
    
    QVERIFY(b1 == b2);
    
    BoardPosition bp(NorthSide);
    b1[bp] = SinglePiece;
    
    QVERIFY(b1 != b2);
    
    b2[bp] = DoublePiece;
    
    QVERIFY(b1 != b2);
    
    b2[bp] = SinglePiece;
    
    QVERIFY(b1 == b2);
}

void TestBoard::Moves()
{
    Board b;
    BoardPosition bp(QString("A1"));
    Move m;
    
    m.SetStartPosition(bp);
    
    QCOMPARE(b.CanMakeMove(SouthSide, m), BMRMissingStartPiece);
    
    b[bp] = TriplePiece;
    m.AddDirection(Up);
    m.AddDirection(Up);
    m.AddDirection(Left);
    
    QCOMPARE(b.CanMakeMove(SouthSide, m), BMRMoveOffBoard);
    
    m.DeleteAllDirections();
    
    QCOMPARE(m.GetDirectionCount(), 0);
    
    m.AddDirection(Up);
    m.AddDirection(Right);
    m.AddDirection(Down);
    m.AddDirection(Down);
    
    QCOMPARE(b.CanMakeMove(SouthSide, m), BMRTooManyDirections);
    
    bp.MovePosition(Right);
    b[bp] = SinglePiece;
    
    QCOMPARE(b.CanMakeMove(SouthSide, m), BMRMoveOffBoard);
    QCOMPARE(b.CanMakeMove(NorthSide, m), BMRValidMove);
    Board winBoard(b);
    winBoard.ApplyMove(NorthSide, m);
    QCOMPARE(winBoard.GameState(), NorthWin);
    
    m.DeleteLastDirection();
    m.AddReplacement(QString("C4"));
    
    QCOMPARE(b.CanMakeMove(SouthSide, m), BMRReplaceOutOfBounds);
    QCOMPARE(b.CanMakeMove(NorthSide, m), BMRValidMove);
    
    b[QString("C1")] = SinglePiece;
    m.Clear();
    m.SetStartPosition(bp);
    m.AddDirection(Right);
    QCOMPARE(b.CanMakeMove(SouthSide, m), BMRIncompleteMove);
}

void TestBoard::Rotation()
{
    Board b;
    
    b[NorthSide] = SinglePiece;
    b[SouthSide] = TriplePiece;
    b[QString("A1")] = DoublePiece;
    b[QString("B1")] = DoublePiece;
    b[QString("C1")] = DoublePiece;
    b[QString("D1")] = DoublePiece;
    b[QString("E1")] = DoublePiece;
    b[QString("F1")] = DoublePiece;
    b[QString("B2")] = SinglePiece;
    b[QString("C3")] = TriplePiece;
    b[QString("D4")] = SinglePiece;
    b[QString("E5")] = DoublePiece;
    b[QString("F6")] = SinglePiece;
    b.Rotate();
    
    QCOMPARE(b[NorthSide], TriplePiece);
    QCOMPARE(b[SouthSide], SinglePiece);
    QCOMPARE(b[QString("F6")], DoublePiece);
    QCOMPARE(b[QString("E6")], DoublePiece);
    QCOMPARE(b[QString("D6")], DoublePiece);
    QCOMPARE(b[QString("C6")], DoublePiece);
    QCOMPARE(b[QString("B6")], DoublePiece);
    QCOMPARE(b[QString("E5")], SinglePiece);
    QCOMPARE(b[QString("D4")], TriplePiece);
    QCOMPARE(b[QString("C3")], SinglePiece);
    QCOMPARE(b[QString("B2")], DoublePiece);
    QCOMPARE(b[QString("A1")], SinglePiece);
    
    b.Rotate();
    
    QCOMPARE(b[NorthSide], SinglePiece);
    QCOMPARE(b[SouthSide], TriplePiece);
    QCOMPARE(b[QString("A1")], DoublePiece);
    QCOMPARE(b[QString("B1")], DoublePiece);
    QCOMPARE(b[QString("C1")], DoublePiece);
    QCOMPARE(b[QString("D1")], DoublePiece);
    QCOMPARE(b[QString("E1")], DoublePiece);
    QCOMPARE(b[QString("F1")], DoublePiece);
    QCOMPARE(b[QString("B2")], SinglePiece);
    QCOMPARE(b[QString("C3")], TriplePiece);
    QCOMPARE(b[QString("D4")], SinglePiece);
    QCOMPARE(b[QString("E5")], DoublePiece);
    QCOMPARE(b[QString("F6")], SinglePiece);
}

void TestBoard::FindMoves()
{
    Board b;
    BoardPosition p1 = QString("A1");
    BoardPosition p2 = QString("D1");
    
    b[p1] = TriplePiece;
    b[p2] = DoublePiece;
    QList<Move> moves = b.FindAllMoves(p1);
    
    QCOMPARE(moves.size(), 15);
    
    Move m (p1);
    m.AddDirection(Right);
    m.AddDirection(Right);
    m.AddDirection(Right);
    moves = b.FindMoves(p1, p2);
    
    QVERIFY(moves.length() == 1);
    QCOMPARE(m, moves.first());
    QCOMPARE(b.FindMoves(p1, QString("B3")).length(), 3);
    
    b[QString("A4")] = TriplePiece;
    qDebug() << b;
    
    QCOMPARE(b.FindMoves(p1, NorthSide).size(), 1);
}

void TestBoard::BoardPositions()
{
    Board b;
    const BoardPosition p = QString("C2");
    b[QString("C2")] = TriplePiece;
    b[QString("B2")] = DoublePiece;
    b[QString("C1")] = SinglePiece;
    
    QCOMPARE(b[p], TriplePiece);
    QCOMPARE(b[p.PeekDirection(Left)], DoublePiece);
    QCOMPARE(b[p.PeekDirection(Down)], SinglePiece);
    QCOMPARE(b[p.PeekDirection(Up)], NoPiece);
    
    b[p.PeekDirection(Right)] = SinglePiece;
    
    QCOMPARE(b[p.PeekDirection(Right)], SinglePiece);
}

void TestBoard::FindMovesBench()
{
    Board b;
    
    b[QString("C1")] = TriplePiece;
    b[QString("C2")] = TriplePiece;
    b[QString("A6")] = TriplePiece;
    b[QString("C6")] = TriplePiece;
    b[QString("A5")] = SinglePiece;
    b[QString("A4")] = SinglePiece;
    b[QString("C5")] = SinglePiece;
    b[QString("F4")] = SinglePiece;
    b[QString("C4")] = DoublePiece;
    b[QString("D2")] = DoublePiece;
    b[QString("F2")] = DoublePiece;
    b[QString("E4")] = DoublePiece;
    
    QList<Move> moves;
    QBENCHMARK(moves = b.FindAllMoves(QString("C1")));
    QCOMPARE(moves.size(), 104);
}

void TestBoard::LockedHomeRow()
{
    Board b;
    b[tr("A1")] = TriplePiece;
    b[tr("C1")] = TriplePiece;
    b[tr("E1")] = TriplePiece;
    b[tr("A2")] = SinglePiece;
    b[tr("B2")] = SinglePiece;
    b[tr("C2")] = SinglePiece;
    b[tr("D2")] = SinglePiece;
    b[tr("E2")] = DoublePiece;
    b[tr("F2")] = DoublePiece;
    
    QCOMPARE(b.FindHomeRow(SouthSide), short(2));
    QCOMPARE(b.FindHomeRow(NorthSide), short(2));
    
    b[tr("F5")] = DoublePiece;
    
    QCOMPARE(b.FindHomeRow(NorthSide), short(5));
}

QTEST_MAIN(TestBoard)
#include <testboard.moc>