#include <QtTest/QtTest>
#include <move.h>

class TestMove : public QObject {
    Q_OBJECT
private slots:
    void Serialize();
    void Deserialize();
};

void TestMove::Serialize()
{
    Move m;
    
    m.SetStartPosition(tr("A1"));
    m.AddDirection(Up);
    m.AddDirection(Right);
    m.AddDirection(Right);
    
    QCOMPARE(m.Serialize(), tr("A1-NEE"));
    
    m.AddReplacement(tr("C5"));
    
    QCOMPARE(m.Serialize(), tr("A1-NEE-C5"));
    
    m.DeleteReplacement();
    m.AddDirection(Up);
    m.AddDirection(Up);
    m.AddDirection(Up);
    m.AddDirection(Up);
    m.AddDirection(Up);
    
    QCOMPARE(m.Serialize(), tr("A1-NEENNNNN"));
    
    m.AddDirection(Down);
    m.AddDirection(Down);
    m.AddDirection(Down);
    m.AddDirection(Down);
    m.AddDirection(Down);
    m.AddDirection(Down);
    m.AddDirection(Down);
    
    QCOMPARE(m.Serialize(), tr("A1-NEENNNNNSSSSSSS"));
    
    m.DeleteAllDirections();
    m.AddDirection(Up);
    m.AddDirection(Down);
    m.AddDirection(Left);
    m.AddDirection(Right);
    m.AddReplacement(tr("F6"));
    m.AddDirection(Left);
    
    QCOMPARE(m.Serialize(), tr("A1-NSWEW-F6"));
}

void TestMove::Deserialize()
{
    Move n = Move::Deserialize(tr("A1-NSWEW-F6"));
    Move m;
    m.SetStartPosition(tr("A1"));
    m.AddDirection(Up);
    m.AddDirection(Down);
    m.AddDirection(Left);
    m.AddDirection(Right);
    m.AddReplacement(tr("F6"));
    m.AddDirection(Left);
    
    QCOMPARE(n,m);
}

QTEST_MAIN(TestMove)
#include <testmove.moc>