#include "profilemanager.h"

#include <QtDebug>

const qreal DEFAULT_VOLUME = 0.5;

ProfileManager* ProfileManager::instance()
{
    if (theProfile == NULL)
        theProfile = new ProfileManager;
    
    return theProfile;
}

QString ProfileManager::profileName()
{
    if (theProfile == NULL)
        return QString::null;
    return theProfile->pProfileName;
}

void ProfileManager::loadProfile(const QString &profileName)
{
    leaveAllGroups();
    
    qDebug() << __FILE__ << __LINE__ << "Now loading profile" << profileName;

    pProfileName = profileName;
    settings.beginGroup(PROFILE_GROUP);
    settings.beginGroup(profileName);

    if (settings.value(strNewProfile, true).toBool())
        createNewProfile(profileName);

    emit profileChanged(profileName);
}

void ProfileManager::deleteProfile(const QString &profileName)
{
    leaveAllGroups();
    settings.beginGroup(PROFILE_GROUP);
    settings.beginGroup(profileName);
    settings.remove("");
    settings.endGroup();
    settings.sync();
    emit profileListChanged(settings.childGroups());
}

QStringList ProfileManager::profileList()
{
    leaveAllGroups();
    settings.beginGroup(PROFILE_GROUP);

    qDebug() << __FILE__ << __LINE__ << "Getting profile list:" << settings.childGroups();

    const QStringList allProfiles = settings.childGroups();
    settings.beginGroup(pProfileName);
    return allProfiles;
}

qreal ProfileManager::musicVolume()
{
    return settings.value(strMusicVolume, DEFAULT_VOLUME).toReal();
}

void ProfileManager::setMusicVolume (qreal volume)
{
    settings.setValue(strMusicVolume, volume);
    emit musicVolumeChanged(volume);
}

bool ProfileManager::musicEnabled()
{
    return settings.value(strMusicEnabled, true).toBool();
}

void ProfileManager::setMusicEnabled (bool enabled)
{
    settings.setValue(strMusicEnabled, enabled);
    emit musicEnabledChanged(enabled);
}

qreal ProfileManager::effectsVolume()
{
    return settings.value(strEffectsVolume, DEFAULT_VOLUME).toReal();
}

void ProfileManager::setEffectsVolume (qreal volume)
{
    settings.setValue(strEffectsVolume, volume);
    emit effectsVolumeChanged(volume);
}

bool ProfileManager::effectsEnabled()
{
    return settings.value(strEffectsEnabled, true).toBool();
}

void ProfileManager::setEffectsEnabled (bool enabled)
{
    settings.setValue(strEffectsEnabled, enabled);
    emit effectsEnabledChanged(enabled);
}

qreal ProfileManager::voiceVolume()
{
    return settings.value(strVoiceVolume, DEFAULT_VOLUME).toReal();
}

void ProfileManager::setVoiceVolume (qreal volume)
{
    settings.setValue(strVoiceVolume, volume);
    emit voiceVolumeChanged(volume);
}

bool ProfileManager::voiceEnabled()
{
    return settings.value(strVoiceEnabled, true).toBool();
}

void ProfileManager::setVoiceEnabled (bool enabled)
{
    settings.setValue(strVoiceEnabled, enabled);
    emit voiceEnabledChanged(enabled);
}

int ProfileManager::easyWins()
{
    return settings.value(strEasyWins, 0).toInt();
}

void ProfileManager::addEasyWin(int wins)
{
    wins += easyWins();
    settings.setValue(strEasyWins, wins);
    emit easyWinsChanged(wins);
}

int ProfileManager::easyLosses()
{
    return settings.value(strEasyLosses, 0).toInt();
}

void ProfileManager::addEasyLoss(int losses)
{
    losses += easyLosses();
    settings.setValue(strEasyLosses, losses);
    emit easyLossesChanged(losses);
}

int ProfileManager::mediumWins()
{
    return settings.value(strMediumWins, 0).toInt();
}

void ProfileManager::addMediumWin(int wins)
{
    wins += mediumWins();
    settings.setValue(strMediumWins, wins);
    emit mediumWinsChanged(wins);
}

int ProfileManager::mediumLosses()
{
    return settings.value(strMediumLosses, 0).toInt();
}

void ProfileManager::addMediumLoss(int losses)
{
    losses += mediumLosses();
    settings.setValue(strMediumLosses, losses);
    emit mediumLossesChanged(losses);
}

int ProfileManager::hardWins()
{
    return settings.value(strHardWins, 0).toInt();
}

void ProfileManager::addHardWin(int wins)
{
    wins += hardWins();
    settings.setValue(strHardWins, wins);
    emit hardWinsChanged(wins);
}

int ProfileManager::hardLosses()
{
    return settings.value(strHardLosses, 0).toInt();
}

void ProfileManager::addHardLoss(int losses)
{
    losses += hardLosses();
    settings.setValue(strHardLosses, losses);
    emit hardLossesChanged(losses);
}

int ProfileManager::networkWins()
{
    return settings.value(strNetworkWins, 0).toInt();
}

void ProfileManager::addNetworkWin(int wins)
{
    wins += networkWins();
    settings.setValue(strNetworkWins, wins);
    emit networkWinsChanged(wins);
}

int ProfileManager::networkLosses()
{
    return settings.value(strNetworkLosses, 0).toInt();
}

void ProfileManager::addNetworkLoss(int losses)
{
    losses += networkLosses();
    settings.setValue(strNetworkLosses, losses);
    emit networkLossesChanged(losses);
}

int ProfileManager::campaignLevel()
{
    return settings.value(strCampaignLevel, FirstLevel).toInt();
}

void ProfileManager::setCampaignLevel(int level)
{
    settings.setValue(strCampaignLevel, level);
    emit campaignLevelChanged(CampaignProgress(level));
}

int ProfileManager::catfightDifficulty()
{
    return settings.value(strCatfightDifficulty, EasyCat).toInt();
}

void ProfileManager::setCatfightDifficulty(int difficulty)
{
    settings.setValue(strCatfightDifficulty, difficulty);
    emit catfightDifficultyChanged(difficulty);
}

void ProfileManager::leaveAllGroups()
{
    while (!settings.group().isEmpty())
        settings.endGroup();
}

void ProfileManager::createNewProfile(const QString &profileName)
{
    leaveAllGroups();
    settings.beginGroup(PROFILE_GROUP);
    settings.beginGroup(profileName);

    setMusicVolume(DEFAULT_VOLUME);
    setMusicEnabled(true);
    setEffectsVolume(DEFAULT_VOLUME);
    setEffectsEnabled(true);
    setVoiceVolume(DEFAULT_VOLUME);
    setVoiceEnabled(true);
    setCatfightDifficulty(EasyCat);

    settings.setValue(strNewProfile, false);

    settings.endGroup();
    emit profileListChanged(settings.childGroups());
    emit newProfileCreated();
}

ProfileManager::ProfileManager() :
    settings(QSettings::IniFormat,
             QSettings::UserScope,
             QApplication::instance()->organizationName(),
             QApplication::instance()->applicationName())
{
    connect(QApplication::instance(), SIGNAL(aboutToQuit()), this, SLOT(deleteLater()));
}

ProfileManager::~ProfileManager()
{
    qDebug() << __FILE__ << __LINE__ << "Destroying ProfileManager";
    theProfile = NULL;
    settings.sync();
}

const QString ProfileManager::strMusicVolume = "musicVolume";
const QString ProfileManager::strMusicEnabled = "musicEnabled";
const QString ProfileManager::strEffectsVolume = "effectsVolume";
const QString ProfileManager::strEffectsEnabled = "effectsEnabled";
const QString ProfileManager::strVoiceVolume = "voiceVolume";
const QString ProfileManager::strVoiceEnabled = "voiceEnabled";
const QString ProfileManager::strNewProfile = "newProfile";
const QString ProfileManager::strEasyWins = "easyWins";
const QString ProfileManager::strEasyLosses = "easyLosses";
const QString ProfileManager::strMediumWins = "mediumWins";
const QString ProfileManager::strMediumLosses = "mediumLosses";
const QString ProfileManager::strHardWins = "hardWins";
const QString ProfileManager::strHardLosses = "hardLosses";
const QString ProfileManager::strNetworkWins = "networkWins";
const QString ProfileManager::strNetworkLosses = "networkLosses";
const QString ProfileManager::strCampaignLevel = "campaignLevel";
const QString ProfileManager::strCatfightDifficulty = "catfightDifficulty";
const QString ProfileManager::PROFILE_GROUP = "profiles";

ProfileManager *ProfileManager::theProfile = NULL;

#include "moc_profilemanager.cpp"
