#ifndef TUTORIALMANAGER_H
#define TUTORIALMANAGER_H
#include "boardmanager.h"
#include <QVector>

class TutorialManager : public BoardManager
{
    Q_OBJECT
public:
    explicit TutorialManager(QObject *gameBoard, QObject *parent = 0);
    ~TutorialManager();

signals:
    
public slots:
    void playTutorialGame();
    void CancelMove(bool getMove = true){}
    void Quit();

private:    
    struct DialogElement
    {
        QString text;
        QString soundUrl;

        DialogElement(QString text = "", QString soundUrl = "") : text(text), soundUrl(soundUrl) {}
    };
    void UIPositionClicked(QString);
    void highlightPosition(BoardPosition);
    void quickHighlightPosition(BoardPosition, int duration);
    void quickHighlightPositions(QVector<BoardPosition> positions, int duration);
    void setAllMouseActive();
    void WaitForSpeech(DialogElement content);
    void SpeechWithoutWait(DialogElement content);

    bool quitRequest;
    int minTextDisplayTime;
};

#endif // TUTORIALMANAGER_H
