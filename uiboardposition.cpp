#include "uiboardposition.h"

#include <QDebug>
#include <QPainter>
#include <QGraphicsEffect>
#include <QGraphicsColorizeEffect>
#include <QGraphicsBlurEffect>
#include <QPropertyAnimation>
#include <QGraphicsSceneMouseEvent>
#include <QRadialGradient>

UIBoardPosition::UIBoardPosition(QGraphicsItem *parent) : hover(false), QGraphicsRectItem(parent)
{
    init();
}

UIBoardPosition::UIBoardPosition(BoardPosition pos, QGraphicsItem *parent) : name(pos), pos(pos), piece(NoPiece), hover(false), clicked(false), QGraphicsRectItem(parent)
{
    init();
}

UIBoardPosition::UIBoardPosition(BoardPosition pos, GamePiece piece, QGraphicsItem *parent) : name(pos), pos(pos), piece(piece), hover(false), clicked(false), QGraphicsRectItem(parent)
{
    init();
}

void UIBoardPosition::init()
{
    setAcceptsHoverEvents(true);
}

UIBoardPosition::~UIBoardPosition()
{}

void UIBoardPosition::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    clicked = true;
}

void UIBoardPosition::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    hover = false;
    if(clicked && boundingRect().contains(event->scenePos()))
    {
        // qDebug() << "clicked on" << name;
        emit wasClicked(pos);
    }
    clicked = false;
}

void UIBoardPosition::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    // qDebug() << "mouseMoveEvent on" << name;

    // check if the mouse is over this
    bool prev = hover;
    hover = boundingRect().contains(event->scenePos());

    if(prev != hover)
        update(rect());
}

void UIBoardPosition::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    // qDebug() << "hoverEnterEvent on" << name;

    hover = true;
    update(rect());
}

void UIBoardPosition::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    // qDebug() << "hoverLeaveEvent on" << name;

    hover = false;
    update(rect());
}

QVector<QColor> UIBoardPosition::getColors()
{
    QVector<QColor> colors;
    QColor color;
    switch(piece)
    {
    case NoPiece:
        color = Qt::transparent;
        break;
    case SinglePiece:
        color = Qt::lightGray;
        break;
    case DoublePiece:
        color = Qt::gray;
        break;
    case TriplePiece:
        color = Qt::darkGray;
        break;
    }

    colors.push_back(color);

    if(hover)
        colors.push_back(Qt::cyan);
    else
        colors.push_back(color);

    return colors;
}

void UIBoardPosition::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    // qDebug() << "paint() called for" << name;
    // fun stuff that isn't working...
    /*
    QVector<QColor> colors = getColors();
    QRadialGradient gradient(rect().center(), .001);
    gradient.setColorAt(0, colors[0]);
    gradient.setColorAt(1, colors[1]);
    QBrush brush(gradient);

    painter->setPen(QPen(Qt::RoundJoin));

    painter->setBrush(brush);
    */
    painter->setBrush(getColors()[1]);
    painter->drawRect(rect());

    // trying to apply a blur effect, but it likes to seg-fault
    /*
    if(hover)
    {
        QGraphicsBlurEffect* effect = new QGraphicsBlurEffect();
        effect->setBlurRadius(5);
        setGraphicsEffect(effect);
    }
    else
    {
        setGraphicsEffect(NULL);
    }
    */

    QGraphicsRectItem::paint(painter, option, widget);
}

