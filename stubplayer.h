#ifndef STUBPLAYER_H
#define STUBPLAYER_H
#include "board.h"
#include "player.h"

class StubPlayer : public Player
{
public:
    StubPlayer () {}

    virtual void SetSide(PlayerSide side) {this->side = side;}
    virtual HomeRow ChooseStartingPositions (const Board& board);
    ///This method does nothing in StubPlayer
    virtual void InformOfStartingPositions (const HomeRow& homeRow){}
    ///This method does nothing in StubPlayer
    virtual void AcknowledgeOpponentMove (const Move& opponentMove){}
    virtual Move MakeMove (const Board& board);

private:

};

#endif // STUBPLAYER_H
