#ifndef BOARD_H
#define BOARD_H
#include "move.h"
#include "types.h"

#include <map>
#include <vector>
#include <sstream>
#include <QDebug>

class Board : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Default ctor
     */
    Board();
    /**
     * @brief Copy ctor
     */
    Board (const Board& other);

    bool operator== (const Board& other) const;
    bool operator!= (const Board& other) const;
    Board& operator= (const Board& other);
    
    operator QString() const {
        QString result;
        QTextStream stream (&result);
        stream << northGoal;
        for (int i = 0; i < BOARDSIZE; ++i)
            for (int j = 0; j < BOARDSIZE; ++j)
                stream << board[i][j];
        stream << southGoal;
        return result;
    }

    /**
     * @brief Determine if the given move is valid on this board, without actually applying it
     * @param player The side of the player who is trying to make this move (either N for north or S for south)
     * @param move The move to validate
     * @return Either BMRValidMove or the reason the move was invalid
     */
    BadMoveReason CanMakeMove (PlayerSide player, const Move& move) const;
    /**
     * @brief Apply a move to the board
     * @param player The side of the player who is trying to make this move (either N for north or S for south)
     * @param move The move to apply to the board
     * @return Either BMRValidMove or the reason the move was invalid
     */
    BadMoveReason ApplyMove (PlayerSide player, const Move& move);

    /**
     * @brief Retrieves the piece at the given position
     * @param position The position on the board to query
     * @return A read-write value of the piece at supplied location
     */
    GamePiece& operator[] (const BoardPosition& position);
    /**
     * @brief Retrieves the piece at the given position
     * @param position The position on the board to query
     * @return A write-only value of the piece at supplied location
     */
    const GamePiece& operator[] (const BoardPosition& position) const;
    /**
     * @brief Retrieves the piece at the given position
     * @param position The position on the board to query
     * @return A read-write value of the piece at supplied location
     */
    GamePiece& At (const BoardPosition& position) {
        return (*this)[position];
    }
    /**
     * @brief Retrieves the piece at the given position
     * @param position The position on the board to query
     * @return A write-only value of the piece at supplied location
     */
    const GamePiece& At (const BoardPosition& position) const {
        return (*this)[position];
    }
    
    /**
     * @brief See the current game state
     * @return The game state
     */
    GameOverState GameState();
    
    /**
     * @brief Find a valid move from one position to another
     * @param begin The starting position of the move
     * @param end The ending position of the move
     * @return All possible moves from begin to end
     */
    QList< Move > FindMoves (BoardPosition begin, BoardPosition end) const;
    /**
     * @brief Find all moves from the given position
     * @param startPos The position to start searching from
     * @return A list of all the moves from the given position
     */
    QList< Move > FindAllMoves (BoardPosition startPos) const;
    /**
     * @brief Determine if this piece can move anywhere
     * Moves into goals are ignored.
     * @param startPos Position of the piece to check
     * @return True if the piece can move anywhere; false if not
     */
    bool HasMove (const BoardPosition &startPos) const;
    
    /**
     * @brief Rotate this board 180 degrees
     * All pieces on the board will be moved to the opposite side, as though the board were rotated
     */
    void Rotate();

    /**
     * @brief Find the specified player's home row
     * @param player The side to find a home row for
     * @return The row which is the given player's home row
     */
    short FindHomeRow (PlayerSide player) const;
    /**
     * @brief Get the specified player's home row
     * @param player The side to get the home row for
     * @return The given player's home row
     */
    HomeRow GetHomeRow (PlayerSide player) const;

    void QuitGame();
    
private:
    struct SearchNode {
        SearchNode (const BoardPosition &pos, int movesRemaining) : move(pos), movesRemaining(movesRemaining), endpos(pos) {}
        
        Move move;
        int movesRemaining;
        BoardPosition endpos;
        QList<BreadCrumb> breadcrumbs;
    };
    
    GamePiece board[BOARDSIZE][BOARDSIZE];
    GamePiece northGoal;
    GamePiece southGoal;
    GameOverState gameState;

    short FindHomeRowIndex (PlayerSide player) const;
    bool OutOfBounds (BoardPosition position, bool includeGoalsInBounds = false) const;
};

QDebug operator<< (QDebug dbg, const Board& b);

#endif // BOARD_H
