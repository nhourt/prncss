#pragma once
#include<string>
#include<vector>
using namespace std;

struct Scylla {
public:
    struct ScyllaMove {

        ScyllaMove() {
            x1 = 0;
            x2 = 0;
            x3 = 0;
            win = 0;
            note = 0;
        }

        ScyllaMove (const ScyllaMove& o) {
            this->x1 = o.x1;
            this->x2 = o.x2;
            this->x3 = o.x3;
            this->win = o.win;
            this->note = o.note;
        }

        ScyllaMove (int i, int j, int k, int note = 0) {
            x1 = i;
            x2 = j;
            x3 = k;
            win = 0;
            this->note = note;
        }

        bool operator < (const ScyllaMove& o) const {
            if (note > o.note)
                return true;
            else if (note < o.note)
                return false;
            else if (x1 < o.x1)
                return true;
            else if (x1 > o.x1)
                return false;
            else if (x2 < o.x2)
                return true;
            else if (x2 > o.x2)
                return false;
            else if (x3 < o.x3)
                return true;
            else if (x3 > o.x3)
                return false;
            else return false;
        }

        //members
        int x1;
        int x2;
        int x3;
        int win;
        long note;
    };

    Scylla (Scylla* nextHead);
    Scylla();
    ~Scylla();
    Scylla::ScyllaMove chooseMove (vector<int> board, bool south, int lvl);
    Scylla::ScyllaMove evaluate();
    void simulate (int start, int end, int replace);
    void unsimulate (int start, int end, int replace);
    void buildMove (int startIndex, int endIndex);
    vector<int> getLiberty (int index, bool south);
    vector<int> getPlayables (bool south, int homeRowOffset = 0);
    int getFirstLine (bool south);
    void reset();
    bool isInBoard (int i);
    void search (int index, int moves, int incomingDirection);

private:
    string debugMsg;
    vector<int> board;
    bool h[67];
    bool v[67];
    bool south;
    int lvl;
    int lineOpp;
    vector<ScyllaMove> moves;
    vector<int> liberty;
    bool mySide;
    int saveCell;
    long note;
    int win;
    int nest;
    Scylla* innerMove;
    ScyllaMove move;
};

