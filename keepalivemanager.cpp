#include "keepalivemanager.h"

const short KeepAliveManager::TIMEOUT = 5;
const short KeepAliveManager::INTERVAL = 2;

KeepAliveManager::KeepAliveManager (QHostAddress peer, quint16 port, QObject *parent) :
    QObject(parent),
    peer(peer),
    port(port),
    connectionState(disconnected)
{
    qDebug() << __FILE__ << __LINE__ << "Creating keep-alive manager with peer" << peer << "on port" << port;
    
    receiveTimer.setInterval(TIMEOUT * 1000);
    sendTimer.setInterval(INTERVAL * 1000);
    connect(&receiveTimer, SIGNAL(timeout()), this, SLOT(Timeout()));
    connect(&sendTimer, SIGNAL(timeout()), this, SLOT(SendKeepAlive()));
    
    udpSocket = new QUdpSocket(this);
    
    connect(udpSocket, SIGNAL(readyRead()), this, SLOT(ReceivedDatagram()));
    udpSocket->bind(port, QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
}

void KeepAliveManager::Start()
{
    qDebug() << __FILE__ << __LINE__ << "Starting keep-alive manager";
    connectionState = connected;
    receiveTimer.start();
    sendTimer.start();
}

void KeepAliveManager::Stop()
{
    qDebug() << __FILE__ << __LINE__ << "Stopping keep-alive manager";
    connectionState = disconnected;
    receiveTimer.stop();
    sendTimer.stop();
}

void KeepAliveManager::SendKeepAlive()
{
    udpSocket->writeDatagram(KEEPALIVE, peer, port);
}

void KeepAliveManager::ReceivedDatagram()
{
    while (udpSocket->hasPendingDatagrams()) {
        QByteArray datagram (udpSocket->pendingDatagramSize(), ' ');
        QHostAddress sender;
        quint16 port;
        udpSocket->readDatagram(datagram.data(), datagram.size(), &sender, &port);
        
        if (datagram == KEEPALIVE && sender == peer) {

            if (connectionState == connected)
                receiveTimer.start();
            else {
                connectionState = connected;
                emit RecoveredConnection();
            }
        } else {
            qDebug() << __FILE__ << __LINE__ << "Received bogus UDP value" << datagram << "from" << sender;
        }
    }
}

void KeepAliveManager::Timeout()
{
    qDebug() << __FILE__ << __LINE__ << "Timed out waiting for keep-alive";
    connectionState = disconnected;
    receiveTimer.stop();
    emit LostConnection();
}

const QByteArray KeepAliveManager::KEEPALIVE = "KA";

#include "moc_keepalivemanager.cpp"
