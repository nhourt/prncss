#include "networkplayer.h"
#include <QInputDialog>

NetworkPlayer::NetworkPlayer (NetworkManager* networkManager, QObject* parent) : Player ( parent), networkManager (networkManager)
{
    connect (this, SIGNAL (SendMessage (QString)), networkManager, SLOT (SendGameMessage (QString)));
}

HomeRow NetworkPlayer::ChooseStartingPositions (const Board& board)
{
    qDebug() << __FILE__ << __LINE__ << "Waiting for network player on" << side << "to send starting row";

    HomeRow homeRow;

    //Store the new row
    homeRow = networkManager->GetGameMessage();

    if (homeRow == QString::null)
        AbortGame();
    
    qDebug() << __FILE__ << __LINE__ << "Got" << side << "starting row";
    return homeRow;
}

void NetworkPlayer::InformOfStartingPositions (const HomeRow& homeRow)
{
    QString homeRowString = homeRow;
    qDebug() << __FILE__ << __LINE__ << "Sending starting row" << homeRowString << "to" << side << "over network.";
    emit SendMessage (homeRowString);
}

void NetworkPlayer::AcknowledgeOpponentMove (const Move& opponentMove)
{
    emit SendMessage (opponentMove.Serialize());
}

Move NetworkPlayer::MakeMove (const Board& board)
{
    Move move;
    QString moveString = networkManager->GetGameMessage();

    if (moveString == QString::null)
        AbortGame();

    move = Move::Deserialize (moveString);


    return move;
}

#include "moc_networkplayer.cpp"
