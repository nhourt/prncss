#ifndef HUMANPLAYER_H
#define HUMANPLAYER_H

#include "player.h"

class HumanPlayer : public Player
{
    Q_OBJECT
public:
    HumanPlayer(QObject *parent = 0);

    virtual HomeRow ChooseStartingPositions(const Board &board);
    ///This method does nothing in HumanPlayer
    virtual void AcknowledgeOpponentMove (const Move& opponentMove);
    virtual void InformOfStartingPositions(const HomeRow &homeRow) {}
    virtual Move MakeMove(const Board &board);

signals:
    void ChooseStart(PlayerSide side);
    void GetMove(const Board &board, PlayerSide side);
    void Resume();
    void ReleaseAcknowledgeMove();
    void OpponentMakesMove(Move);

public slots:
    void StartChosen(HomeRow hr);
    void MoveMade(Move move);
    void QuitGame();
    void MoveAcknowledged();

private:
    HomeRow homeRow;
    Move move;

    bool givenUp;
};

#endif // HUMANPLAYER_H
