#ifndef PRNCSSMAINWINDOW_H
#define PRNCSSMAINWINDOW_H

#include "networkmanager.h"
#include "humanplayer.h"
#include "gameengine.h"
#include "boardmanager.h"

#include <QMainWindow>
#include <QApplication>
#include <QDeclarativeView>
#include <QGraphicsObject>

class PRNCSSMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit PRNCSSMainWindow(QDeclarativeView* view, QApplication* app, QWidget *parent = 0);

signals:
    void InvalidProfile();
    void quit();

public slots:
    void OnProfileChosen(QString profileName);
    void StartCatFight(QString side);
    void StartCampaign();
    void ReturnToMainMenu();
    void StartLocalMultiplayer();
    void StartNetworking();
    void OnConnectionMade(PlayerSide side);
    void StopNetworking();
    void PlayTutorialGame();
    void Quit();

private slots:
    void NetworkManagerDeleted();
    void GameOver(GameOverState endState);
    void NextCampaignLevel();

private:
    QDeclarativeView* view;
    QGraphicsObject* root;
    QApplication* app;
    NetworkManager* networkManager;
    QThread* networkThread;
    QObject* gameBoard;
    bool goToNextCampaignLevel;
    QString profileName;
    PlayerSide playerSide;

    void ConnectHumanPlayer (HumanPlayer *human, BoardManager *manager);
    void ConnectGameBoard(BoardManager *manager);
    void PlayGame(GameEngine *engine);
    void UpdateStats(PlayerSide humanSide, short gameType);
};

#endif // PRNCSSMAINWINDOW_H
