#ifndef UIMANAGER_H
#define UIMANAGER_H

#include "types.h"
#include "move.h"
#include "board.h"

#include <QVector>
#include <QVariant>
#include <QDeclarativeItem>
#include <QPropertyAnimation>

class BoardManager : public QObject
{
    Q_OBJECT
public:
    explicit BoardManager(QObject* rootObject, bool noAnimations = false, QObject *parent = 0);
    virtual ~BoardManager() { qDebug() << __FILE__ << __LINE__ << "BoardManager deleted."; }

signals:
    void UIChooseStart(QVariant start);

    void StartChosen(HomeRow row);
    void MoveMade(Move move);

    void ShowCancelButton();
    void HideCancelButton();

    void ShowAcceptButton();
    void HideAcceptButton();

    void AnimationEnds();

public slots:
    void UpdateBoard(const Board &board);

    virtual void UIPositionClicked(QString pos);

    void ChooseStart(PlayerSide side);
    void GetMove(const Board &board, PlayerSide side);

    void UIGotStart(QString row);

    void CancelMove(bool getMove = true);
    void AcceptMove();

    void AnimateMove(Move move);

    void BeastMode();
    void DoIDie();

private slots:
    void AiSuggestMove();

protected:
    QVector<QObject*> boardPositions;
    QDeclarativeItem* heldPiece;
    QDeclarativeItem* animatedPiece;
    QObject* gameBoard;

    void ShowCurrentTurn(PlayerSide side);
    void DisableAll();
    void EnableSide(const Board &board, PlayerSide side);
    QPropertyAnimation * GeneratePartialAnimation(BoardPosition previousPosition, BoardPosition currentPosition);
    void SendChat(const QString &message, const QString &sender) {
        qDebug() << __FILE__ << __LINE__ << "Sending chat" << message << "from" << sender;
        QMetaObject::invokeMethod(gameBoard, "onNewChatMessage", Q_ARG(QVariant, QVariant::fromValue(message)), Q_ARG(QVariant, QVariant::fromValue(sender)));
    }
    Move GetAiMove(Board moveBoard, PlayerSide aiSide);

    Move move;
    bool needMove;
    bool gotFirstPos;
    bool needReplacement;
    PlayerSide side;
    BoardPosition firstPos;
    BoardPosition secondPos;
    BoardPosition replacement;
    Board board;
    bool disableAnimations;
    bool beastMode;
};

#endif // UIMANAGER_H
