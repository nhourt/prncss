#include "player.h"
#include "gameengine.h"

PlayerSide Player::GetSide()
{
    return side;
}

void Player::AbortGame()
{
    throw GameEngine::UserAbortedGameException();
}

QDebug operator<< (QDebug dbg, PlayerSide p) {
    if (p == NorthSide)
        dbg.nospace() << "North";
    else
        dbg.nospace() << "South";
    return dbg.space();
}
#include "moc_player.cpp"
