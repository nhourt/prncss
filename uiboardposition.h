#ifndef UIBOARDPOSITION_H
#define UIBOARDPOSITION_H

#include <QGraphicsRectItem>
#include <QGraphicsBlurEffect>
#include <QVector>
#include <QColor>
#include "types.h"

class UIBoardPosition : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    explicit UIBoardPosition(QGraphicsItem *parent = 0);
    UIBoardPosition(BoardPosition pos, QGraphicsItem *parent = 0);
    UIBoardPosition(BoardPosition pos, GamePiece piece, QGraphicsItem *parent);
    ~UIBoardPosition();

    QString getName() { return name; }
    void setName(const QString name) { this->name = name; }

    GamePiece getPiece() { return piece; }
    void setPiece(const GamePiece piece) { this->piece = piece; }

    BoardPosition getPosition() { return pos; }
    void setPosition(const BoardPosition position) { this->pos = position; }

    QVector<QColor> getColors();

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

signals:
    void wasClicked(BoardPosition position);

private:
    void init();

    QString name;
    BoardPosition pos;
    GamePiece piece;

    bool hover;
    bool clicked;
};

#endif // UIBOARDPOSITION_H
