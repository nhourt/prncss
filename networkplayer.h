
#ifndef NETWORKPLAYER_H
#define NETWORKPLAYER_H
#include "board.h"
#include "player.h"
#include "networkmanager.h"

class NetworkPlayer : public Player
{
    Q_OBJECT
public:
    NetworkPlayer (NetworkManager* networkManager, QObject* parent = 0);
    virtual ~NetworkPlayer() {}

    /**
     * @brief Sets the Home Row
     * @param board The current state of the game board
     * @return The network player's (opponent's) Home Row
     */
    virtual HomeRow ChooseStartingPositions (const Board& board);

    /**
     * @brief Sends the local player's home row to the remote player
     * @param homeRow The home row chosen by the local player
     */
    virtual void InformOfStartingPositions (const HomeRow& homeRow);

    /**
     * @brief Sends the opponent's move over the network
     * @param opponentMove The opponent's move
     */
    virtual void AcknowledgeOpponentMove (const Move& opponentMove);

    /**
     * @brief Gets a move from over the network
     * @param board The current state of the game board
     * @return The move that was made
     */
    virtual Move MakeMove (const Board& board);

signals:
    void SendMessage (const QString& message);

private:
    NetworkManager* networkManager;
};

#endif // NETWORKPLAYER_H
