#include "humanplayer.h"

#include <QEventLoop>

HumanPlayer::HumanPlayer(QObject *parent) : Player(parent), givenUp(false) {}

HomeRow HumanPlayer::ChooseStartingPositions(const Board &board)
{
      QEventLoop el;

      qDebug() << __FILE__ << __LINE__ << side << "emit ChooseStart";
      connect(this, SIGNAL(Resume()), &el, SLOT(quit()));

      emit ChooseStart(side); // goes to BoardManager

      el.exec();

      if (givenUp) {
          givenUp = false;
          AbortGame();
      }

      return homeRow;
}

void HumanPlayer::AcknowledgeOpponentMove(const Move &opponentMove)
{
    emit OpponentMakesMove(opponentMove);

    QEventLoop el;
    connect(this, SIGNAL(ReleaseAcknowledgeMove()), &el, SLOT(quit()));
    el.exec();
}

void HumanPlayer::StartChosen(HomeRow row)
{
    homeRow = row;
    emit Resume();
}

Move HumanPlayer::MakeMove(const Board &board)
{
    QEventLoop el;
    
    qDebug() << __FILE__ << __LINE__ << side << "emit GetMove";
    emit GetMove(board, side);
    connect(this, SIGNAL(Resume()), &el, SLOT(quit()));

    el.exec();

    if (givenUp) {
        givenUp = false;
        AbortGame();
    }
    
    return move;
}

void HumanPlayer::MoveMade(Move move)
{
    this->move = move;
    emit Resume();
}

void HumanPlayer::QuitGame()
{
    givenUp = true;
    emit Resume();
}

void HumanPlayer::MoveAcknowledged()
{
    emit ReleaseAcknowledgeMove();
}
