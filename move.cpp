#include "move.h"
#include <QStringList>

Move::Move(const Move &other) : startPos(other.startPos),
                                path(other.path),
                                pathPos(path.begin()),
                                pathDepth(0),
                                replace(other.replace),
                                repPos(other.repPos)
{}

Move& Move::operator =(const Move& other)
{
    startPos = other.startPos;
    pathPos = other.pathPos;
    pathDepth = other.pathDepth;
    replace = other.replace;
    repPos = other.repPos;
    path = other.path;

    return *this;
}

bool Move::operator== (const Move& other) const
{
    if (startPos != other.startPos || GetEndPosition() != other.GetEndPosition() ||
        replace != other.replace || repPos != other.repPos)
        return false;
    return true;
}

Move::operator QString()
{
    QString move;
    QTextStream moveStream(&move);

    moveStream << "moves from <strong>"
               << startPos.operator QString()
               << "</strong> to <strong>"
               << GetEndPosition().operator QString()
               << "</strong>";

    if (replace)
        moveStream << " and replaces to <strong>"
                   << repPos.operator QString()
                   << "</strong>";

    moveStream << '.';
    return move;
}

void Move::SetStartPosition (BoardPosition startPosition)
{
    startPos = startPosition;
}

BoardPosition Move::GetStartPosition() const
{
    return startPos;
}

BoardPosition Move::GetEndPosition () const
{
    ResetGetNextDirection();
    BoardPosition p = GetStartPosition();
    for (int i = 0; i < GetDirectionCount(); ++i)
        p.MovePosition(GetNextDirection());
    ResetGetNextDirection();
    return p;
}

void Move::AddDirection (MoveDirection d)
{
    path.push_back (d);
}

void Move::DeleteLastDirection()
{
    path.pop_back();
    ResetGetNextDirection();
}

MoveDirection Move::GetNextDirection() const
{
    if (pathDepth == 0)
        pathPos = path.begin();
    if (pathPos == path.end())
        throw OutOfDirectionsException ("No more directions available; cannot retrieve next direction.");

    MoveDirection d = *pathPos;
    ++pathPos;
    ++pathDepth;
    return d;
}

void Move::ResetGetNextDirection() const
{
    pathPos = path.begin();
    pathDepth = 0;
}

short int Move::GetNextDirectionCount() const
{
    return pathDepth;
}

int Move::GetDirectionCount() const
{
    return path.size();
}

void Move::AddReplacement (BoardPosition replaceTo)
{
    replace = true;
    repPos = replaceTo;
}

void Move::DeleteReplacement()
{
    replace = false;
}

bool Move::IsReplacement() const
{
    return replace;
}

BoardPosition Move::GetReplacePosition() const
{
    return repPos;
}

QString Move::Serialize() const
{
    //The current serial form of a move is as follows:
    //<Two char starting coordinate, in form C3>-<NSEW directions (north, south, east, west)>-<Two char replacement coordinate>
    //Note that the last hyphen and coordinate are optional.
    
    QString out;
    QTextStream serial(&out);
    // add start position to serialized move
    serial << startPos.GetColumn() << startPos.GetRow() << '-';

    // step through all of the directions
    foreach (MoveDirection direction, path) {
        switch (direction) {
        case Up:
            serial << 'N';
            break;
        case Down:
            serial << 'S';
            break;
        case Left:
            serial << 'W';
            break;
        case Right:
            serial << 'E';
        }
    }

    // add replacement
    if (replace)
        serial << '-' << repPos.GetColumn() << repPos.GetRow();

    return out;
}

Move Move::Deserialize(QString serialMove)
{
    //Current move serial format is described in Move::Serialize
    
    Move deserializedMove;
    QStringList serialParts = serialMove.split('-');
    
    deserializedMove.SetStartPosition(serialParts[0]);

    foreach (QChar direction, serialParts[1]) {
        switch (direction.toAscii()) {
        case 'N':
            deserializedMove.AddDirection(Up);
            break;
        case 'S':
            deserializedMove.AddDirection(Down);
            break;
        case 'E':
            deserializedMove.AddDirection(Right);
            break;
        case 'W':
            deserializedMove.AddDirection(Left);
            break;
        }
    }
    
    if (serialParts.size() == 3)
        deserializedMove.AddReplacement(serialParts[2]);
    
    return deserializedMove;
}

void Move::Rotate()
{
    startPos.Rotate();
    repPos.Rotate();
    
    QList<MoveDirection> newPath;
    foreach (MoveDirection m, path) {
        switch (m) {
            case Up:
                newPath.push_back(Down);
                break;
            case Down:
                newPath.push_back(Up);
                break;
            case Left:
                newPath.push_back(Right);
                break;
            case Right:
                newPath.push_back(Left);
                break;
        }
    }
    path = newPath;
}

void Move::Clear()
{
    (*this) = Move();
}

void Move::DeleteAllDirections()
{
    path.clear();
    ResetGetNextDirection();
}

QDebug operator<< (QDebug dbg, const Move& m)
{
    dbg.nospace() << "Move starts at " << m.GetStartPosition() << "and goes";

    m.ResetGetNextDirection();
    for (int i = m.GetDirectionCount(); i > 0; --i) {
        dbg.nospace() << m.GetNextDirection();
    }
    m.ResetGetNextDirection();

    dbg.nospace() << "to " << m.GetEndPosition();

    if (m.IsReplacement()) {
        dbg.nospace() << "and replaces to " << m.GetReplacePosition();
    }

    return dbg.space();
}

QDebug operator<< (QDebug dbg, MoveDirection d)
{
    switch (d) {
    case Up:
        dbg.nospace() << "Up";
        break;
    case Down:
        dbg.nospace() << "Down";
        break;
    case Left:
        dbg.nospace() << "Left";
        break;
    case Right:
        dbg.nospace() << "Right";
        break;
    }

    return dbg.space();
}

#include "moc_move.cpp"
