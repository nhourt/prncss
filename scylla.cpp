#include "scylla.h"

#include<string>
#include<sstream>
#include<vector>
#include <ctime>
#include<iostream>
#include<algorithm>

using namespace std;

Scylla::Scylla (Scylla* nextHead) : board(67,0), liberty(67,0)
{
    if (nextHead == NULL)
        nest = 0;
    else nest = 1;
    innerMove = nextHead;
}

Scylla::Scylla() : board(67,0), liberty(67,0)
{
    nest = 0;
    innerMove = NULL;
}

Scylla::~Scylla() {}

Scylla::ScyllaMove Scylla::chooseMove (vector<int> board, bool south, int lvl)
{
    moves.clear();
    reset();
    this->board = board;
    this->south = south;
    this->lvl = lvl;
    return evaluate();
}

Scylla::ScyllaMove Scylla::evaluate()
{
    moves.reserve (1000);
    int startIndex = 0;
    lineOpp = getFirstLine (!south);
    moves.clear();
    vector<int> homeRowIndeces;
    note = 0;

    //Find all possible moves, or take a win if available
    int homeRowOffset = 0;
    while (moves.size() == 0) {
        homeRowIndeces = getPlayables (south, homeRowOffset++);

        for (size_t l = 0; l < homeRowIndeces.size(); ++l) {
            startIndex = homeRowIndeces[l];
            if (startIndex > 0) {
                win = 0;
                vector<int> liberties = getLiberty (startIndex, south);

                //Take wins if possible
                if (lvl != 1) {
                    if (win > 0) {
                        if (south)
                            return ScyllaMove (startIndex, 01, 0, 10000000);
                        else
                            return ScyllaMove (startIndex, 71, 0, 10000000);
                    }
                }

                //Find all moves this piece can make
                for (int index = 11; index <= 66; ++index)
                    if (liberties[index] > 0)
                        buildMove (startIndex, index);
            }
        }
    }

    srand(time(NULL));
    int totalPossibleMoves = moves.size();
    if (lvl == 1 || lvl == 2)
        return moves[rand() % totalPossibleMoves];
    int c = 0;
    if (lvl == 3)
        c = (double) totalPossibleMoves * .5;
    if (lvl == 4)
        c = (double) totalPossibleMoves * 1;
    if (lvl == 5)
        c = (double) totalPossibleMoves * 1;


    //Throw away moves at random; the number tossed depends on c (bigger c == fewer moves tossed)
    srand ( (unsigned int) time (NULL));

    for (unsigned int i = 0; i < moves.size();) {
        int randomVal = rand() % totalPossibleMoves;
        if (randomVal >= c) {
            swap (moves[i], moves.back());
            moves.pop_back();

        } else
            ++i;
    }

    sort (moves.begin(), moves.end());
    if (nest == 1 && lvl == 5) {
        for (vector<ScyllaMove>::iterator it = moves.begin(); it != moves.end(); unsimulate (it->x1, it->x2, it->x3), ++it) {
            simulate (it->x1, it->x2, it->x3);
            it->note = - (innerMove->chooseMove (board, !south, lvl).note);
        }
    } else {
        for (vector<ScyllaMove>::iterator it = moves.begin(); it != moves.end(); unsimulate (it->x1, it->x2, it->x3), ++it) {
            simulate (it->x1, it->x2, it->x3);
            note = 0;
            win = 0;

            vector<int> homeRowIndeces = getPlayables (south);
            for (int idx = 0; idx < 6; ++idx) {
                startIndex = homeRowIndeces[idx];
                vector<int> liberties;
                if (startIndex > 0)
                    liberties = getLiberty (startIndex, south);
            }

            it->note = note + 30 * win;
            it->win = win;

            //player moves
            note = 0;
            win = 0;
            homeRowIndeces = getPlayables (!south);
            for (int idx = 0; idx < 6; ++idx) {
                startIndex = homeRowIndeces[idx];
                vector<int> liberties;
                if (startIndex > 0)
                    liberties = getLiberty (startIndex, !south);
            }

            it->note -= note;
            if (win > 0)
                it->note -= 10000000;

        }

        sort (moves.begin(), moves.end());
        return moves[0];
    }

    sort (moves.begin(), moves.end());
    return moves[0];
}

void Scylla::simulate (int start, int end, int replace)
{
    if (replace == 0) {
        board[end] = board[start];
        board[start] = 0;
    } else if (replace == start) {
        swap (board[start], board[end]);
    } else {
        board[replace] = board[end];
        board[end] = board[start];
        board[start] = 0;
    }
}

void Scylla::unsimulate (int start, int end, int replace)
{
    if (replace == 0) {
        board[start] = board[end];
        board[end] = 0;
    } else if (replace == start) {
        swap (board[start], board[end]);
    } else {
        board[start] = board[end];
        board[end] = board[replace];
        board[replace] = 0;
    }
}

//This function finds all conceivably possible moves and replacements starting at startIndex and ending at endIndex
//and adds them to the moves vector.
void Scylla::buildMove (int startIndex, int endIndex)
{
    ScyllaMove move1;
    move1.x1 = startIndex;
    move1.x2 = endIndex;
    if (startIndex != endIndex) {
        if (board[endIndex] == 0)
            //If endIndex is empty, just add a non-replacing move from startIndex to endIndex
        {
            move1.x3 = 0;
            moves.push_back (move1);
        } else if (south)
            //If endIndex is not empty and I am South...
        {
            //Add all legal replacements of endIndex
            for (int row = 60; row >= 10 * lineOpp; row -= 10) {
                for (int col = 1; col <= 6; ++col) {
                    int idx = row + col;
                    if ( (board[idx] == 0 || idx == startIndex) && (board[startIndex] != board[endIndex] || idx != startIndex))
                        moves.push_back (ScyllaMove (startIndex, endIndex, idx));
                    if (idx == startIndex && board[startIndex] != board[endIndex])
                        moves.push_back (ScyllaMove (startIndex, endIndex, idx));
                }

            }

        } else {
            for (int row = 10; row <= 10 * lineOpp; row += 10) {
                for (int col = 1; col <= 6; ++col) {
                    int idx = row + col;
                    if (board[idx] == 0)
                        moves.push_back (ScyllaMove (startIndex, endIndex, idx));
                    if (idx == startIndex && board[startIndex] != board[endIndex])
                        moves.push_back (ScyllaMove (startIndex, endIndex, idx));
                }
            }
        }
    }
}

vector<int> Scylla::getLiberty (int index, bool south)
{
    for (int k = 11; k <= 66; ++k)
        liberty[k] = 0;

    mySide = south;
    saveCell = index;
    search (index, board[index], 0);
    return liberty;
}

//This method returns an array of the indeces of the home row pieces
vector<int> Scylla::getPlayables (bool south, int homeRowOffset)
{
    vector<int> homeRow (6, 0);
    int homeRowIndex;

    if (south)
        homeRowIndex = getFirstLine(south) - homeRowOffset;
    else
        homeRowIndex = getFirstLine (south) + homeRowOffset;

    for (int k = 1; k <= 6; ++k) {
        int index = 10 * homeRowIndex + k;
        if (board[index] > 0)
            homeRow[k - 1] = index;
    }

    return homeRow;
}

int Scylla::getFirstLine (bool south)
{
    int x = 0;
    if (south) {
        x = 7;
        int firstRow = 7;
        while (firstRow == 7) {
            x--;
            for (int y = 1; y <= 6; ++y)
                firstRow += board[10 * x + y];

        }
    } else {
        x = 0;
        int firstRow = 0;
        while (firstRow == 0) {
            ++x;
            for (int y = 1; y <= 6; ++y)
                firstRow += board[10 * x + y];

        }
    }
    return x;
}

void Scylla::reset()
{
    for (int i = 0; i < 67; ++i) {
        board[i] = 0;
        h[i] = v[i] = true;
    }
}

bool Scylla::isInBoard (int i)
{
    if (i > 10 && i < 17)
        return true;
    if (i > 20 && i < 27)
        return true;
    if (i > 30 && i < 37)
        return true;
    if (i > 40 && i < 47)
        return true;
    if (i > 50 && i < 57)
        return true;
    return i > 60 && i < 67;
}

//This appears to be a depth-first search, seeing where all a given piece can go
//As it goes, it keeps track of:
// liberty -- how many ways can I access a given position?
// note -- a count of how many wins a piece has plus how many pieces it can reach
// win -- a count of how many wins a piece has
void Scylla::search (int index, int moves, int incomingDirection)
{
    //If we are out of moves, try to get more
    if (moves == 0) {
        ++liberty[index];
        ++note;
        if (board[index] > 0) {
            ++note;
            search (index, board[index], incomingDirection);
        }
    } else {
        if (incomingDirection != 1) {
            int downIndex = index + 10;

            if (isInBoard (downIndex)) {
                if (v[index] && (board[downIndex] == 0 || moves == 1 || downIndex == saveCell)) {
                    v[index] = false;
                    search (downIndex, moves - 1, 3);
                    v[index] = true;
                }
            } else if (moves == 1) {
                if (!mySide && index > 60) {
                    ++win;
                    ++note;
                } else if (mySide  && index < 17) {
                    ++win;
                    ++note;
                }
            }
        }
        if (incomingDirection != 2) {
            int rightIndex = index + 1;

            if (isInBoard (rightIndex)) {
                if (h[index] && (board[rightIndex] == 0 || moves == 1 || rightIndex == saveCell)) {

                    h[index] = false;
                    search (rightIndex, moves - 1, 4);
                    h[index] = true;
                }
            } else if (moves == 1) {
                if (!mySide && index > 60) {
                    ++win;
                    ++note;
                } else if (mySide && index < 17) {
                    ++win;
                    ++note;
                }
            }
        }
        if (incomingDirection != 3) {
            int upIndex = index - 10;

            if (isInBoard (upIndex)) {
                if (v[index - 10] && (board[upIndex] == 0 || moves == 1 || upIndex == saveCell)) {
                    v[index - 10] = false;
                    search (upIndex, moves - 1, 1);
                    v[index - 10] = true;
                }
            } else if (moves == 1) {
                if (!mySide && index > 60) {
                    ++win;
                    ++note;
                } else if (mySide && index < 17) {
                    ++win;
                    ++note;
                }
            }
        }
        if (incomingDirection != 4) {
            int leftIndex = index - 1;

            if (isInBoard (leftIndex)) {
                if (h[index - 1] && (board[leftIndex] == 0 || moves == 1 || leftIndex == saveCell)) {
                    h[index - 1] = false;
                    search (leftIndex, moves - 1, 2);
                    h[index - 1] = true;
                }
            } else if (moves == 1) {
                if (!mySide && index > 60) {
                    ++win;
                    ++note;
                } else if (mySide && index < 17) {
                    ++win;
                    ++note;
                }
            }
        }
    }
}
