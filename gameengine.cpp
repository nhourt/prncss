#include "gameengine.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QtGlobal>
#include <QtCore/QDebug>

GameEngine* GameEngine::GetEngine()
{
    if (engine == NULL) {
        engine = new GameEngine;
        engineThread = new QThread();
        engineThread->setObjectName("Engine Thread");
        engine->moveToThread(engineThread);
        engineThread->start(QThread::LowPriority);

        terminationRequested = false;
        inGame = false;
    }
    return engine;
}

void GameEngine::SetPlayers (Player* northPlayer, Player* southPlayer)
{
    north = northPlayer;
    north->moveToThread(engineThread);
    north->SetSide(NorthSide);

    south = southPlayer;
    south->moveToThread(engineThread);
    south->SetSide(SouthSide);
}

void GameEngine::PlayGame()
{
    if (north == NULL || south == NULL)
        throw MissingPlayerException ("Tried to play game without players!");

    QObject gameWatcher;
    connect(&gameWatcher, SIGNAL(destroyed(QObject*)), this, SLOT(EndGame()));
    
    try {
        //Reset the game
        terminationRequested = false;
        gameBoard = Board();
        emit BoardUpdated(gameBoard);

        currentPlayer = south;
        currentPlayerSide = SouthSide;

        qDebug() << __FILE__ << __LINE__ << "Starting new game; players choosing initial positions";
        inGame = true;
        HomeRow homeRow = south->ChooseStartingPositions(gameBoard);
        qDebug() << __FILE__ << __LINE__ << SouthSide << "choses home row" << homeRow;
        SetInitialHomeRow (homeRow, SouthSide);
        north->InformOfStartingPositions(homeRow);
        emit BoardUpdated(gameBoard);

        NextTurn();

        homeRow = north->ChooseStartingPositions(gameBoard);
        SetInitialHomeRow (homeRow, NorthSide);
        south->InformOfStartingPositions(homeRow);
        qDebug() << __FILE__ << __LINE__ << "Selected initial positions:\n" << gameBoard;
        emit BoardUpdated(gameBoard);

        NextTurn();

        while (gameBoard.GameState() == InProgress) {
            if (terminationRequested)
            {
                delete engine;
                engine = NULL;
                return;
            }

            qDebug() << __FILE__ << __LINE__ << currentPlayerSide << "turn begins.";

            Move move = currentPlayer->MakeMove (gameBoard);

            BadMoveReason moveResult = gameBoard.CanMakeMove (currentPlayerSide, move);
            short badMoves = 0;

            while (moveResult != BMRValidMove) {
                if (badMoves == MAXBADMOVES || terminationRequested)
                    return;

                qDebug() << __FILE__ << __LINE__ << "Bad move" << badMoves++ << "given:" << moveResult;
                qDebug() << __FILE__ << __LINE__ << "The offending" << move;
                //TODO: Emit signal that a bad move was given
                move = currentPlayer->MakeMove (gameBoard);

                moveResult = gameBoard.CanMakeMove (currentPlayerSide, move);
            }

            qDebug() << __FILE__ << __LINE__ << "Applying move:" << move;

            gameBoard.ApplyMove (currentPlayerSide, move);
            if (currentPlayerSide == NorthSide)
                south->AcknowledgeOpponentMove(move);
            else
                north->AcknowledgeOpponentMove(move);

            emit BoardUpdated(gameBoard);

            qDebug() << __FILE__ << __LINE__ << "Board after move:\n" << gameBoard;

            if (currentPlayerSide == NorthSide)
                emit MoveMade(QString("North ") + move);
            else
                emit MoveMade(QString("South ") + move);

            NextTurn();
        }
    } catch (const UserAbortedGameException &aborted) {
        gameBoard.QuitGame();
        qDebug() << __FILE__ << __LINE__ << "Player aborted game; declaring tie";
    }
}

PlayerSide GameEngine::CurrentTurn()
{
    return currentPlayerSide;
}

void GameEngine::EndGame()
{
    qDebug() << __FILE__ << __LINE__ << "Game over:" << gameBoard.GameState();
    emit GameOver(gameBoard.GameState());
    inGame = false;
    
    DeletePlayers();
}

void GameEngine::DeletePlayers()
{
    north->deleteLater();
    north = NULL;
    south->deleteLater();
    south = NULL;
}

GameEngine::~GameEngine()
 {
    qDebug() << __FILE__ << __LINE__ << "Destroying GameEngine";
    if (inGame)
        DeletePlayers();
}

Board GameEngine::GetBoard() const
{
    return gameBoard;
}

void GameEngine::SetInitialHomeRow (HomeRow row, PlayerSide side)
{
    BoardPosition p;
    p.SetRow ( (side == NorthSide) ? BOARDSIZE : 1);

    for (char col = 'A'; col <= 'F'; p.SetColumn (++col))
        gameBoard[p] = row[col];
}

void GameEngine::NextTurn()
{
    if (currentPlayerSide == NorthSide) {
        currentPlayerSide = SouthSide;
        currentPlayer = south;
    } else {
        currentPlayerSide = NorthSide;
        currentPlayer = north;
    }
}

QDebug operator<< (QDebug dbg, GameOverState s)
{
    switch (s) {
    case OpponentQuit:
        dbg.nospace() << "Tie game";
        break;
    case NorthWin:
        dbg.nospace() << "North wins";
        break;
    case SouthWin:
        dbg.nospace() << "South wins";
        break;
    case InProgress:
        dbg.nospace() << "Game in progress";
    }
    return dbg.space();
}

GameEngine* GameEngine::engine = NULL;
QThread* GameEngine::engineThread = NULL;
bool GameEngine::terminationRequested = false;
bool GameEngine::inGame = false;
const short GameEngine::MAXBADMOVES = 5;

#include "moc_gameengine.cpp"
