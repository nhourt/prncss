#ifndef NETWORKINTERFACE_H
#define NETWORKINTERFACE_H

#include "types.h"
#include "keepalivemanager.h"

#include <QString>
#include <QtNetwork/QtNetwork>

const quint16 UDPPORT = 45454;
const quint16 GAMEPORT = UDPPORT;

class NetworkManager : public QObject
{
    Q_OBJECT
public:
    NetworkManager (QString name, QObject *parent = 0);
    virtual ~NetworkManager();

    /**
     * @brief Returns a list of the peers
     * @return A list of all the peers' names
     */
    QStringList PeersFound();

    /**
     * @brief Gets a message from the game network connection
     * If no message is available, this method will block until one is
     * @return The message
     */
    QString GetGameMessage();

signals:
    void PeersUpdated(const QVariant &peers);
    void PlayRequest(const QVariant &peerName);
    void PlayRequestRejected();
    void ChooseSide();
    void Resume();
    void ConnectionMade(PlayerSide side);
    void NewChatMessage (const QVariant &message, const QVariant &sender);
    void ConnectionDropped();
    void ConnectionRecovered();
    void ConnectionLost();
    void chatSocket (QByteArray messageBlock);
    
public slots:
    /**
     * @brief Asks opponent if they would like to play
     * @param peerName The name of the opponent
     */
    void SendPlayRequest(const QString &peerName);

    /**
     * @brief Sends the user's response to a play request
     * @param acceptance A bool specifying true if user would like to play and false if not
     */
    void RespondToPlayRequest(bool acceptance = true);

    void SideChosen(QString side);
    
    /**
     * @brief Sends a message over the Game connection
     * @param message The message to send
     */
    void SendGameMessage (const QString &message);

    /**
     * @brief Causes the network player to resign
     */
    void resign();
    
    /**
     * @brief Sends a message over the Chat connection
     * @param message The message to send
     */
    void SendChatMessage (const QString &message);
    void LeaveRoom();
    
private slots:
    void EnterRoom();
    void RecoveredConnection();
    void TCPError (QAbstractSocket::SocketError error);
    void DataGramReady();
    void GameSocketReadyRead();
    void ConnectToServer(QHostAddress serverIP);
    void ListenForClients();
    void WaitForConnection();
    void LostConnection();

private:
    QString name;
    QString opponentName;
    QHostAddress opponentAddress;

    QUdpSocket *udpSocket;
    QTcpSocket *gameSocket;
    QTcpServer *gameServer;
    QHash<QString, QHostAddress> namesToAddresses;

    KeepAliveManager *keepAliveManager;

    QQueue< QString > gameMessageQueue;
    QSemaphore gameMessages;
    
    const static QString WHO;
    const static QString PLAY;
    const static QString ME;
    const static QString NO;
    const static QString YES;
    const static QString GONE;
    const static QString OK;
    const static QString NORTH;
    const static QString SOUTH;
    const static QString LOST;
    const static char* RAMROD;
    
    const static char GAME_PREFIX;
    const static char CHAT_PREFIX;

    void AddPeer(const QString &peerName, const QHostAddress &peerAddress);
    void ConnectSocketSignals();
    void StartKeepAliveManager();

};

#endif // NETWORKINTERFACE_H
