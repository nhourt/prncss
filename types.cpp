#include "types.h"

QTextStream &operator<< (QTextStream &ts, GamePiece piece) {
    ts << short(piece);
    return ts;
}
QTextStream &operator>> (QTextStream &ts, GamePiece &piece) {
    char streamPiece;
    ts >> streamPiece;
    piece = GamePiece(streamPiece-'0');
    return ts;
}

HomeRow::HomeRow(QString rowString)
{
    QTextStream textStream(&rowString);

    for (short i = 0; i < BOARDSIZE; ++i)
        textStream >> row[i];
}

HomeRow::operator QString() const
{
    QString result;
    QTextStream textStream (&result);

    for (short i = 0; i < BOARDSIZE; ++i)
        textStream << row[i];

    return result;
}
