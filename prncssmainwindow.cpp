#include "prncssmainwindow.h"
#include "networkmanager.h"
#include "networkplayer.h"
#include "aiplayer.h"
#include "profilemanager.h"
#include "tutorialmanager.h"

#include <QDeclarativeEngine>
#include <QThread>
#include <QDebug>
#include "audiomanager.h"

const short CATFIGHT_GAME = 0;
const short CAMPAIGN_GAME = 1;
const short NETWORK_GAME = 2;

PRNCSSMainWindow::PRNCSSMainWindow(QDeclarativeView* view, QApplication* app, QWidget *parent)
    : QMainWindow(parent),
      view(view),
      root(view->rootObject()),
      app(app),
      goToNextCampaignLevel(false),
      networkManager(NULL),
      networkThread(NULL),
      gameBoard(NULL)
{
    QObject* profileSelect = root->findChild<QObject*>("profileSelect");

    connect(profileSelect, SIGNAL(profileChosen(QString)), this, SLOT(OnProfileChosen(QString)));
    connect(view->engine(), SIGNAL(quit()), this, SLOT(Quit()));
}

void PRNCSSMainWindow::Quit()
{
    emit quit();

    int fadeOutTime = 750;
    AudioManager::instance()->stopMusic(fadeOutTime);
    QPropertyAnimation* closingAnimation = new QPropertyAnimation(root, "opacity");
    closingAnimation->setEndValue(0.0);
    closingAnimation->setDuration(fadeOutTime);

    QEventLoop el;
    connect(closingAnimation, SIGNAL(finished()), &el, SLOT(quit()));
    closingAnimation->start(QAbstractAnimation::DeleteWhenStopped);
    el.exec();

    app->quit();
}

void PRNCSSMainWindow::OnProfileChosen(QString profileName)
{
    this->profileName = profileName;
    root->setProperty("state", "MainMenu");

    QObject* mainMenu = root->findChild<QObject*>("mainMenu");
    
    if (networkManager != NULL) {
        networkManager->deleteLater();
        QCoreApplication::processEvents();
        networkManager = NULL;
    }

    connect(mainMenu, SIGNAL(startLocalMultiplayer()), this, SLOT(StartLocalMultiplayer()), Qt::UniqueConnection);
    connect(mainMenu, SIGNAL(startNetworking()), this, SLOT(StartNetworking()), Qt::UniqueConnection);
    connect(mainMenu, SIGNAL(startCampaign()), this, SLOT(StartCampaign()), Qt::UniqueConnection);
    connect(mainMenu, SIGNAL(startCatFight(QString)), this, SLOT(StartCatFight(QString)), Qt::UniqueConnection);
    connect(mainMenu, SIGNAL(startTutorial()), this, SLOT(PlayTutorialGame()), Qt::UniqueConnection);
}

void PRNCSSMainWindow::ReturnToMainMenu()
{
    root->setProperty("state", "MainMenu");
}

void PRNCSSMainWindow::PlayTutorialGame()
{
    qDebug() << __FILE__ << __LINE__ << "Starting Tutorial Game";
    root->setProperty("state", "GameBoard");
    gameBoard = root->findChild<QObject*>("gameBoard");
    gameBoard->setProperty("networkGame", false);
    gameBoard->setProperty("campaignGame", false);
    gameBoard->setProperty("localMultiplayer", false);
    gameBoard->setProperty("easyGame", ProfileManager::instance()->catfightDifficulty() == ProfileManager::EasyCat);

    TutorialManager* boardManager = new TutorialManager(gameBoard);

    ConnectGameBoard(boardManager);

    boardManager->playTutorialGame();

    boardManager->deleteLater();
    root->setProperty("state", "MainMenu");
}

void PRNCSSMainWindow::StartCampaign()
{
    playerSide = SouthSide;

    goToNextCampaignLevel = true;
    while(goToNextCampaignLevel)
    {
        goToNextCampaignLevel = false;
        qDebug() << __FILE__ << __LINE__ << "Starting campaign";
        root->setProperty("state", "GameBoard");
        gameBoard = root->findChild<QObject*>("gameBoard");
        gameBoard->setProperty("networkGame", false);
        gameBoard->setProperty("campaignGame", true);
        gameBoard->setProperty("localMultiplayer", false);
        QMetaObject::invokeMethod(gameBoard, "blackOut");
        QMetaObject::invokeMethod(gameBoard, "campaignLaunchPad");

        connect(gameBoard, SIGNAL(userQuit()), this, SLOT(ReturnToMainMenu()));
        connect(gameBoard, SIGNAL(nextCampaignLevel()), this, SLOT(NextCampaignLevel()));

        GameEngine* engine = GameEngine::GetEngine();

        AIPlayer* aiPlayer;
        int level = ProfileManager::instance()->campaignLevel();
        switch (level) {
        case ProfileManager::FirstLevel:
            aiPlayer = new AIPlayer(ProfileManager::EasyCat);
            gameBoard->setProperty("easyGame", true);
            break;
        case ProfileManager::SecondLevel:
            aiPlayer = new AIPlayer(ProfileManager::MediumCat);
            gameBoard->setProperty("easyGame", false);
            break;
        case ProfileManager::ThirdLevel:
            aiPlayer = new AIPlayer(ProfileManager::HardCat);
            gameBoard->setProperty("easyGame", false);
            break;
        default:
            aiPlayer = new AIPlayer(ProfileManager::HardCat);
            gameBoard->setProperty("easyGame", false);
        }

        HumanPlayer* humanPlayer = new HumanPlayer();

        engine->SetPlayers(aiPlayer, humanPlayer);

        BoardManager* boardManager = new BoardManager(gameBoard);

        connect(engine, SIGNAL(BoardUpdated(Board)), boardManager, SLOT(UpdateBoard(Board)));

        ConnectHumanPlayer(humanPlayer, boardManager);
        ConnectGameBoard(boardManager);
        connect(gameBoard, SIGNAL(userQuit()), this, SLOT(ReturnToMainMenu()));

        PlayGame(engine);
        UpdateStats(SouthSide, CAMPAIGN_GAME);

        gameBoard->setProperty("campaignGame", false);
        boardManager->deleteLater();

        qDebug() << __FILE__ << __LINE__ << "finished Campaign. goToNextCampaignLevel ==" << (goToNextCampaignLevel ? "yep" : "nope");
    }
}

void PRNCSSMainWindow::StartCatFight(QString side)
{
    playerSide = (side == "North" ? NorthSide : SouthSide);

    qDebug() << __FILE__ << __LINE__ << "Starting Cat Fight";
    root->setProperty("state", "GameBoard");
    gameBoard = root->findChild<QObject*>("gameBoard");
    gameBoard->setProperty("networkGame", false);
    gameBoard->setProperty("campaignGame", false);
    gameBoard->setProperty("localMultiplayer", false);
    gameBoard->setProperty("easyGame", ProfileManager::instance()->catfightDifficulty() == ProfileManager::EasyCat);

    GameEngine* engine = GameEngine::GetEngine();

    AIPlayer* aiPlayer = new AIPlayer(ProfileManager::CatfightDifficulty(ProfileManager::instance()->catfightDifficulty()));
    HumanPlayer* humanPlayer = new HumanPlayer();

    if(playerSide == NorthSide)
        engine->SetPlayers(humanPlayer, aiPlayer);
    else
        engine->SetPlayers(aiPlayer, humanPlayer);

    BoardManager* boardManager = new BoardManager(gameBoard);

    connect(engine, SIGNAL(BoardUpdated(Board)), boardManager, SLOT(UpdateBoard(Board)));

    ConnectHumanPlayer(humanPlayer, boardManager);
    ConnectGameBoard(boardManager);

    PlayGame(engine);
    UpdateStats(playerSide, CATFIGHT_GAME);

    boardManager->deleteLater();
    root->setProperty("state", "MainMenu");
}

void PRNCSSMainWindow::StartLocalMultiplayer()
{
    playerSide = UnknownSide;

    qDebug() << __FILE__ << __LINE__ << "Starting local multiplayer";
    root->setProperty("state", "GameBoard");
    gameBoard = root->findChild<QObject*>("gameBoard");
    gameBoard->setProperty("networkGame", false);
    gameBoard->setProperty("campaignGame", false);
    gameBoard->setProperty("easyGame", false);
    gameBoard->setProperty("localMultiplayer", true);

    GameEngine* engine = GameEngine::GetEngine();

    HumanPlayer* northPlayer = new HumanPlayer();
    HumanPlayer* southPlayer = new HumanPlayer();

    BoardManager* boardManager = new BoardManager(gameBoard, true);

    connect(engine, SIGNAL(BoardUpdated(Board)), boardManager, SLOT(UpdateBoard(Board)));

    ConnectHumanPlayer(northPlayer, boardManager);
    ConnectHumanPlayer(southPlayer, boardManager);
    ConnectGameBoard(boardManager);

    engine->SetPlayers(northPlayer, southPlayer);

    PlayGame(engine);

    boardManager->deleteLater();
    root->setProperty("state", "MainMenu");
}

void PRNCSSMainWindow::StartNetworking()
{
    qDebug() << __FILE__ << __LINE__ << "Setting up NetworkManager with name:" << profileName;
    
    if (networkManager != NULL) {
        networkManager->deleteLater();
        QCoreApplication::processEvents();
    }
    networkManager = new NetworkManager(profileName);

    if (networkThread == NULL) {
        networkThread = new QThread(view);
        networkThread->setObjectName("Network Manager Thread");
    }

    networkManager->moveToThread(networkThread);
    networkThread->start(QThread::LowPriority);

    gameBoard = root->findChild<QObject*>("gameBoard");
    connect(networkManager, SIGNAL(ConnectionDropped()), gameBoard, SLOT(networkLost()));
    connect(networkManager, SIGNAL(ConnectionRecovered()), gameBoard, SLOT(networkRecovered()));
    connect(networkManager, SIGNAL(ConnectionLost()), gameBoard, SLOT(networkFailed()));
    connect(networkManager, SIGNAL(destroyed()), this, SLOT(NetworkManagerDeleted()));

    QObject* playerList = root->findChild<QObject*>("playerList");
    QObject* networkListDialog = root->findChild<QObject*>("networkListDialog");

    connect(networkManager, SIGNAL(PeersUpdated(QVariant)), playerList, SLOT(onPeersUpdated(QVariant)));
    connect(networkManager, SIGNAL(PlayRequest(QVariant)), networkListDialog, SLOT(showPlayRequest(QVariant)));
    connect(networkThread, SIGNAL(finished()), networkManager, SLOT(deleteLater()));

    connect(playerList, SIGNAL(requestPlay(QString)), networkManager, SLOT(SendPlayRequest(QString)));
    connect(networkListDialog, SIGNAL(respondToPlayRequest(bool)), networkManager, SLOT(RespondToPlayRequest(bool)));
    connect(playerList, SIGNAL(leave()), networkManager, SLOT(LeaveRoom()));
    connect(playerList, SIGNAL(leave()), SLOT(StopNetworking()));

    connect(networkManager, SIGNAL(PlayRequestRejected()), networkListDialog, SLOT(showRejection()));
    connect(networkManager, SIGNAL(ChooseSide()), networkListDialog, SLOT(showChooseSide()));
    connect(networkListDialog, SIGNAL(sideChosen(QString)), networkManager, SLOT(SideChosen(QString)));

    connect(networkManager, SIGNAL(ConnectionMade(PlayerSide)), this, SLOT(OnConnectionMade(PlayerSide)));
    QMetaObject::invokeMethod(networkManager, "EnterRoom");

    root->setProperty("state", "NetworkList");
}

void PRNCSSMainWindow::OnConnectionMade(PlayerSide side)
{
    playerSide = side;

    root->setProperty("state", "GameBoard");
    gameBoard = root->findChild<QObject*>("gameBoard");
    gameBoard->setProperty("networkGame", true);
    gameBoard->setProperty("campaignGame", false);
    gameBoard->setProperty("easyGame", false);

    QObject* networkListDialog = root->findChild<QObject*>("networkListDialog");
    QMetaObject::invokeMethod(networkListDialog, "hide");

    if (side == NorthSide)
        QMetaObject::invokeMethod(gameBoard, "showNetworkWait");

    GameEngine* engine = GameEngine::GetEngine();

    HumanPlayer* humanPlayer = new HumanPlayer();
    NetworkPlayer* networkPlayer = new NetworkPlayer(networkManager);

    BoardManager* boardManager = new BoardManager(gameBoard);

    connect((QObject*)view->engine(), SIGNAL(quit()), networkPlayer->thread(), SLOT(quit()));

    connect(engine, SIGNAL(BoardUpdated(Board)), boardManager, SLOT(UpdateBoard(Board)));

    connect(gameBoard, SIGNAL(sendChat(QString)), networkManager, SLOT(SendChatMessage(QString)));
    connect(networkManager, SIGNAL(NewChatMessage(QVariant, QVariant)), gameBoard, SLOT(onNewChatMessage(QVariant, QVariant)));

    connect(gameBoard, SIGNAL(userQuit()), humanPlayer, SLOT(QuitGame()));

    ConnectHumanPlayer(humanPlayer, boardManager);
    ConnectGameBoard(boardManager);

    if(side == NorthSide)
        engine->SetPlayers(humanPlayer, networkPlayer);
    else
        engine->SetPlayers(networkPlayer, humanPlayer);

    PlayGame(engine);
    UpdateStats(side, NETWORK_GAME);

    if (engine->GetBoard().GameState() == InProgress && engine->CurrentTurn() != side)
        networkManager->resign();

    networkManager->deleteLater();
    boardManager->deleteLater();
    root->setProperty("state", "MainMenu");
}

void PRNCSSMainWindow::StopNetworking()
{
    if (networkManager != NULL)
        networkManager->deleteLater();
    networkManager = NULL;
}

void PRNCSSMainWindow::NetworkManagerDeleted()
{
    networkManager = NULL;
    networkThread->exit();
}

void PRNCSSMainWindow::GameOver(GameOverState endState)
{
    QString gameOverReason;

    switch (endState) {
    case OpponentQuit:
        gameOverReason = "Opponent has quit";
        break;
    case NorthWin:
        if(playerSide == NorthSide)
            root->findChild<QObject*>("fireworks")->setProperty("visible", true);
        gameOverReason = "North wins";
        break;
    case SouthWin:
        if(playerSide == SouthSide)
            root->findChild<QObject*>("fireworks")->setProperty("visible", true);
        gameOverReason = "South wins";
        break;
    case InProgress:
        qWarning() << __FILE__ << __LINE__ << "Game is over, but game state is InProgress?";
        break;
    }

    QMetaObject::invokeMethod(gameBoard, "gameEnded", Q_ARG(QVariant, gameOverReason));
}

void PRNCSSMainWindow::NextCampaignLevel()
{
    goToNextCampaignLevel = true;
}

void PRNCSSMainWindow::ConnectHumanPlayer(HumanPlayer *human, BoardManager *manager)
{
    connect(human, SIGNAL(ChooseStart(PlayerSide)), manager, SLOT(ChooseStart(PlayerSide)));
    connect(manager, SIGNAL(StartChosen(HomeRow)), human, SLOT(StartChosen(HomeRow)));
    connect(human, SIGNAL(GetMove(Board, PlayerSide)), manager, SLOT(GetMove(Board,PlayerSide)));
    connect(manager, SIGNAL(MoveMade(Move)), human, SLOT(MoveMade(Move)));
    connect(human, SIGNAL(OpponentMakesMove(Move)), manager, SLOT(AnimateMove(Move)));
    connect(manager, SIGNAL(AnimationEnds()), human, SLOT(MoveAcknowledged()));
}

void PRNCSSMainWindow::ConnectGameBoard(BoardManager* manager)
{
    if(manager && gameBoard)
        qDebug() << __FILE__ << __LINE__ << "connecting manger and gameBoard";
    connect(manager, SIGNAL(UIChooseStart(QVariant)), gameBoard, SLOT(onChooseStart(QVariant)));
    connect(gameBoard, SIGNAL(moveCanceled()), manager, SLOT(CancelMove()));
    connect(gameBoard, SIGNAL(homeRowChosen(QString)), manager, SLOT(UIGotStart(QString)));
    connect(gameBoard, SIGNAL(buttonClick(QString)), manager, SLOT(UIPositionClicked(QString)));
    connect(gameBoard, SIGNAL(beastMode()), manager, SLOT(BeastMode()));
    connect(gameBoard, SIGNAL(doIDie()), manager, SLOT(DoIDie()));
}

void PRNCSSMainWindow::PlayGame(GameEngine *engine)
{
    QEventLoop el;

    connect(engine, SIGNAL(MoveMade(QVariant)), gameBoard, SLOT(logMove(QVariant)), Qt::UniqueConnection);
    connect(engine, SIGNAL(GameOver(GameOverState)), this, SLOT(GameOver(GameOverState)), Qt::UniqueConnection);
    connect(gameBoard, SIGNAL(userQuit()), &el, SLOT(quit()));
    connect(gameBoard, SIGNAL(nextCampaignLevel()), &el, SLOT(quit()));
    QMetaObject::invokeMethod(engine, "PlayGame");

    el.exec();
    root->findChild<QObject*>("fireworks")->setProperty("visible", false);
    qDebug() << __FILE__ << __LINE__ << "exited PlayGame EventLoop";
}

void PRNCSSMainWindow::UpdateStats(PlayerSide humanSide, short int gameType)
{
    GameOverState gameState = GameEngine::GetEngine()->GetBoard().GameState();
    ProfileManager *profile = ProfileManager::instance();

    if (gameState == InProgress)
        return;

    switch (gameType) {
    case CAMPAIGN_GAME:
        if ((gameState == NorthWin && humanSide == NorthSide) || (gameState == SouthWin && humanSide == SouthSide)) {
            if (profile->campaignLevel() == ProfileManager::FirstLevel) {
                profile->setCampaignLevel(ProfileManager::SecondLevel);
                profile->addEasyWin();
            } else if (profile->campaignLevel() == ProfileManager::SecondLevel) {
                profile->setCampaignLevel(ProfileManager::ThirdLevel);
                profile->addMediumWin();
            } else {
                profile->setCampaignLevel(ProfileManager::CampaignComplete);
                profile->addHardWin();
            }
        } else {
            if (profile->campaignLevel() == ProfileManager::FirstLevel)
                profile->addEasyLoss();
            else if (profile->campaignLevel() == ProfileManager::SecondLevel)
                profile->addMediumLoss();
            else
                profile->addHardLoss();
        }
        break;
    case CATFIGHT_GAME:
        if ((gameState == NorthWin && humanSide == NorthSide) || (gameState == SouthWin && humanSide == SouthSide)) {
            if (profile->catfightDifficulty() == ProfileManager::EasyCat)
                profile->addEasyWin();
            else if (profile->catfightDifficulty() == ProfileManager::MediumCat)
                profile->addMediumWin();
            else
                profile->addHardWin();
        } else {
            if (profile->catfightDifficulty() == ProfileManager::EasyCat)
                profile->addEasyLoss();
            else if (profile->catfightDifficulty() == ProfileManager::MediumCat)
                profile->addMediumLoss();
            else
                profile->addHardLoss();
        }
        break;
    case NETWORK_GAME:
        if ((gameState == NorthWin && humanSide == NorthSide) || (gameState == SouthWin && humanSide == SouthSide))
            profile->addNetworkWin();
        else
            profile->addNetworkLoss();
        break;
    }
}
