#-------------------------------------------------
#
# Project created by QtCreator 2012-02-01T19:12:56
#
#-------------------------------------------------

QT       += core gui network phonon

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets quick1
} else {
    QT += declarative
}

TARGET = prncss
!release {
    CONFIG   += console
}
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    types.cpp \
    stubplayer.cpp \
    board.cpp \
    move.cpp \
    networkplayer.cpp \
    networkmanager.cpp \
    keepalivemanager.cpp \
    gameengine.cpp \
    player.cpp \
    prncssmainwindow.cpp \
    humanplayer.cpp \
    aiplayer.cpp \
    scylla.cpp \
    screensnapper.cpp \
    audiomanager.cpp \
    profilemanager.cpp \
    boardmanager.cpp \
    tutorialmanager.cpp

HEADERS += types.h \
    stubplayer.h \
    board.h \
    move.h \
    networkplayer.h \
    networkmanager.h \
    keepalivemanager.h \
    gameengine.h \
    player.h \
    prncssmainwindow.h \
    humanplayer.h \
    aiplayer.h \
    scylla.h \
    screensnapper.h \
    audiomanager.h \
    profilemanager.h \
    boardmanager.h \
    tutorialmanager.h \
    wheelarea.h

RESOURCES += \
    resources.qrc
RC_FILE = prncss.rc

OTHER_FILES += \
    qml/NetworkPlayerList.qml \
    qml/main.qml \
    qml/Button.qml \
    qml/BoardPosition.qml \
    qml/Board.qml \
    qml/GuestList.qml \
    qml/GameBoard.qml \
    qml/MainMenu.qml \
    qml/ProfileSelect.qml \
    qml/HomeRowPrompt.qml \
    qml/SplashScreen.qml \
    qml/Profile.qml \
    qml/Options.qml \
    qml/Slider.qml \
    qml/SnappingSlider.qml \
    qml/Fadeable.qml \
    qml/Throbber.qml \
    imgs/board.svg \
    imgs/button.svg \
    imgs/incendiary_logo_white.svg \
    imgs/prncss_logo_black.svg \
    imgs/prncss_logo_gold.svg \
    imgs/throbber.png \
    qml/MouseTrap.qml\
    qml/AnimatedSprite.qml \
    qml/Checkbox.qml \
    qml/GameRules.qml \
    qml/SpeechBubble.qml \
    js/CampaignScript.js \
    qml/ConfigurableDialog.qml \
    qml/Fireworks.qml \
    qml/Firework.qml \
    eliza.js \
    prncss.rc \
    qml/ShadowedText.qml
