#ifndef TYPES_H
#define TYPES_H
#include <QDebug>
#include <QTextStream>
#include <QString>

const QString NORTH_GOAL_STRING = "North Goal";
const QString SOUTH_GOAL_STRING = "South Goal";

enum MoveDirection {Up, Down, Left, Right};
enum GamePiece {NoPiece = 0, SinglePiece, DoublePiece, TriplePiece};
///Which side of the board the player is on (north vs. south)
enum PlayerSide {UnknownSide, NorthSide, SouthSide};
enum GameOverState {InProgress, NorthWin, SouthWin, OpponentQuit};
/**
 * The reason a move is bad.
 * BMRValidMove: The move is valid
 * BMROutOfBounds: The start position is not on the board
 * BMRReplaceOutOfBounds: Tried to replace a piece to somewhere outside the playable area
 * BMRMissingStartPiece: There is no piece at the start position of this move
 * BMRNotOnHomeRow: Tried to move a piece that is not on your home row
 * BMRMoveOffBoard: Either you moved off the side of the board, or you moved into your own goal space
 * BMRCollision: Tried to move into an occupied square with moves remaining
 * BMRHitBreadcrumb: Tried to move over a breadcrumbed location
 * BMRIncompleteMove: There are not enough directions to complete the piece's move
 * BMRTooManyDirections: There are more directions than the piece can move
 * BMRMissingReplacePiece: Tried to do a replace, but there is no piece to replace
 * BMRReplaceCollision: Tried to replace a piece to an occupied position
 * BMRSameBoard: The move has not modified the board state
 * BMRGameOver: The game is already over
 */
enum BadMoveReason {BMRValidMove, BMROutOfBounds, BMRReplaceOutOfBounds, BMRMissingStartPiece, BMRNotOnHomeRow, BMRMoveOffBoard, BMRCollision, BMRHitBreadcrumb, BMRIncompleteMove, BMRTooManyDirections, BMRMissingReplacePiece, BMRReplaceCollision, BMRSameBoard, BMRGameOver};

const short BOARDSIZE = 6;

/**
 * The purpose of this class is to represent an initial home row, at the beginning of the game.
 * Access is through the [] operator, which expects a char in the A to F range. If any other value is supplied,
 * row A is returned.
 */
class HomeRow
{
public:
    HomeRow(){}
    HomeRow(QString rowString);

    operator QString() const;

    GamePiece& operator[] (char column) {
        if (isxdigit (column))
            return row[column - 'A'];
        return row[0];
    }
    const GamePiece& operator[] (char column) const {
        if (isxdigit (column))
            return row[column - 'A'];
        return row[0];
    }
    
    HomeRow& Rotate() {
        for (quint8 i = 0; i < BOARDSIZE/2; ++i)
            std::swap(row[i], row[BOARDSIZE-i-1]);
        return *this;
    }

private:
    GamePiece row[BOARDSIZE];
};

/**
 * This class represents a position on the board
 */
class BoardPosition
{
public:
    BoardPosition() : row (0), column (0), inGoal (false), whichGoal(SouthSide) {}
    BoardPosition (char column, short row) : inGoal (false), whichGoal(SouthSide) {
        SetColumn (column);
        SetRow (row);
    }
    BoardPosition (QString coordinates) : inGoal (false), whichGoal(SouthSide) {
        if(coordinates == NORTH_GOAL_STRING)
            setInGoal(NorthSide);
        else if(coordinates == SOUTH_GOAL_STRING)
            setInGoal(SouthSide);
        else
        {
            SetColumn (coordinates[0].toAscii());
            SetRow (coordinates.remove (0, 1).toShort());
        }
    }
    BoardPosition (PlayerSide goalSide)
    {
        setInGoal(goalSide);
    }
    BoardPosition (int numberPosition) {
        column = numberPosition % 10 - 1;
        SetRow(7 - (numberPosition / 10));
    }

    bool operator < (const BoardPosition& position) const {
        if (row < position.row)
            return true;
        if (column < position.column)
            return true;
        return false;
    }
    bool operator== (const BoardPosition& other) const {
        if (row == other.row && column == other.column)
            return true;
        return false;
    }
    bool operator!= (const BoardPosition& other) const {
        if (row != other.row || column != other.column)
            return true;
        return false;
    }
    
    operator QString() const {
        QString result;
        QTextStream stream(&result);
        stream << GetColumn() << GetRow();
        return result;
    }
    operator QString()
    {
        QString out;
        QTextStream outStream(&out);

        if (inGoal) {
            if (whichGoal == NorthSide)
                outStream << NORTH_GOAL_STRING;
            else
                outStream << SOUTH_GOAL_STRING;
        } else {
            outStream << GetColumn() << GetRow();
        }

        return out;
    }
    operator int() const {
        return ((row + 1) * 10) + (column + 1);
    }

    /**
     * @brief Retrieve this position's row in the 1-6 range
     * The rows are enumerated 1 to 6 from bottom to top
     * A row of 0 or 7 represents the goal positions; see InGoal()
     * @return Row as a number between 1 and 6
     */
    short GetRow() const {
        return BOARDSIZE - row;
    }
    /**
     * @brief Set this position's row
     * If row is 0 or 7, position is set to the appropriate goal space, and column is set arbitrarily to A.
     * Note that the column is meaningless until the position is moved out of the goal space.
     * @param row The row, from 0 to 7
     */
    void SetRow (short row) {
        this->row = BOARDSIZE - row;

        if (row == -1 || row == BOARDSIZE + 1) {
            inGoal = true;
            column = 0;
            whichGoal = (row == -1) ? NorthSide : SouthSide;
        } else
            inGoal = false;
    }
    /**
     * @brief Retrieve this position's column in the A-F range
     * The columns are enumerated A to F from left to right
     * @return Column as a letter between A and F
     */
    char GetColumn() const {
        return column + 'A';
    }
    /**
     * @brief Set this position's column
     * @param column The column, from A to F
     */
    void SetColumn (char column) {
        this->column = column - 'A';
    }
    /**
     * @brief Determine whether this position is in a goal or not
     * @return True if position is in either goal
     */
    bool InGoal() const {
        return inGoal;
    }
    /**
     * @brief Which goal this position is in
     * Do not use this method unless InGoal() is true!
     * @return Which goal this position is in
     */
    PlayerSide WhichGoal() const {
        return whichGoal;
    }

    /**
     * @brief Move this BoardPosition in a given direction
     * @note If moved out of a goal, the column is arbitrarily set to A; if moved left or right in a goal, the move is ignored
     * @note If moved out of bounds, the BoardPosition remains unchanged
     * @param direction The direction to move in
     * @param noedgecheck Do not check if this move will go over the edge; just do it
     * @return This BoardPosition
     */
    BoardPosition& MovePosition (MoveDirection direction, bool noedgecheck = false) {
        switch (direction) {
        case Up:
            inGoal = false;

            if (row > 0 || noedgecheck)
                --row;
            else {
                row = -1;
                column = 0;
            }
            if (row == -1) {
                inGoal = true;
                whichGoal = NorthSide;
            }
            break;
        case Down:
            inGoal = false;

            if (row < BOARDSIZE - 1 || noedgecheck)
                ++row;
            else {
                row = BOARDSIZE;
                column = 0;
            }
            if (row == BOARDSIZE) {
                inGoal = true;
                whichGoal = SouthSide;
            }
            break;
        case Left:
            if ((column > 0 && !inGoal) || noedgecheck)
                --column;
            break;
        case Right:
            if ((column < BOARDSIZE - 1 && !inGoal) || noedgecheck)
                ++column;
            break;
        }

        return *this;
    }
    /**
     * @brief Get a new BoardPosition that is in the direction specified
     * This method does not modify the current board position; it makes a new one
     * @param direction The direction to move in
     * @param noedgecheck Do not check if this move will go over the edge; just do it
     * @return A BoardPosition one move in the specified direction
     */
    BoardPosition PeekDirection (MoveDirection direction, bool noedgecheck = false) const {
        return BoardPosition(*this).MovePosition(direction, noedgecheck);
    }
    
    /**
     * @brief Rotate this position so that it would refer to the same piece on a rotated board
     */
    void Rotate() {
        row = BOARDSIZE - 1 - row;
        column = BOARDSIZE - 1 - column;
        
        if (inGoal)
            whichGoal = (whichGoal == NorthSide)?SouthSide:NorthSide;
    }

    friend class Board;

private:
    void setInGoal(PlayerSide goalSide)
    {
        row = (goalSide == NorthSide ? -1 : BOARDSIZE);
        column = 0;
        inGoal = true;
        whichGoal = goalSide;
    }

    short row;
    short column;
    bool inGoal;
    PlayerSide whichGoal;
};
struct BreadCrumb {
    BreadCrumb (const BoardPosition& f, const BoardPosition& s) :
        first (f), second (s) {}

    BoardPosition first;
    BoardPosition second;

    bool operator == (const BreadCrumb& other) const {
        if ((first == other.first && second == other.second) ||
            (first == other.second && second == other.first))
            return true;
        return false;
    }
};
/**
 * A base class for exception types
 */
class PRNCSSException
{
public:
    const char* what() {
        return reason;
    }
protected:
    PRNCSSException (const char* reason) : reason (reason) {}
    const char* reason;
};

QDebug operator<< (QDebug dbg, PlayerSide p);
QDebug operator<< (QDebug dbg, BadMoveReason r);
QDebug operator<< (QDebug dbg, MoveDirection d);
QDebug operator<< (QDebug dbg, BoardPosition p);
QDebug operator<< (QDebug dbg, GameOverState s);

#endif
