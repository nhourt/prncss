import QtQuick 1.0

Item {
    id: container
    anchors.fill: parent
    property string log_tag: "GuestList.qml: "

    signal gotName(string playerName)

    function prepStart() {
        var nameText = name.text
        alert.text = ""
        if(nameText != "")
            gotName(nameText)
        else
            alert.text = "Please give a name"
    }

    Rectangle {
        anchors.fill: parent
        color: "transparent"

        Image {
            id: logo
            source: "../imgs/prncss_logo_gold_outlined.svg"; smooth: true
            fillMode: Image.PreserveAspectFit

            anchors { top: parent.top; bottom: alert.top; horizontalCenter: parent.horizontalCenter }
        }

        Text {
            id: alert
            color: "red"
            y: container.height / 2
            anchors { bottom: inputBox.top; horizontalCenter: parent.horizontalCenter }
        }

        Rectangle {
            id: inputBox
            border.color: "black"
            width: inputLabel.width + name.width; height: inputLabel.height + 8
            radius: 3
            anchors {horizontalCenter: parent.horizontalCenter }
            Text {
                id: inputLabel
                text: qsTr("Name:")
                anchors.leftMargin: 3
                anchors.left: inputBox.left
                anchors.verticalCenter: parent.verticalCenter
            }
            TextInput {
                id: name
                width: 100

                anchors.topMargin: 30
                anchors.margins: 3
                anchors.left: inputLabel.right
                anchors.verticalCenter: inputLabel.verticalCenter
            }
        }

        Button {
            id: playButton
            text: "Play"
            anchors.top: inputBox.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            onButtonClicked: prepStart()
        }
    }
}
