import QtQuick 1.1
import Effects 1.0

Item {
    id:container
    property string log_tag: "main.qml: "
    property int animationLength: 400
    property int introAnimationLength: 2500
    width: 800; height: 800
    anchors.fill: parent

    function showImages() {
        logo.opacity = 1
        background.opacity = 1
        penelope.opacity = 1
        penelopeShadow.opacity = .3
        jessica.opacity = 1
        jessicaShadow.opacity = .3
    }

    Loader {
        id: splashLoader
        anchors.fill: parent
        z: 100
        sourceComponent: SplashScreen {
            id: splashScreen
            onFullyOpaque: { showImages(); container.state = "PreIntro" }
            onSplashCompleted: { container.state = "ProfileSelection"; introSkipper.enabled = true; profileSelect.takeFocus(); splashLoader.sourceComponent = undefined }
            animateLogo: true
        }
    }

    Item {
        id: background
        anchors.fill: parent
        z: -1000
        opacity: 0
        Image {
            id: backgroundImage
            source: "../imgs/background.png"; smooth: true
            fillMode: Image.PreserveAspectCrop
            anchors.fill: parent
            z: -100
        }
    }

    MouseArea {
        id: introSkipper
        enabled: false
        anchors { left: undefined; right: undefined; top: undefined; bottom: undefined }
        onClicked: { container.introAnimationLength = 0 }
    }

    Image {
        id: logo
        opacity: 0
        source: "../imgs/prncss_logo_gold_outlined.svg"; smooth: true
        fillMode: Image.PreserveAspectFit
        height: 350
        z: gameboard.z - 5

        anchors { top: parent.top; topMargin: 15; horizontalCenter: parent.horizontalCenter }

        effect: DropShadow {
            color: "#AA000000"
            blurRadius: 8
            offset: Qt.point(0,5)
        }
    }

    Image {
        id: penelope
        opacity: 0
        source: ":/imgs/penelope.png"
        fillMode: Image.PreserveAspectFit
        height: container.height * 0.8
        anchors { verticalCenter: parent.verticalCenter; right: logo.left }
        z: logo.z + 1
    }

    Image {
        id: penelopeShadow
        opacity: 0
        source: "../imgs/penelopeShadow.png"
        fillMode: Image.PreserveAspectFit
        height: container.height * 0.8
        anchors { top: penelope.top; bottom: penelope.bottom; right: penelope.right }
        z: penelope.z - 1
    }

    Image {
        id: jessica
        opacity: 0
        source: "../imgs/jessica.png"
        fillMode: Image.PreserveAspectFit
        height: container.height * 0.8
        anchors { verticalCenter: parent.verticalCenter; left: logo.right }
        z: logo.z + 1
    }

    Image {
        id: jessicaShadow
        opacity: 0
        source: "../imgs/jessicaShadow.png"
        fillMode: Image.PreserveAspectFit
        height: container.height * 0.8
        anchors { top: jessica.top; bottom: jessica.bottom; left: jessica.left }
        z: penelope.z - 1
    }

    Rectangle {
        id: menuContainer
        color: "transparent"
        anchors.top : logo.bottom
        anchors.left: penelope.right
        anchors.right: jessica.left
        anchors.bottom: container.bottom


        ProfileSelect {
            id: profileSelect
            objectName: "profileSelect"
            anchors.fill: parent
            opacity: 0

            onProfileChosen: profileManager.loadProfile(profileName);

            Behavior on opacity {
                PropertyAnimation { duration: animationLength }
            }
        }

        Profile {
            id: profile
            objectName: "profile"
            anchors.fill: parent
            opacity: 0

            onProfileChangeClicked: { container.state = "ProfileSelection" }
            onCancelClicked: { container.state = "MainMenu" }

            Behavior on opacity {
                PropertyAnimation { duration: animationLength }
            }
        }

        MainMenu {
            id: mainMenu
            objectName: "mainMenu"
            anchors.fill: parent
            opacity: 0

            onProfileButtonClicked: { container.state = "Profile" }

            Behavior on opacity {
                PropertyAnimation { duration: animationLength }
            }

            onChooseCatFightSide: {
                catFightChooseSide.show()
            }
            onUpdateToolTip: {
                toolTip.textBuffer = newText;
            }
        }

        NetworkPlayerList {
            id: networkplayerlist
            objectName: "playerList"
            width: parent.width / 2
            anchors.centerIn: parent
            opacity: 0

            Behavior on opacity {
                PropertyAnimation { duration: animationLength }
            }

            onRequestPlay: networkListDialog.showWaitingForResponse(name)
            onLeave: container.state = "MainMenu"         
        }
    }

    ConfigurableDialog {
        id: catFightChooseSide
        objectName: "catFightChooseSide"
        z: networkListDialog.z

        text: "Choose a side"
        hasButton1: true; button1Text: "South"
        hasButton2: true; button2Text: "North"
        hasThrobber: false

        onButton1Clicked: {
            hide()
            mainMenu.startCatFight("South")
        }

        onButton2Clicked: {
            hide()
            mainMenu.startCatFight("North")
        }


    }

    ConfigurableDialog {
        id: networkListDialog
        objectName: "networkListDialog"
        z: networkplayerlist.z + 10

        signal respondToPlayRequest(bool reply)
        signal sideChosen(string reply)

        property string __opponentName
        property string __dialogType

        function showWaitingForResponse(opponentName) {
            __opponentName = opponentName;
            __dialogType = "responseWait";
            hasButton1 = false;
            hasButton2 = false;
            text = "Waiting for " + opponentName + " to respond..."
            hasThrobber = true;

            show();
        }

        function showPlayRequest(opponentName) {
            __opponentName = opponentName;
            __dialogType = "playRequest";
            hasButton1 = true;
            hasButton2 = true;
            button1Text = "Yes";
            button2Text = "No";
            text = "Would you like to play with " + opponentName + "?"
            hasThrobber = false;

            show();
        }

        function showChooseSide() {
            __dialogType = "chooseSide";
            hasButton1 = true;
            hasButton2 = true;
            button1Text = "South";
            button2Text = "North";
            text = __opponentName + " has accepted your request. Would you like to play from North or South?";
            hasThrobber = false;

            show();
        }

        function showRejection() {
            __dialogType = "rejection";
            hasButton1 = true;
            hasButton2 = false;
            button1Text = "Awwww...";
            text = __opponentName + " has rejected your request.";
            hasThrobber = false;

            show();
        }

        function showWaitingForGame() {
            __dialogType = "gameWait";
            hasButton1 = false;
            hasButton2 = false;
            text = "Waiting for the game to begin...";
            hasThrobber = true;

            show();
        }

        function __button1Clicked() {
            if (__dialogType === "playRequest") {
                respondToPlayRequest(true);
                showWaitingForGame();
            } else if (__dialogType === "chooseSide") {
                sideChosen("South");
            } else if (__dialogType === "rejection") {
                hide();
            }
        }

        function __button2Clicked() {
            if (__dialogType === "playRequest") {
                respondToPlayRequest(false);
                hide();
            } else if (__dialogType === "chooseSide") {
                sideChosen("North");
            }
        }

        onButton1Clicked: __button1Clicked()
        onButton2Clicked: __button2Clicked()
    }

    ConfigurableDialog {
        id: tutorialPromptDialog
        hasThrobber: false
        text: "It looks like you're new here. In that case, it is highly recommended that you play the tutorial first.\n\
Would you like to do this now?"
        hasButton1: true
        button1Text: "Sure"
        hasButton2: true
        button2Text: "No thanks"
        dialogWidth: 675
        dialogHeight: 130

        onButton1Clicked: {
            hide()
            mainMenu.startTutorial()
        }
        onButton2Clicked: hide()

        Connections {
            target: profileManager
            onNewProfileCreated: tutorialPromptDialog.show()
        }
    }

    Text {
        id: toolTip
        anchors.bottom: container.bottom
        anchors.bottomMargin: 15
        anchors.horizontalCenter: container.horizontalCenter
        color: "white"
        opacity: visibleOpacity
        style: Text.Raised
        font.pointSize: 25
        font.bold: true

        property real visibleOpacity: .8
        property string textBuffer

        SequentialAnimation {
            id: changeTextAnimation

            PropertyAnimation {
                target: toolTip
                property: "opacity"
                from: toolTip.opacity; to: 0
            }
            ScriptAction {
                script: toolTip.text = toolTip.textBuffer
            }
            PropertyAnimation {
                target: toolTip
                property: "opacity"
                from: 0; to: toolTip.visibleOpacity
            }
        }

        onTextBufferChanged: changeTextAnimation.restart()
    }

    GameBoard {
        objectName: "gameBoard"
        id: gameboard
        anchors.left: container.left
        anchors.right: container.right
        height: container.height

        y: -container.height - 30
    }

    state: "PreIntro"
    states: [
        State {
            name: "PreIntro"
            PropertyChanges {
                target: introSkipper
                width: container.width; height: container.height
            }
            AnchorChanges {
                target: logo
                anchors { top: undefined; verticalCenter: parent.verticalCenter }
            }
            PropertyChanges {
                target: logo
                height: 0
                anchors.verticalCenterOffset: -500
            }
            AnchorChanges {
                target: penelope
                anchors { right: parent.left }
            }
            AnchorChanges {
                target: jessica
                anchors { left: parent.right }
            }
            AnchorChanges {
                target: menuContainer
                anchors { top: parent.bottom }
            }
        },
        State {
            name: "ProfileSelection"
            PropertyChanges {
                target: profile
                opacity: 0
            }
            PropertyChanges {
                target: profileSelect
                opacity: 1
            }

            PropertyChanges {
                target: mainMenu
                opacity: 0
            }

            PropertyChanges {
                target: networkplayerlist
                opacity: 0
            }

            PropertyChanges {
                target: gameboard
                y: -container.height - 30
            }
        },
        State {
            name: "Profile"
            PropertyChanges {
                target: profile
                opacity: 1
            }
            PropertyChanges {
                target: profileSelect
                opacity: 0
            }

            PropertyChanges {
                target: mainMenu
                opacity: 0
            }

            PropertyChanges {
                target: networkplayerlist
                opacity: 0
            }

            PropertyChanges {
                target: gameboard
                y: -container.height - 30
            }
        },
        State {
            name: "MainMenu"
            PropertyChanges {
                target: profile
                opacity: 0
            }

            PropertyChanges {
                target: profileSelect
                opacity: 0
            }

            PropertyChanges {
                target: mainMenu
                opacity: 1
            }

            PropertyChanges {
                target: networkplayerlist
                opacity: 0
            }

            PropertyChanges {
                target: gameboard
                y: -container.height - 30
            }
        },
        State {
            name: "NetworkList"
            PropertyChanges {
                target: profile
                opacity: 0
            }

            PropertyChanges {
                target: profileSelect
                opacity: 0
            }

            PropertyChanges {
                target: mainMenu
                opacity: 0
            }

            PropertyChanges {
                target: networkplayerlist
                opacity: 1
            }

            PropertyChanges {
                target: gameboard
                y: -container.height - 30
            }
        },
        State {
            name: "GameBoard"
            PropertyChanges {
                target: gameboard
                y: 0
            }
            PropertyChanges {
                target: mainMenu
                enabled: false
            }
        }
    ]

    transitions: [
        Transition {
            from: "*"
            to: "GameBoard"

            PropertyAnimation {
                target: gameboard
                property: "y"
                duration: 1000
                easing.type: Easing.OutBounce
                from: -container.height - 30; to: 0
            }
            PropertyAction {
                target: gameboard
                property: "active"
                value: "true"
            }
        },
        Transition {
            from: "GameBoard"
            to: "*"

            PropertyAnimation {
                target: gameboard
                property: "y"
                duration: 1000
                easing.type: Easing.InCubic
                from: 0; to: container.height + 30
            }
            PropertyAction {
                target: gameboard
                property: "active"
                value: "false"
            }
        },
        Transition {
            from: "PreIntro"
            to: "ProfileSelection"
            SequentialAnimation {
                id: introAnimation
                PropertyAnimation {
                    target: logo
                    property: "height"
                    from: 0; to: 1000
                    easing.type: Easing.OutBack
                    duration: introAnimationLength / 2
                }
                PauseAnimation { duration: introAnimationLength * 1.5 }
                ParallelAnimation {
                    PropertyAnimation {
                        target: logo
                        property: "height"
                        easing.type: Easing.InOutBack
                        duration: introAnimationLength
                    }
                    AnchorAnimation {
                        targets: logo
                        easing.type: Easing.InOutBack
                        duration: introAnimationLength
                    }
                }
                ParallelAnimation {
                    AnchorAnimation {
                        targets: penelope
                        duration: introAnimationLength / 2
                        easing.type: Easing.OutBack
                    }
                    AnchorAnimation {
                        targets: jessica
                        duration: introAnimationLength / 2
                        easing.type: Easing.OutBack
                    }
                }
                AnchorAnimation {
                    targets: menuContainer
                    duration: introAnimationLength / 2
                    easing.type: Easing.OutBounce
                }
                PropertyAnimation {
                    target: introSkipper
                    properties: "width,height"
                    to: 0
                    duration: 0
                }
            }
        }
    ]

    Component.onCompleted: {
        splashLoader.forceActiveFocus()
    }
}
