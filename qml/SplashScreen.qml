import QtQuick 1.1
import Qt.labs.particles 1.0

Rectangle {
    id: container
    anchors.fill: parent
    width: 500; height: 300

    signal splashCompleted()
    signal fullyOpaque()

    property alias animateLogo: animationTimer.running

    function endSplash() {
        logoAnimation.stop();
        container.visible = false;
    }

    onVisibleChanged: if (!visible) {splashCompleted()}

    color: "transparent"

    Rectangle {
        id: blacker
        anchors.fill: container
        color: "black"
        opacity: 0
    }

    Item {
        id: logoContainer
        anchors.centerIn: parent
        width: ipFire.width + ipLogo.width
        scale: 0
        Image {
            id: ipFire
            source: "../imgs/incendiary_logo_fire.svg"; smooth: true
            anchors { right: ipLogo.left; verticalCenter: parent.verticalCenter }

            Timer {
                id: animationTimer
                interval: 250
                onTriggered: ipFire.mirror = !ipFire.mirror
                repeat: Animation.Infinite
            }
        }
        Image {
            id: ipLogo
            source: "../imgs/incendiary_logo_white_text.svg"; smooth: true
            anchors { verticalCenter: ipFire.verticalCenter; right: parent.right }
            fillMode: Image.PreserveAspectFit
            z: fire.z + 1
        }
    }

    /*
    Particles {
        id: particles
        y: parent.height / 2; x: parent.width / 2
        width: 0; height: 0
        z: blacker.z + 1

        source: "../imgs/red_particle.png"
        count: 3000
        emissionRate: 1000
        lifeSpan: 1000000
        angle: -45
        angleDeviation: 20
        velocity: 150
        velocityDeviation: 600

        ParticleMotionGravity {
            acceleration: 300
            yattractor: container.height / 2
            xattractor: container.width / 2
        }
    }
    */

    Particles {
        id: fire
        anchors { bottom: parent.bottom }
        width: parent.width
        z: sparks.z + 1

        source: "../imgs/red_particle.png"
        count: 5000
        emissionRate: 3000
        lifeSpan: 800
        lifeSpanDeviation: 200
        fadeInDuration: 0
        fadeOutDuration: 1000
        angle: -90
        angleDeviation: 20
        velocity: 200
        velocityDeviation: 20

        ParticleMotionGravity {
            acceleration: 9.8
            yattractor: container.height * 2
        }
    }

    Particles {
        id: smoke
        anchors { bottom: parent.bottom }
        width: parent.width
        z: blacker.z + 1

        source: "../imgs/smoke_particle.png"
        count: 300
        emissionRate: 100
        lifeSpan: 2000
        lifeSpanDeviation: 200
        fadeOutDuration: 1000
        angle: -80
        angleDeviation: 10
        velocity: 150
        velocityDeviation: 20
    }

    Particles {
        id: sparks
        anchors.bottom: parent.bottom
        width: parent.width
        z: smoke.z + 1

        source: "../imgs/spark.png"
        count: 3000
        emissionRate: 500
        lifeSpan: 1000
        angle: 270
        angleDeviation: 100
        velocity: 300
        velocityDeviation: 600

        ParticleMotionWander {
            xvariance: 300
            pace: 1000
        }
    }

    SequentialAnimation {
        id: logoAnimation
        PauseAnimation { duration: 1000 }
        ParallelAnimation {
            NumberAnimation {
                target: logoContainer
                property: "scale"
                duration: 2000
                from: 0; to: 1
                easing.type: Easing.InOutCubic
            }
            NumberAnimation {
                target: blacker
                property: "opacity"
                duration: 2000
                from: 0; to: 1
                easing.type: Easing.InOutBounce
            }
        }

        ScriptAction { script: container.fullyOpaque() }

        PauseAnimation { duration: 3000 }

        NumberAnimation {
            target: container
            property: "opacity"
            to: 0
            duration: 1000
            easing.type: Easing.InOutQuad
        }

        onCompleted: endSplash()
    }

    Component.onCompleted: {
        logoAnimation.start()
    }
}
