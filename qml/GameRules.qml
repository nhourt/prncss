// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Fadeable {
    id: gamerules
    anchors.fill: parent

    signal closeClicked()

    MouseTrap {
        z: 1
        opacity: .5
        onClicked: gamerules.closeClicked()
    }

    Rectangle{
        id: container

        width: 650
        height: 500
        color: "#AA222222"
        z: 2

        anchors.centerIn: parent

        property int titlefontsize: 20
        property int rulesfontsize: 14
        property string rules: ""
        property int selectedrule: 1

        MouseArea {
            anchors.fill: parent
        }

        Rectangle{
            id: title

            width: parent.width
            height: 50
            color: "#dd222222"

            Text{
                id: rule

                text: "Help"
                anchors.centerIn: parent
                font.pointSize: container.titlefontsize
                color: "white"
            }

            Button{
                id: buttonclose
                text: "Close"
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                onButtonClicked: {gamerules.closeClicked();}
            }
        }

        ListModel {
            id: rulecategory
            ListElement {
                name: "About Gygès"
                image: ""
                description: "Gygès is an abstract strategy game originating in France. Its complex rules and limitless opportunities for strategy allow for an exhilarating gaming experience every time."
            }
            ListElement{
                name: "Game Screen"
                image: ""
                description: "The game board is in the center of the screen. A chat/game log is located on the right below Jessica's head. On the top left is a boom box where songs and volume can be controlled."
            }
            ListElement{
                name: "Game Board"
                image: ""
                description: "The game board is a 6 x 6 layout of spaces with a goal at both the North (top) and South (bottom) ends. \
                Move your pieces through the spaces to get to the goal on the opposite end of your home row."
            }
            ListElement {
                name: "Pieces"
                image: ""
                description: "There are 12 pieces in the game: 4 one pieces, 4 two pieces, and 4 three pieces.  The one pieces can only move 1 space, \
                the two pieces can move 2 spaces, and the three pieces can move 3 spaces.  \
                Any piece can be used by any player. Only the pieces in the row closest to you can be moved."
            }
            ListElement {
                name: "Selecting a home row"
                image: ""
                description: "The first thing you will do when you start a game is select your home row.  Your home row is the starting position for \
                your pieces at the beginning of the game.  You can change your home row by dragging the pieces to different positions in the home row order."
            }
            ListElement {
                name: "Don't click here"
                image: "../imgs/followdirections.jpg"
                description: ""
            }
            ListElement {
                name: "Making a Move"
                image: ""
                description: "Click a piece to select it, then click the space you would like to move to."
            }
            ListElement{
                name: "Bouncing"
                image: ""
                description: "Click a piece to select it, then, without clicking on the piece you intend to bounce on, move that many additional spaces. \
                Click on the space you would like to move to using the total number of spaces you are able to move."
            }
            ListElement{
                name: "Replacing"
                image: ""
                description: "Click a piece to select it, then click the piece you would like to replace. Finally, click space you would like to move that replaced piece to."
            }
            ListElement{
                name: "Blocking"
                image: ""
                description: "This is a strategy move to prevent the opponent from winning. The best ways to do this are to either place a piece in their path to win, thus blocking it, \
                or to replace a piece in the path with a piece of a different number, thus breaking the path."
            }
            ListElement{
                name: "Cheating"
                image: ""
                description: "PRNCSS has several cheat codes built in. Cheats are used by typing a special message into the chat box\
 and pressing enter. The recognized cheat messages include: <ul><li><em>Do I die?</em>&nbsp;&nbsp;&nbsp;Determine if the currently selected move gives the\
 opponent a win path</li><li><em>halp</em>&nbsp;&nbsp;&nbsp;Enlist the hard AI to choose a move for you</li><li><em>I'm so over this</em>&nbsp;&nbsp;&nbsp;Skip to the\
 next level in campaign mode</li><li><em>Do a barrel roll</em>&nbsp;&nbsp;&nbsp;I'll give you one guess...</li></ul> There may or may not be other cheats\
 recognized as well."
            }
            ListElement{
                name: "Winning"
                image: ""
                description: "In order to win, a player must get a piece from their back row into the opposing side's goal space. This must be done with one piece by bouncing on \
                other pieces the entire way across the board."
            }
            ListElement{
                name: "Boom Box"
                image: ""
                description: "The boom box, located at the top left of the screen, controls the background music. You can pause a song, skip to the next song, or go back to the  \
                previous song. The volume of the music may also be changed here using the volume control slider."
            }
            ListElement{
                name: "Network Chat"
                image: ""
                description: "During a Network game against another player, you can chat with the other player by clicking the chat box on the bottom of the screen and typing \
                in it. Press the Send button or press Enter to send the message."
            }
        }

        Component {
            id: ruledelegate
            Item {
                width: selectrules.width
                height: ruletext.height

                Text {
                    id: ruletext
                    text: name
                    font.pointSize: container.rulesfontsize
                    wrapMode: Text.WordWrap
                    color: "white"
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: { selectrules.currentIndex = index }
                }
            }
        }

        ListView {
            id: selectrules
            model: rulecategory
            spacing: 2
            delegate: ruledelegate
            width: parent.width / 3
            height: parent.height - title.height
            anchors.top: title.bottom
            anchors.left: parent.left
            anchors.leftMargin: 3
            highlight: Rectangle { color: "#FFAE005F"; radius: 5 }
            keyNavigationWraps: true
            focus: true
            highlightFollowsCurrentItem: true
            boundsBehavior: Flickable.StopAtBounds
        }
        Rectangle {
            id: ruledescription
            color: "transparent"
            opacity: .5
            anchors.top: title.bottom
            anchors.bottom: container.bottom
            anchors.left: selectrules.right
            anchors.right: parent.right

            Rectangle {
                id: textBorder
                width: 5
                color: "black"

                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
            }

            Image {
                id: ruleImage
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: selectrules.model.get(selectrules.currentIndex).image
            }

            Text {
                id: description
                wrapMode: Text.WordWrap
                font.pointSize: container.rulesfontsize
                color: "white"
                anchors.left: textBorder.right
                anchors.leftMargin: 3
                anchors.right: parent.right
                anchors.rightMargin: 3
                textFormat: Text.RichText

                SequentialAnimation {
                    id: changeTextAnimation

                    PropertyAnimation {
                        target: description
                        property: "opacity"
                        from: 1; to: 0
                        duration: 300
                    }
                    ScriptAction { script: description.text = description.textBuffer }
                    PropertyAnimation {
                        target: description
                        property: "opacity"
                        from: 0; to: 1
                        duration: 300
                    }
                }

                property string textBuffer: selectrules.model.get(selectrules.currentIndex).description
                onTextBufferChanged: {
                    changeTextAnimation.restart()
                }
            }
        }
    }
}
