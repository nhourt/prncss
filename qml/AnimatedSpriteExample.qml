// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    width: 500; height: 400
    color: "lightGreen"

    AnimatedSprite {
        id: princess
        anchors { left: parent.left; leftMargin: parent.width/4; verticalCenter: parent.verticalCenter }
        source: "../imgs/princess_sprite_sheet.png"; scale: 2
        frameRects: [[4,0,22,40], [26,0,22,40], [48,0,22,40], [70,0,22,40], [92,0,22,40]]
        frameRate: 60
        forwardBack: true
    }

    Item {
        // this is used to give the AnimatedSprite a center of reference
        id:bat
        property variant sprite: sprite
        anchors { right: parent.right; rightMargin: parent.width/4; verticalCenter: parent.verticalCenter }
        width: 56 // max frame width
        AnimatedSprite {
            id: sprite
            anchors.centerIn: parent
            source: "../imgs/bat_sprite_sheet.png"
            frameRects: [[0,0,46,32], [47,0,56,25], [104,0,43,26], [148,0,22,28], [171,0,32,28], [204,0,45,27], [250,0,47,29], [298,0,27,40]]
            frameRate: 10
        }
    }

    Item {
        id: buttons
        width: parent.width; height: backButton.height
        anchors { bottom: parent.bottom; bottomMargin: 15; horizontalCenter: parent.horizontalCenter }
        Rectangle {
            id: backButton
            width: backText.width + 10; height: backText.height + 4
            anchors { right: playPauseButton.left; rightMargin: 20; verticalCenter: playPauseButton.verticalCenter }
            radius: 2
            border.color: "black"
            Text {
                id: backText
                anchors.centerIn: parent
                text: "Back one frame"
            }

            MouseArea {
                onClicked: {
                    princess.skipBackward(1)
                    bat.sprite.skipBackward(1)
                }
                anchors.fill: parent
            }
        }

        Rectangle {
            id: playPauseButton
            width: playPauseText.width + 10; height: playPauseText.height + 4
            anchors { verticalCenter:  parent.verticalCenter; horizontalCenter: parent.horizontalCenter }
            radius: 2
            border.color: "black"
            Text {
                id: playPauseText
                anchors.centerIn: parent
                text: "Pause | Play"
            }

            MouseArea {
                onClicked: {
                    princess.running = !princess.running
                    bat.sprite.running = !bat.sprite.running
                }
                anchors.fill: parent
            }
        }

        Rectangle {
            id: reverseButton
            width: reverseText.width + 10; height: reverseText.height + 4
            anchors { bottom: playPauseButton.top; bottomMargin: 10; horizontalCenter: playPauseButton.horizontalCenter }
            radius: 2
            border.color: "black"
            Text {
                id: reverseText
                anchors.centerIn: parent
                text: "Reverse"
            }

            MouseArea {
                onClicked: {
                    princess.reverse = !princess.reverse
                    bat.sprite.reverse = !bat.sprite.reverse
                }
                anchors.fill: parent
            }
        }

        Rectangle {
            id: forwardButton
            width: forwardText.width + 10; height: forwardText.height + 4
            anchors { left: playPauseButton.right; leftMargin: 20; verticalCenter: playPauseButton.verticalCenter }
            radius: 2
            border.color: "black"
            Text {
                id: forwardText
                anchors.centerIn: parent
                text: "Forward one frame"
            }

            MouseArea {
                onClicked:  {
                    princess.skipForward(1)
                    bat.sprite.skipForward(1)
                }
                anchors.fill: parent
            }
        }
    }

    Text {
        id: princessAlert
        anchors { top: parent.top; horizontalCenter: princess.left }
        text: getPrincessText()
    }

    Text {
        id: batAlert
        anchors { top: parent.top; left: bat.left }
        text: getBatText()
    }

    function getPrincessText() {
        var string =
                "currentFrame: " + princess.currentFrame +
                "\nimageX: " + princess.imageX +
                "\nimageY: " + princess.imageY +
                "\nscale: " + princess.scale +
                "\nreverse: " + princess.reverse +
                "\nrunning: " + princess.running
        return string
    }

    function getBatText() {
        var string =
                "currentFrame: " + bat.sprite.currentFrame +
                "\nimageX: " + bat.sprite.imageX +
                "\nimageY: " + bat.sprite.imageY +
                "\nscale: " + bat.sprite.scale +
                "\nreverse: " + bat.sprite.reverse +
                "\nrunning: " + bat.sprite.running
        return string
    }
}
