import QtQuick 1.1

Item {
    id: profile
    anchors.centerIn: parent
    signal profileChangeClicked
    signal cancelClicked

    onOpacityChanged: {
        if(opacity === 1) {
            container.playername = function() { return profileManager.profileName; };
            container.playereasylosses = function() { return profileManager.easyLosses; };
            container.playereasywins = function() { return profileManager.easyWins; };
            container.playermediumlosses = function() { return profileManager.mediumLosses; };
            container.playermediumwins = function() { return profileManager.mediumWins; };
            container.playerhardlosses = function() { return profileManager.hardLosses; };
            container.playerhardwins = function() { return profileManager.hardWins; };
            container.playernetworklosses = function() { return profileManager.networkLosses; };
            container.playernetworkwins = function() { return profileManager.networkWins; };
            container.playerlevel = function() { return profileManager.campaignLevel; };
        }
    }

    Rectangle {
        id: container
        color: "transparent"

        anchors.fill: parent

        property string playername: profileManager.profileName
        property int playereasywins: profileManager.easyWins
        property int playereasylosses: profileManager.easyLosses
        property int playermediumwins: profileManager.mediumWins
        property int playermediumlosses: profileManager.mediumLosses
        property int playerhardwins: profileManager.hardWins
        property int playerhardlosses: profileManager.hardLosses
        property int playernetworkwins: profileManager.networkWins
        property int playernetworklosses: profileManager.networkLosses
        property int playertotalgames: playereasywins + playereasylosses + playermediumwins + playermediumlosses + playerhardwins + playerhardlosses + playernetworkwins + playernetworklosses
        property int playerfontsize: 20
        property int playerlevel: profileManager.campaignLevel
        property int statfontsize: 14
        //property color playertextcolor: "#FFFF008E"
        property color playertextcolor: "white"
        property color stattextcolor: "white"
        //property color stattextcolor: "#FF464646"
        //property color stattextcolor: "black"

        Connections {
            target: profileManager
            onProfileChanged: {
                container.playername = function() { return profileManager.profileName; };
                container.playereasylosses = function() { return profileManager.easyLosses; };
                container.playereasywins = function() { return profileManager.easyWins; };
                container.playermediumlosses = function() { return profileManager.mediumLosses; };
                container.playermediumwins = function() { return profileManager.mediumWins; };
                container.playerhardlosses = function() { return profileManager.hardLosses; };
                container.playerhardwins = function() { return profileManager.hardWins; };
                container.playernetworklosses = function() { return profileManager.networkLosses; };
                container.playernetworkwins = function() { return profileManager.networkWins; };
            }
        }

        Button {
            id: resetCampaign
            text: "Reset Campaign"
            anchors.horizontalCenter: container.horizontalCenter
            anchors.top: gamestats.bottom
            opacity: container.playerlevel > 0 ? 1 : 0

            Behavior on opacity {
                PropertyAnimation { duration: 750 }
            }

            onButtonClicked: profileManager.campaignLevel = 0
        }

        Button{
            id: switchprofile
            text: "Switch Profile"
            width: resetCampaign.width
            height: resetCampaign.height
            anchors.horizontalCenter: container.horizontalCenter
            anchors.top: resetCampaign.bottom
            onButtonClicked: {profile.profileChangeClicked()}
        }

        Button{
            id: deleteProfile
            text: "Delete Profile"
            width: resetCampaign.width
            height: resetCampaign.height
            anchors.horizontalCenter: container.horizontalCenter
            anchors.top: switchprofile.bottom
            onButtonClicked: {
                profileManager.deleteProfile(container.playername)
                profile.profileChangeClicked()
            }
        }

        Button{
            id: backbutton
            text:  "Back"
            width: resetCampaign.width
            height: resetCampaign.height
            anchors.horizontalCenter: container.horizontalCenter
            anchors.top: deleteProfile.bottom
            onButtonClicked: {profile.cancelClicked()}
        }

        Rectangle {
            id:playerid
            color: "transparent"
            width: parent.width
            height: container.height/10

            anchors.top: parent.top

            ShadowedText {
                id: playerlabel
                text: container.playername
                anchors.centerIn: parent
                font.pointSize: container.playerfontsize
                color: container.playertextcolor
                font.bold: true
                //color: "#FFAE005F"
                //style: Text.Outline
                //styleColor: "#FFFF008E"
            }
        }

        Rectangle {
            id:gamestats
            color: "transparent"
            width: parent.width
            height: grid.height

            anchors.horizontalCenter: container.horizontalCenter
            anchors.top: playerid.bottom

            Grid {
                id: grid
                columns: 4
                spacing: 10
                anchors.horizontalCenter: gamestats.horizontalCenter


                ShadowedText {
                    id: stats
                    text: "Player statistics"
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: statswins
                    text: "Wins"
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: statsloses
                    text: "Loses"
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: statspercentage
                    text: "Win %"
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: vseasy
                    text: "VS easy:"
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: winsvseasy
                    text: container.playereasywins
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: losesvseasy
                    text: container.playereasylosses
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }

                ShadowedText {
                    id: winspercentvseasy
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                    text: {
                        if((container.playereasylosses + container.playereasywins) === 0)
                        {
                            return "0%";
                        }
                        else
                        {
                            return Math.round((container.playereasywins/(container.playereasylosses + container.playereasywins)) * 100) + "%";
                        }
                    }
                }
                ShadowedText {
                    id: vsmedium
                    text: "VS medium:"
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: winsvsmedium
                    text: container.playermediumwins
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: losesvsmedium
                    text: container.playermediumlosses
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }

                ShadowedText {
                    id: winspercentvsmedium
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                    text: {
                        if((container.playermediumlosses + container.playermediumwins) === 0)
                        {
                            return "0%";
                        }
                        else
                        {
                            return Math.round((container.playermediumwins/(container.playermediumlosses + container.playermediumwins)) % 100) + "%";
                        }
                    }
                }
                ShadowedText {
                    id: vshard
                    text: "VS hard:"
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: winsvshard
                    text: container.playerhardwins
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: losesvshard
                    text: container.playerhardlosses
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }

                ShadowedText {
                    id: winspercentvshard
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                    text: {
                        if((container.playerhardlosses + container.playerhardwins) === 0)
                        {
                            return "0%";
                        }
                        else
                        {
                            return Math.round((container.playerhardwins/(container.playerhardlosses + container.playerhardwins)) * 100) + "%";
                        }
                    }
                }
                ShadowedText {
                    id: vsnetwork
                    text: "VS network:"
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: winsvsnetwork
                    text: container.playernetworkwins
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }
                ShadowedText {
                    id: losesvsnetwork
                    text: container.playernetworklosses
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                }

                ShadowedText {
                    id: winspercentvsnetwork
                    font.pointSize: container.statfontsize
                    font.bold: true
                    color: container.stattextcolor
                    text: {
                        if((container.playernetworklosses + container.playernetworkwins) === 0)
                        {
                            return "0%";
                        }
                        else
                        {
                            return Math.round((container.playernetworkwins/(container.playernetworklosses + container.playernetworkwins)) * 100) + "%";
                        }
                    }
                }
            }
        }
    }
}
