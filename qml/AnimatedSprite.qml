// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: root
    width: 100; height: 100

    property int startFrame: 1 // frame that begins animation

    property alias source: image.source // image source
    property variant frameRects //: [[100,100,100,100]] // a list of rects [x,y,width,height]; bounding boxes in sprite sheet

    property alias running: timer.running // pause/play animation
    property int frameRate: 60 // frames per sec

    property bool forwardBack: false // loop animation forward and then back
    property bool reverse: false

    property alias imageX: image.x // debug info
    property alias imageY: image.y // debug info

    property int currentFrame: currentFrameIndex + 1 // current animation frame
    property int currentFrameIndex: 0 // allows for skipping directly to a frame (0 based)

    clip: true

    function skipForward(frameCount) {
        if(!frameCount)
            frameCount = 1
        root.currentFrameIndex = (root.currentFrameIndex + frameCount) % root.frameRects.length

        drawFrame()
    }

    function skipBackward(frameCount) {
        if(!frameCount)
            frameCount = 1
        root.currentFrameIndex -= frameCount

        while(root.currentFrameIndex < 0)
            root.currentFrameIndex += root.frameRects.length

        drawFrame()
    }

    function drawFrame() {
        if(root.frameRects.length - 1 < root.currentFrameIndex)
            return
        var buffer = root.frameRects[root.currentFrameIndex]
        var bufferRect = Qt.rect(buffer[0], buffer[1], buffer[2], buffer[3])

        root.width = bufferRect.width; root.height = bufferRect.height
        image.x = -bufferRect.x; image.y = -bufferRect.y
    }

    Image {
        id: image
        fillMode: Image.PreserveAspectFit
    }

    Timer {
        id: timer
        interval: 1000 / (root.frameRate / 3.6)
        running: true
        repeat: true

        onTriggered: {
            if(root.forwardBack && (root.currentFrame === 1 && reverse || root.currentFrame === root.frameRects.length && !reverse))
                reverse = !reverse
            if(reverse)
                root.skipBackward(1)
            else
                root.skipForward(1)
        }
    }

    Component.onCompleted: {
        // set the initial state
        if(root.startFrame > root.frameRects.length)
            root.currentFrameIndex = root.frameRects.length - 1
        else if(root.startFrame < 1)
            root.currentFrameIndex = 0
        else
            root.currentFrameIndex = root.startFrame - 1
        drawFrame()
    }
}
