import QtQuick 1.1

Item {
    id: container
    property int fadeDuration: 400

    function show() {
        container.state = "shown"
    }

    function hide() {
        container.state = "hidden"
    }

    Behavior on opacity {
        NumberAnimation { duration: fadeDuration }
    }

    state: "hidden"

    states: [
        State {
            name: "shown"
            PropertyChanges { target: container; enabled: true; opacity: 1 }
        },
        State {
            name: "hidden"
            PropertyChanges { target: container; enabled: false; opacity: 0 }
        }

    ]
}
