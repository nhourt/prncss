import QtQuick 1.1

Rectangle {
    id:container
    width: lowerThrobber.width
    height: lowerThrobber.height
    color: "transparent"

    Image {
        id: lowerThrobber
        x: 0
        y: 0
        source: "../imgs/throbber.png"
        smooth: true

        RotationAnimation {
            id: lowerThrobberAnimation
            target: lowerThrobber
            property: "rotation"
            from: 0
            to: 360
            duration: 1100
            loops: Animation.Infinite
            running: true
        }
    }
    Image {
        id: upperThrobber
        x: 0
        y: 0
        z: lowerThrobber.z + 1
        source: "../imgs/throbber.png"
        smooth: true
        mirror: true

        RotationAnimation {
            id: upperThrobberAnimation
            target: upperThrobber
            property: "rotation"
            from: 360
            to: 0
            duration: 1750
            loops: Animation.Infinite
            running: true
        }
    }
}
