import QtQuick 1.1
import Qt.labs.particles 1.0

Item {
    id: root
    anchors.fill: parent

    property variant origin: Qt.point(root.width / 2, root.height)
    property variant target: Qt.point(root.width / 2, root.height / 2)

    function shoot() {
        shootAnimation.start()
    }

    Particles {
        id: trail
        source: "../imgs/particle.png"
        x: origin.x; y: origin.y
        width: 0; height: 0

        angle: 90
        count: 1000
        emissionRate: 0
        lifeSpan: 500
        velocity: 20
        velocityDeviation: 20

        ParticleMotionGravity {
            acceleration: 100
            yattractor: 636000
        }
    }
    Particles {
        id: burst
        source: "../imgs/particle.png"
        x: root.target.x; y: root.target.y
        width: 0; height: 0
        angle: 360
        angleDeviation: 360
        count: 1000
        lifeSpan: 1000
        fadeInDuration: 0
        emissionRate: 0
        velocity: 0
        velocityDeviation: 600

        ParticleMotionGravity {
            acceleration: 9.8
            yattractor: 636000
        }
    }

    SequentialAnimation {
        id: shootAnimation
        ScriptAction { script: trail.emissionRate = 200 }
        ParallelAnimation {
            PropertyAnimation {
                id: xAnimation
                target: trail
                property: "x"
                from: root.origin.x
                to: root.target.x
                duration: 1000
                easing.type: Easing.InOutBack
            }
            PropertyAnimation {
                target: trail
                property: "y"
                from: root.origin.y
                to: root.target.y
                duration: xAnimation.duration
            }
        }
        ScriptAction { script: { burst.burst(1000, 40000); trail.emissionRate = 0 } }
    }
}

