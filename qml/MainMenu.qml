import QtQuick 1.1
import Effects 1.0

Item {
    id: container
    anchors.fill: parent
    property string log_tag: "MainMenu.qml: "

    signal updateToolTip (string newText)
    signal profileButtonClicked
    signal startLocalMultiplayer
    signal startNetworking
    signal startCampaign
    signal startCatFight(string side)
    signal chooseCatFightSide
    signal startTutorial

    property string nextState: ""

    function changeState(state) {
        nextState = state
        animation.start()
    }

    function doStateChange() {
        container.state = nextState
    }

    Rectangle {
        id: mainMenu
        anchors.fill: parent
        color: "transparent"

        property int buttonWidth: Math.max(button1.label.width, button2.label.width, button3.label.width) + 10
        property int buttonHeight: Math.max(button1.label.height, button2.label.height, button3.label.height) + 10



        Rectangle {
            id: mainMenuSlider
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            state: "mainMenuState"
            color: "transparent"
            clip: true

            function slideUp() {
                mainMenuSlider.state = "optionsState"
            }
            function slideDown() {
                mainMenuSlider.state = "mainMenuState"
            }

            states: [
                State {
                    name: "mainMenuState"

                    AnchorChanges {
                        target: mainMenuContainer
                        anchors.top: mainMenuSlider.top
                        anchors.bottom: undefined
                    }

                    PropertyChanges {
                        target: optionsContainer
                        opacity: 0
                        enabled: false
                    }
                },
                State {
                    name: "optionsState"

                    AnchorChanges {
                        target: mainMenuContainer
                        anchors.top: undefined
                        anchors.bottom: mainMenuSlider.top
                    }

                    PropertyChanges {
                        target: optionsContainer
                        opacity: 1
                        enabled: true
                    }
                }
            ]
            transitions: Transition {
                AnchorAnimation { duration: 750; easing.type: Easing.InOutBack }
            }

            Rectangle {
                id: mainMenuContainer
                width: parent.width
                height: childrenRect.height + 30
                z: optionsContainer.z + 1

                anchors { top:mainMenuSlider.top; horizontalCenter: parent.horizontalCenter; }

                color: "transparent"

                Button {
                    id: button1
                    text: "Single Player"
                    width: mainMenu.buttonWidth; height: mainMenu.buttonHeight

                    anchors { top: mainMenuContainer.top; horizontalCenter: parent.horizontalCenter }

                    onButtonClicked: { container.changeState("SinglePlayer"); container.updateToolTip("") }
                    onMouseEntered: container.updateToolTip("Play against the computer")
                    onMouseExited: container.updateToolTip("")
                }

                Button {
                    id: button2
                    text: "Multiplayer"
                    width: mainMenu.buttonWidth; height: mainMenu.buttonHeight

                    anchors { top: button1.bottom; horizontalCenter: parent.horizontalCenter }

                    onButtonClicked: { container.changeState("Multiplayer"); container.updateToolTip("") }
                    onMouseEntered: container.updateToolTip("Play against another person")
                    onMouseExited: container.updateToolTip("")
                }

                Button {
                    id: button3
                    text: "Profile"
                    width: mainMenu.buttonWidth; height: mainMenu.buttonHeight

                    anchors { top: button2.bottom; horizontalCenter: parent.horizontalCenter }

                    //onButtonClicked: mainMenuSlider.slideUp()
                    onButtonClicked: { console.log(log_tag + "Profile button clicked"); container.profileButtonClicked(); container.updateToolTip("") }
                    onMouseEntered: container.updateToolTip("See your profile information, reset story mode progress or switch profiles")
                    onMouseExited: container.updateToolTip("")
                }

                Button {
                    id: button4
                    text: "Tutorial"
                    width: mainMenu.buttonWidth; height: mainMenu.buttonHeight

                    anchors { top: button3.bottom; horizontalCenter: parent.horizontalCenter }

                    onButtonClicked: { container.startTutorial(); container.updateToolTip("") }
                    onMouseEntered: container.updateToolTip("Play a tutorial game (recommended for new players)")
                    onMouseExited: container.updateToolTip("")
                }

                Button {
                    id: button5
                    text: "Options"
                    width: mainMenu.buttonWidth; height: mainMenu.buttonHeight

                    anchors { top: button4.bottom; horizontalCenter: parent.horizontalCenter }

                    onButtonClicked: { mainMenuSlider.slideUp(); container.updateToolTip("") }
                    onMouseEntered: container.updateToolTip("Change sound settings or set the difficulty of Cat Fight games")
                    onMouseExited: container.updateToolTip("")
                }

                Button {
                    id: button6
                    text: "Exit"
                    width: mainMenu.buttonWidth; height: mainMenu.buttonHeight

                    anchors { top: button5.bottom; horizontalCenter: parent.horizontalCenter }

                    onButtonClicked: { console.log(log_tag + "Exit button clicked"); Qt.quit() }
                    onMouseEntered: container.updateToolTip("Quit (your progress will be saved)")
                    onMouseExited: container.updateToolTip("")
                }
            }

            Rectangle {
                id: optionsContainer
                anchors.fill: parent
                color: "transparent"
                z: mainMenuSlider.z + 1
                opacity: 0
                enabled: false

                Behavior on opacity { PropertyAnimation { duration: 750 } }

                Options {
                    color: "transparent"
                    anchors.fill: parent
                    onAcceptClicked: mainMenuSlider.slideDown()
                    onCancelClicked: mainMenuSlider.slideDown()
                }
            }
        }
    }

    states: [
        State {
            name: "SinglePlayer"
            PropertyChanges {
                target: button1
                text: "Story Mode"
                onButtonClicked: { container.startCampaign(); container.updateToolTip("") }
                onMouseEntered: container.updateToolTip("Help Penelope make sure her sweet sixteen is the best party imaginable")
            }
            PropertyChanges {
                target: button2
                text: "Cat Fight"
                onButtonClicked: { container.chooseCatFightSide(); container.updateToolTip("") }
                onMouseEntered: container.updateToolTip("Play a single game immediately (difficulty can be set in Options menu)")
            }
            PropertyChanges {
                target: button3
                text: "Back"
                onButtonClicked: { container.changeState(""); container.updateToolTip("") }
                onMouseEntered: container.updateToolTip("Go back to the main menu")
            }
            PropertyChanges {
                target: button4
                visible: false
            }
            PropertyChanges {
                target: button5
                visible: false
            }
            PropertyChanges {
                target: button6
                visible: false
            }
        },
        State {
            name: "Multiplayer"
            PropertyChanges {
                target: button1
                text: "Network Game"
                onButtonClicked: { container.startNetworking(); container.updateToolTip("") }
                onMouseEntered: container.updateToolTip("Play against another person over the network")
            }
            PropertyChanges {
                target: button2
                text: "Local Game"
                onButtonClicked: { container.startLocalMultiplayer(); container.updateToolTip("") }
                onMouseEntered: container.updateToolTip("Play against another person on this computer")
            }
            PropertyChanges {
                target: button3
                text: "Back"
                onButtonClicked: { container.changeState(""); container.updateToolTip("") }
                onMouseEntered: container.updateToolTip("Go back to the main menu")
            }
            PropertyChanges {
                target: button4
                visible: false
            }
            PropertyChanges {
                target: button5
                visible: false
            }
            PropertyChanges {
                target: button6
                visible: false
            }
        }

    ]

    SequentialAnimation {
        id: animation
        NumberAnimation { targets: [button1, button2, button3, button4, button5, button6]; properties: "opacity"; to: 0; duration: 500; }
        ScriptAction { script: doStateChange() }
        NumberAnimation { targets: [button1, button2, button3, button4, button5, button6]; properties: "opacity"; to: 1; duration: 500; }
    }
}
