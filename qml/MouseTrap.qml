import QtQuick 1.1

Rectangle {
    id: container
    anchors.fill: parent
    color: "black"

    signal clicked()

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onMousePositionChanged: {mouse.accepted = true}
        onClicked: container.clicked()
    }
}
