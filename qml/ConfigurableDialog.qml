import QtQuick 1.1

Fadeable {
    id: container
    anchors.fill: parent

    property bool hasThrobber: true
    property string text: "Configurable Dialog"
    property alias textAlignment: dialogText.horizontalAlignment
    property bool hasButton1: true
    property string button1Text: "Button 1"
    property bool hasButton2: false
    property string button2Text: "Button 2"
    property alias dialogWidth: visibleDialog.width
    property alias dialogHeight: visibleDialog.height

    signal button1Clicked
    signal button2Clicked

    MouseTrap {
        z: 1
        opacity: .5
    }

    Rectangle {
        id: visibleDialog
        width: 500
        height: 100
        anchors.centerIn: parent
        color: Qt.darker("lightgray")
        radius: height/4
        border.width: 2
        border.color: "black"
        z: 2

        property int childMargins: throbber.x

        Throbber {
            id: throbber
            visible: container.hasThrobber
            anchors.verticalCenter: parent.verticalCenter
            x: y
        }

        Text {
            id: dialogText
            text: container.text
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: {
                if (container.hasThrobber)
                    return throbber.right
                else
                    return parent.left
            }
            anchors.leftMargin: container.hasThrobber? 0 : parent.childMargins
            anchors.right: {
                if (container.hasButton1)
                    return button1.left
                else if (hasButton2)
                    return button2.left
                else
                    return parent.right
            }
            anchors.rightMargin: parent.childMargins
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 15
            color: "white"
            style: Text.Outline; styleColor: "black"
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        Button {
            id: button1
            anchors.right: container.hasButton2? button2.left : parent.right
            anchors.rightMargin: parent.childMargins
            anchors.verticalCenter: parent.verticalCenter
            text: button1Text
            visible: container.hasButton1

            onButtonClicked: container.button1Clicked()
        }

        Button {
            id: button2
            anchors.right: parent.right
            anchors.rightMargin: parent.childMargins
            anchors.verticalCenter: parent.verticalCenter
            text: button2Text
            visible: container.hasButton2

            onButtonClicked: container.button2Clicked()
        }
    }
}
