import QtQuick 1.1

Item {
    id: container
    anchors.fill: parent
    anchors.topMargin: 40
    property string log_tag: "NetworkPlayerList.qml: "
    property alias headerText: label.text

    signal requestPlay(string name)
    signal leave()

    function onPeersUpdated(update) {
        var model = peerList.model
        model.clear()
        for(var peer in update) {
            peerList.model.append({ name:update[peer] })
        }
    }

    function onChooseSide() {
        networkListDialog.showChooseSide();
    }

    function requestRejected() {
        networkListDialog.showRejection();
    }

    Rectangle {
        id: header
        width: container.width
        height: label.height * 2
        color: "#dd222222"

        anchors.top: container.top
        anchors.horizontalCenter: container.horizontalCenter

        Text {
            id: label
            font.pointSize: 17
            color: "#FFFF008E"
            text: "Available Players"
            style: Text.Sunken
            anchors.centerIn: parent
        }
    }

    ListModel { id: peerModel; }

    Component {
        id: listDelegate
        Rectangle {
            width: parent.width / 2
            height: name.height * 2
            radius: height / 4
            smooth: true
            anchors.horizontalCenter: parent.horizontalCenter

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#FFFFF466" }
                GradientStop { position: 1.0; color: "#FFFFF201" }
            }

            Text {
                id: name
                text: modelData
                font.letterSpacing: 1
                font.pointSize: 15
                color: "#FF505050"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: container.requestPlay(name.text)
            }
        }
    }

    Rectangle {
        id: peerListContainer
        width: container.width
        height: 200
        color: "#AA222222"
        anchors.top: header.bottom

        ListView {
            id: peerList
            anchors.fill: parent
            clip: true
            spacing: 3

            model: peerModel
            delegate: listDelegate
        }
    }

    Button {
        anchors { top: peerListContainer.bottom; topMargin: 30; horizontalCenter: parent.horizontalCenter }
        text: "Back"

        onButtonClicked: container.leave()
    }
}
