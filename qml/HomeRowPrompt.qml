// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Fadeable {
    id: container
    width: 700; height: 400
    anchors.fill: parent
    property string log_tag: "HomeRowPrompt.qml: "

    property alias title: title.text
    property color fillColor: "#AA666666"

    signal homeRowChosen(string homeRow)

    function sendHomeRow() {
        container.homeRowChosen(parseHomeRow())
    }

    function parseHomeRow() {
        var homeRow = ""
        for(var i = 0; i < piecesModel.count; ++i)
            homeRow += piecesModel.get(i).piece
        return homeRow
    }

    function randomize() {
        var homeRow = []
        while(homeRow.length < 6) {
            var pos = Math.floor(Math.random() * 6)
            homeRow[homeRow.length] = pos
        }
        console.debug("random homeRow: " + homeRow)

        for(var p in homeRow) {
            piecesModel.move(p, homeRow[p], 1)
        }
    }

    MouseTrap{opacity: .5}

    Rectangle {
        id: dialog
        width: title.width > content.width ? title.width + 20 : content.width + 20;
        height: title.height + content.height + 40
        anchors.centerIn: parent

        color: Qt.darker("lightgray")
        border.color: "black"
        radius: 10;

        Text {
            id: title
            text: "Title"; color: "white"
            style: Text.Outline; styleColor: "black"
            font { pointSize: 20; bold: true }

            anchors { top: parent.top; horizontalCenter: parent.horizontalCenter; margins: 10 }
        }

        Item {
            id: content
            width: pieces.width; height: pieces.height + confirmButton.height
            anchors { top: title.bottom; topMargin: 10; horizontalCenter: parent.horizontalCenter }

            Rectangle {
                color: "#af0f0f0f"
                border.color: "black"
                radius: 10
                anchors.fill: pieces
            }

            ListModel {
                id: piecesModel
                ListElement { piece: 1; gridId: 0 }
                ListElement { piece: 2; gridId: 1 }
                ListElement { piece: 3; gridId: 2 }
                ListElement { piece: 1; gridId: 3 }
                ListElement { piece: 2; gridId: 4 }
                ListElement { piece: 3; gridId: 5 }

            }

            Component {
                id: piecesDelegate
                Item {
                    id: delegateContainer
                    width: pieces.cellWidth; height: pieces.cellHeight
                    Item {
                        id: itemContainer
                        BoardPosition {
                            id: item
                            parent: location
                            x: delegateContainer.x
                            y: delegateContainer.y
                            enabled: true; mouseActive: false
                            color: "#80FFFFFF"
                            piece: model.piece

                            Behavior on x {
                                enabled: itemContainer.state !== "active"
                                NumberAnimation { duration: 400; easing.type: Easing.OutBounce }
                            }
                        }

                        states: State {
                            name: "active"
                            when: location.currId === model.gridId
                            PropertyChanges {
                                target: item
                                x: location.mouseX - item.width / 2
                                z: 10
                            }
                        }
                    }
                }
            }

            GridView {
                id: pieces
                width: cellWidth * 6; height: cellHeight
                anchors { top: parent.top; horizontalCenter: parent.horizontalCenter }

                cellWidth: 100; cellHeight: 100

                interactive: false

                model: piecesModel
                delegate: piecesDelegate

                MouseArea {
                    id: location
                    anchors.fill: parent
                    hoverEnabled: true

                    property int currId: -1
                    property int newIndex
                    property int index: pieces.indexAt(mouseX, mouseY)

                    onPressed: currId = piecesModel.get(newIndex = index).gridId
                    onPressAndHold: currId = piecesModel.get(newIndex = index).gridId
                    onReleased: currId = -1
                    onMousePositionChanged: {
                        if(location.currId !== -1 && index !== -1 && index !== newIndex)
                            piecesModel.move(newIndex, newIndex = index, 1)
                    }
                    onContainsMouseChanged: {
                        if(!location.containsMouse) {
                            currId = -1
                        }
                    }
                }
            }
            Item {
                id: buttonContainer
                anchors { top: pieces.bottom; topMargin: confirmButton.height / 4; horizontalCenter: parent.horizontalCenter }
                width: childrenRect.width; height: confirmButton.height
                Button {
                    id: confirmButton
                    text: "Confirm"
                    anchors { left: parent.left; verticalCenter: parent.verticalCenter }

                    onButtonClicked: { container.sendHomeRow(); container.hide() }
                }
                Button {
                    id: randomizeButton
                    text: "Randomize"
                    anchors { left: confirmButton.right; verticalCenter: confirmButton.verticalCenter }

                    onButtonClicked: container.randomize()
                }
            }
        }
    }
}
