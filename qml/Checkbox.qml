import QtQuick 1.1
import Effects 1.0

Rectangle {
    id: container
    width: height
    height: 30

    property bool checked: true

    property int __fillerCheckedSize: 22
    property int __fillerUncheckedSize: 5
    onCheckedChanged: if (checked) {filler.height = __fillerCheckedSize} else {filler.height = __fillerUncheckedSize}

    border.width: 2
    border.color: "black"

    effect: DropShadow {
        color: "#AA000000"
        blurRadius: 8
        offset: Qt.point(3,3)
    }

    Rectangle {
        id: filler
        width: height
        height: checked? __fillerCheckedSize : __fillerUncheckedSize
        anchors.centerIn: container
        clip: true

        Behavior on height { PropertyAnimation {duration: 300} }

        Rectangle {
            id: x1
            width: 40
            height: 6
            anchors.centerIn: filler
            anchors.horizontalCenterOffset: -1

            rotation: 135

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#FFEC008C" }
                GradientStop { position: 1.0; color: "#FF9E005D" }
            }
        }
        Rectangle {
            id: x2
            width: 40
            height: 6
            anchors.centerIn: filler

            rotation: 45

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#FFEC008C" }
                GradientStop { position: 1.0; color: "#FF9E005D" }
            }
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: container

        hoverEnabled: true
        onHoveredChanged: {
            if (containsMouse) {
                if (checked)
                    filler.height = __fillerCheckedSize * .8
                else
                    filler.height = __fillerUncheckedSize * 1.5
            } else {
                if (checked)
                    filler.height = __fillerCheckedSize
                else
                    filler.height = __fillerUncheckedSize
            }
        }

        onReleased: checked = !checked
    }
}
