import QtQuick 1.1
import Effects 1.0
import Wheelie 1.0

Item {
    id: slider; width: 400; height: 40

    // value is read/write.
    property real value: 0
    onValueChanged: {
        if(value < minimum)
            value = minimum
        else if(value > maximum)
            value = maximum

        updatePos()
    }
    property real maximum: 1
    property real minimum: 0
    property int xMax: width - Math.sqrt(Math.pow(handle.width, 2) + Math.pow(handle.height, 2)) + 2
    onXMaxChanged: updatePos()
    onMinimumChanged: updatePos()

    function updatePos() {
        if (maximum > minimum) {
            var pos = 2 + (value - minimum) * slider.xMax / (maximum - minimum);
            pos = Math.min(pos, width - handle.width - 2);
            pos = Math.max(pos, 2);
            handle.x = pos;
        } else {
            handle.x = 2;
        }
    }

    Rectangle {
        id: background; smooth: true
        width: slider.height; height: slider.width; radius: height / 2
        anchors.centerIn: slider
        rotation: -90
        border { color: "#FF464646"; width: 2 }
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#FFEC008C" }
            GradientStop { position: 1.0; color: "#FF9E005D" }
        }

        effect: DropShadow {
            color: "#AA000000"
            blurRadius: 8
            offset: Qt.point(3,3)
        }

        MouseArea {
            anchors.fill: parent

            onPressed: {
                var clickedX = mapToItem(slider, mouseX, mouseY).x - handle.width / 2
                value = (maximum - minimum) * (clickedX-2) / slider.xMax + minimum
            }
        }

        WheelArea {
            anchors.fill: parent
            onVerticalWheel: {
                    value += (delta > 0 ? 0.1 : -0.1)
            }
        }
    }

    Item {
        // workaround for an odd problem
        id: centerLine
        width: slider.width; height: 1
        y: (slider.height + background.border.width * 2) / 2
    }

    Rectangle {
        id: handle; smooth: true
        anchors.verticalCenter: centerLine.verticalCenter
        width: slider.height * 1.3; height: width; radius: 2
        rotation: 45
        clip: true
        border.color: "#FFA1A1A1"
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#FFFFFFFF" }
            GradientStop { position: 1.0; color: "#FF959595" }
        }

        Behavior on x {
            NumberAnimation { duration: 400; easing.type: Easing.OutBounce }
        }

        MouseArea {
            id: mouse
            anchors.fill: parent; drag.target: parent
            drag.axis: Drag.XAxis; drag.minimumX: 2; drag.maximumX: slider.xMax+2
            onPositionChanged: { value = (maximum - minimum) * (handle.x-2) / slider.xMax + minimum; }
        }
    }
}
