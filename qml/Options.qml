// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: container
    property string log_tag: "Options.qml: "

    property real __originalMusicVolume
    property bool __originalMusicEnabled
    property real __originalEffectsVolume
    property bool __originalEffectsEnabled
    property real __originalVoiceVolume
    property bool __originalVoiceEnabled

    signal acceptClicked()
    signal cancelClicked()

    onEnabledChanged: {
        if (enabled) {
            difficultySlider.selected = profileManager.catfightDifficulty
            __originalMusicVolume = musicSlider.value = profileManager.musicVolume
            __originalMusicEnabled = musicCheckbox.checked = profileManager.musicEnabled
            __originalEffectsVolume = effectsSlider.value = profileManager.effectsVolume
            __originalEffectsEnabled = effectsCheckbox.checked = profileManager.effectsEnabled
            __originalVoiceVolume = voiceSlider.value = profileManager.voiceVolume
            __originalVoiceEnabled = voiceCheckbox.checked = profileManager.voiceEnabled
        }
    }

    Item {
        id: optionsContainer
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: optionsList.width
        height: children.height

        Rectangle {
            id: optionsList
            width: 300; height: 400
            color: "transparent"
            anchors.fill: parent

            //Music Controls
            ShadowedText {
                id: musicText
                text: "Music"
                font.pointSize: 16; font.bold: true
                anchors { horizontalCenter: optionsList.horizontalCenter; top: optionsList.top; topMargin: 15 }
                color: "white"
            }

            Checkbox {
                id: musicCheckbox
                anchors { verticalCenter: musicText.verticalCenter; left: musicText.right; leftMargin: 5 }
                checked: profileManager.musicEnabled
                onCheckedChanged: if (profileManager.musicEnabled !== checked) profileManager.musicEnabled = checked;
            }

            Slider {
                id: musicSlider
                width: 220; height: 15
                anchors { top: musicText.bottom; topMargin: 8; horizontalCenter: optionsList.horizontalCenter }

                value: profileManager.musicVolume
                onValueChanged: if (profileManager.musicVolume !== value) profileManager.musicVolume = value
            }

            //Sound Effects Controls
            ShadowedText {
                id: effectsText
                text: "Sound Effects"
                font.pointSize: 16; font.bold: true
                anchors { horizontalCenter: optionsList.horizontalCenter; top: musicSlider.bottom; topMargin: 30 }
                color: "white"
            }

            Checkbox {
                id: effectsCheckbox
                anchors { verticalCenter: effectsText.verticalCenter; left: effectsText.right; leftMargin: 5 }
                checked: profileManager.effectsEnabled
                onCheckedChanged: if (profileManager.effectsEnabled !== checked) profileManager.effectsEnabled = checked;
            }

            Slider {
                id: effectsSlider
                width: 220; height: 15
                anchors { top: effectsText.bottom; topMargin: 8; horizontalCenter: optionsList.horizontalCenter }

                value: profileManager.effectsVolume
                onValueChanged: if (profileManager.effectsVolume !== value) profileManager.effectsVolume = value
            }

            //Voice Volume Controls
            ShadowedText {
                id: voiceText
                text: "Voice"
                font.pointSize: 16; font.bold: true
                anchors { horizontalCenter: optionsList.horizontalCenter; top: effectsSlider.bottom; topMargin: 30 }
                color: "white"
            }

            Checkbox {
                id: voiceCheckbox
                anchors { verticalCenter: voiceText.verticalCenter; left: voiceText.right; leftMargin: 5 }
                checked: profileManager.voiceEnabled
                onCheckedChanged: if (profileManager.voiceEnabled !== checked) profileManager.voiceEnabled = checked;
            }

            Slider {
                id: voiceSlider
                width: 220; height: 15
                anchors { top: voiceText.bottom; topMargin: 8; horizontalCenter: optionsList.horizontalCenter }

                value: profileManager.voiceVolume
                onValueChanged:  if (profileManager.voiceVolume !== value) profileManager.voiceVolume = value
            }

            //Catfight Difficulty Controls
            ShadowedText {
                id: difficultyText
                text: "Catfight Difficulty"
                font.pointSize: 16; font.bold: true
                anchors { horizontalCenter: optionsList.horizontalCenter; top: voiceSlider.bottom; topMargin: 30 }
                color: "white"
            }

            SnappingSlider {
                id: difficultySlider
                width: 220; height: 30
                anchors { top: difficultyText.bottom; topMargin: 8; horizontalCenter: optionsList.horizontalCenter }
                itemNames: ["Easy", "Medium", "Hard"]
                Connections {
                    target: profileManager
                    onProfileChanged: difficultySlider.selected = profileManager.catfightDifficulty
                    onCatfightDifficultyChanged: difficultySlider.selected = profileManager.catfightDifficulty
                }
            }

            Rectangle {
                id: buttonContainer
                width: acceptButton.width * 2 + 50; height: acceptButton.height + 10
                y: optionsList.height / 2 + 140
                anchors.top: difficultySlider.bottom
                anchors.topMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"

                Button {
                    id: acceptButton
                    text: "Accept"
                    anchors.left: buttonContainer.left

                    onButtonClicked: {
                        profileManager.setCatfightDifficulty(difficultySlider.selected)
                        acceptClicked()
                    }
                }

                Button {
                    id: cancelButton
                    text: "Cancel"
                    anchors.right: buttonContainer.right

                    onButtonClicked: {
                        profileManager.musicVolume = __originalMusicVolume
                        profileManager.musicEnabled = __originalMusicEnabled
                        profileManager.effectsVolume = __originalEffectsVolume
                        profileManager.effectsEnabled = __originalEffectsEnabled
                        profileManager.voiceVolume = __originalVoiceVolume
                        profileManager.voiceEnabled = __originalVoiceEnabled

                        cancelClicked()
                    }
                }
            }
        }
    }
}
