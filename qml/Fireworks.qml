import QtQuick 1.1

Item {
    id: root
    anchors.fill: parent

    function getNextTimeout() {
        return Math.random() * 8000
    }
    function getOrigin() {
        return Qt.point(Math.random() * root.width, root.height)
    }
    function getTarget() {
        return Qt.point(Math.random() * root.width, (Math.random() * root.height) / 4)
    }

    Firework {
        id: f1
        Timer {
            interval: getNextTimeout()
            running: visible
            repeat: true
            onTriggered: {
                parent.target = getTarget()
                parent.origin = getOrigin()
                parent.shoot()
                interval = getNextTimeout()
            }
        }
    }
    Firework {
        id: f2
        Timer {
            interval: getNextTimeout()
            running: visible
            repeat: true
            onTriggered: {
                parent.target = getTarget()
                parent.origin = getOrigin()
                parent.shoot()
                interval = getNextTimeout()
            }
        }
    }
    Firework {
        id: f3
        Timer {
            interval: getNextTimeout()
            running: visible
            repeat: true
            onTriggered: {
                parent.target = getTarget()
                parent.origin = getOrigin()
                parent.shoot()
                interval = getNextTimeout()
            }
        }
    }
    Firework {
        id: f4
        Timer {
            interval: getNextTimeout()
            running: visible
            repeat: true
            onTriggered: {
                parent.target = getTarget()
                parent.origin = getOrigin()
                parent.shoot()
                interval = getNextTimeout()
            }
        }
    }
}
