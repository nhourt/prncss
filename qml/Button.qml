import QtQuick 1.0
import Effects 1.0

Rectangle {
    id: container
    width: label.width + 20
    height: label.height + 10
    radius: 10
    anchors.margins: 10
    state: "bored"
    property string log_tag: "Button.qml: "

    property alias label: label
    property alias text: label.text
    property int fontSize: 20

    property color baseColor: "#FFAE005F"
    property color textColor: "white"
    color: baseColor
    border { width: 2; color: "#FFAE005F" }

    signal buttonClicked
    signal mouseMoved(int x, int y)
    signal mouseEntered
    signal mouseExited

    onEnabledChanged: state = "bored"

    effect: DropShadow {
        color: "#AA000000"
        blurRadius: 8
        offset: state === "pressed" ? Qt.point(1,1) : Qt.point(3,3)

        Behavior on offset { PropertyAnimation { duration: 100 } }
    }

    Rectangle {
        id: gradient
        anchors.fill: container
        radius: parent.radius
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#FFFF008E" }
            GradientStop { position: 1.0; color: "#FFAE005F" }
        }

        Behavior on opacity { PropertyAnimation { duration: 100 } }

        onEnabledChanged: if (!enabled) opacity = 1
    }

    Rectangle {
        id: glow
        visible: container.state === "pressed"
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: parent.right

            margins: {left: 8; right: 8; top: 8; bottom: 8}
        }
        radius: container.radius
        color: "#FFFF008E"
        effect: Blur {blurRadius:  10}

        opacity: 0
        Behavior on opacity { PropertyAnimation { duration: 100 } }
    }

    Text {
        id: label
        color: textColor
        anchors.centerIn: container
        text: "Label"
        style: Text.Sunken
        font.pointSize: container.fontSize

        onTextChanged: mouseArea.__hasMouse = false

        effect: DropShadow {
            color: "#AA000000"
            blurRadius: 8
            offset: Qt.point(1,1)
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: container
        hoverEnabled: true
        acceptedButtons: Qt.LeftButton

        property bool __hasMouse: false

        onClicked: {
            audioManager.playEffect("audio/click.wav")
            container.buttonClicked()
        }
        onMousePositionChanged: {
            if (!pressed)
                container.state = "hover"
            container.mouseMoved(mouseX, mouseY)

            if (!__hasMouse) {
                container.mouseEntered()
                __hasMouse = true
            }
        }
        onEntered: {
            container.mouseEntered()
            __hasMouse = true
        }
        onExited: {
            container.mouseExited()
            __hasMouse = false
        }
    }

    states: [
        State {
            name: "bored"
            when: !mouseArea.pressed && mouseArea.containsMouse === false
            PropertyChanges {
                target: gradient
                opacity: 1
            }
            PropertyChanges {
                target: container
                color: container.baseColor
            }
        },
        State {
            name: "hover"
            when: !mouseArea.pressed && mouseArea.containsMouse
            PropertyChanges {
                target: gradient
                opacity: 0
            }
            PropertyChanges {
                target: container
                color: Qt.lighter(container.baseColor)
            }
        },
        State {
            name: "pressed"
            when: mouseArea.containsMouse && mouseArea.pressed
            PropertyChanges {
                target: gradient
                opacity: 0
            }
            PropertyChanges {
                target: glow
                opacity: 1
            }
        },
        State {
            name: "disabled"
            PropertyChanges {
                target: container
                enabled: false
            }
            PropertyChanges {
                target: gradient
                visible: false
            }
            PropertyChanges {
                target: container
                color: Qt.darker(container.baseColor)
            }
            PropertyChanges {
                target: label
                color: Qt.darker(container.textColor)
            }
        }
    ]
}
