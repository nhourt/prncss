// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Effects 1.0

Text {
    effect: DropShadow {
        color: "black"
        blurRadius: 8
        offset: Qt.point(0,2)
    }
}
