import QtQuick 1.1
import Wheelie 1.0
import "../eliza.js" as Eliza

// game board, consists of BoardPositions
Item {
    id: container
    property string log_tag: "GameBoard.qml: "
    property string selfName: "You"
    property string aiChatName: "Prncss"
    property bool networkGame
    property bool easyGame
    property bool campaignGame
    property bool localMultiplayer
    property bool tutorial: false
    property bool active
    property int minTextDisplayTime: 1500
    property string side: "South"

    onActiveChanged: {
        storyItems.__storyPosition = 0
        clearChatLog()
        networkFailureDialog.hide()
        networkWaitDialog.hide()
        gameOverDialog.hide()
        campaignDialog.hide()
        homeRowPrompt.hide()
        gameHelpFadeable.hide()

        heldPiece.piece = 0
        boardSpinner.stop()
        board.rotation = 0

        boomboxVolumeSlider.value = function() { return profileManager.musicVolume }

        if(active)
            chatInput.forceActiveFocus()
        else
            chatInput.focus = false
    }

    signal buttonClick(string pos)
    signal homeRowChosen(string homeRow)
    onHomeRowChosen: {
        if (networkWaitDialog.needToShow) {
            networkWaitDialog.show()
            networkWaitDialog.needToShow = false
        }
        if(homeRowPrompt.title.toLowerCase().indexOf("north") != -1)
            side = "South"
        else if(homeRowPrompt.title.toLowerCase().indexOf("south") != -1)
                side = "North"
    }

    signal userQuit()
    signal sendChat(string message)
    signal nextCampaignLevel()

    signal moveCanceled()

    signal beastMode()
    signal doIDie()

    onNextCampaignLevel: {
        console.debug(log_tag + "onNextCampaignLevel")
        storyItems.__storyPosition = 0
    }
    function showNetworkWait() {
        networkWaitDialog.show()
    }
    function hideNetworkWait() {
        networkWaitDialog.hide()
    }

    function blackOut() {
        storyBlackout.show()
    }
    function endBlackOut() {
        storyBlackout.hide()
    }

    function displayPenelopeText (message, voiceUrl) {
        var duration = audioManager.playVoice(voiceUrl);
        if(duration < minTextDisplayTime)
            duration = minTextDisplayTime
        penelopeBubble.displayMessage(message, duration);

        chatLog.model.append({t: "<span style='color: blue'><strong>Penelope:</strong> " + message + "</span>", alignment: Text.AlignLeft});
        chatLog.currentIndex = chatLog.count-1;

        return duration
    }

    function displayJessicaRagsText (message, voiceUrl) {
        var duration = audioManager.playVoice(voiceUrl);
        if(duration < minTextDisplayTime)
            duration = minTextDisplayTime
        jessicaBubble.displayMessage(message, duration);

        chatLog.model.append({t: "<span style='color: red'><strong>Jessica:</strong> " + message + "</span>", alignment: Text.AlignLeft});
        chatLog.currentIndex = chatLog.count-1;

        return duration
    }

    function displayJessicaText(message, voiceUrl) {
        jessicaHead.showRags = false
        return displayJessicaRagsText(message, voiceUrl)
    }

    function displayRagsText(message, voiceUrl) {
        jessicaHead.showRags = true
        return displayJessicaRagsText(message, voiceUrl)
    }

    function getCoordinatesByName(position) {
        position = board.getPositionByName(position);
        var coords = mapFromItem(position, 0, 0);
        return Qt.point(coords.x, coords.y);
    }

    function onChooseStart(side) {
        console.log(log_tag + "choose start")

        if (networkGame) {
            if (networkWaitDialog.state === "shown")
                networkWaitDialog.hide()
            else
                networkWaitDialog.needToShow = true
        }

        homeRowPrompt.title = side + ": choose starting positions"
        homeRowPrompt.show()
    }

    function onNewChatMessage(chatItem, sender) {
        chatItem = String(chatItem)
        if (chatItem.indexOf("/me ") === 0) {
            if (String(sender) === selfName)
                chatLog.model.append({ t: "<em>" + profileManager.profileName + chatItem.substring(3) + "</em>", alignment: Text.AlignLeft } )
            else
                chatLog.model.append({ t: "<em>" + sender + chatItem.substring(3) + "</em>", alignment: Text.AlignLeft } )
        } else {
            chatLog.model.append({ t: "<strong>" + sender + ":</strong> " + chatItem, alignment: Text.AlignLeft})
        }
        chatLog.currentIndex = chatLog.count-1
    }

    function logMove(moveText) {
        chatLog.model.append({t: "<em style='color:gray'>" + moveText + "</em>", alignment: Text.AlignRight})
        chatLog.currentIndex = chatLog.count-1
    }

    function doSendChat() {
        if(chatInput.text != "")
        {
            var input = chatInput.text
            chatInput.text = ""

            if (input === "halp") {
                aiSuggestMoveButton.buttonClicked();
            } else if (input === "Do a barrel roll") {
                boardSpinner.restart();
            } else if (input === "Keep rolling") {
                //Please note that this cheat is meant to remain undocumented.
                boardSpinner.spins = 20;
                chatLog.model.append({t:"OK", alignment: Text.AlignHCenter});
            } else if (input === "Do I die?") {
                doIDie();
            } else if (input === "BEAST MODE") {
                //Please note that this cheat is meant to remain undocumented.
                chatLog.model.append({t: "<strong style='color:red'>BEAST MODE</strong>", alignment: Text.AlignHCenter});
                beastMode();
            } else if(input === "I'm so over this" && campaignGame) {
                userQuit()
                if(profileManager.campaignLevel < 3)
                    profileManager.campaignLevel++
            } else {
                sendChat(input)
                onNewChatMessage(input, selfName)

                if (!networkGame) {
                    Eliza.chat(input)
                    var chatDelayTimer = Qt.createQmlObject("import QtQuick 1.1; Timer{interval: Math.random() * 8000; repeat: false}", container, "chatDelayTimer");
                    chatDelayTimer.triggered.connect(function(){onNewChatMessage(Eliza.response, aiChatName); chatDelayTimer.destroy()});
                    chatDelayTimer.start();
                }
            }

        }
    }

    function clearChatLog() {
        chatLog.model.clear()
    }

    function networkLost() {
        console.log (log_tag + "connection lost")
        networkFailureDialog.isFatal = false
        networkFailureDialog.show()
    }
    function networkRecovered() {
        console.log (log_tag + "connection recovered")
        networkFailureDialog.hide()
    }
    function networkFailed() {
        console.log (log_tag + "connection failed")
        networkFailureDialog.isFatal = true
        networkFailureDialog.show()
    }

    function gameEnded(gameOverReason) {
        gameOverDialog.text = "Game over: " + gameOverReason
        campaignDialog.text = "Game over: " + gameOverReason

        if (campaignGame)
            campaignDialog.show()
        else
            gameOverDialog.show()
    }

    function campaignLaunchPad()
    {
        container.side = "none"
        var level = profileManager.campaignLevel
        console.debug(log_tag + "campaignLaunchPad:" + level + "[" + storyItems.__storyPosition + "]")

        if (level === 0) {
            audioManager.voiceFinished.connect(storyItems.campaignLevelZero)
            storyItems.campaignLevelZero()
        } else if (level === 1) {
            audioManager.voiceFinished.connect(storyItems.campaignLevelOne)
            storyItems.campaignLevelOne()
        } else if (level === 2) {
            audioManager.voiceFinished.connect(storyItems.campaignLevelTwo)
            storyItems.campaignLevelTwo()
        } else if (level === 3) {
            audioManager.voiceFinished.connect(storyItems.campaignLevelThree)
            storyItems.campaignLevelThree()
        }
    }

    property color bgColor: "#FF55BA47"

    Fireworks {
        id: fireworks
        objectName: "fireworks"
        anchors.fill: parent
        visible: false
        z: storyItems.z - 1
    }

    Item {
        id: storyItems
        z: 1000
        anchors.fill: container

        property int __storyPosition: 0

        function endStory() {
            audioManager.stopVoice()
            jessicaBubble.hide()
            penelopeBubble.hide()
            endBlackOut()
            jessicaHead.showRags = false;
            container.side = "South"

            switch (profileManager.campaignLevel) {
            case 0:
                audioManager.voiceFinished.disconnect(campaignLevelZero)
                break;
            case 1:
                audioManager.voiceFinished.disconnect(campaignLevelOne)
                break;
            case 2:
                audioManager.voiceFinished.disconnect(campaignLevelTwo)
                break;
            case 3:
                audioManager.voiceFinished.disconnect(campaignLevelThree)
                userQuit()
                break;
            }
        }

        function call(func, params) {
            if(params.length === 0)
                return func()
            else if(params.length === 1)
                return func(params[0])
            else if(params.length === 2)
                return func(params[0], params[1])
        }

        function nextLevel() {
            __storyPosition = 0
            if(profileManager.campaignLevel < 2) {
                campaignLaunchPad(profileManager.campaignLevel + 1)
            }
            container.nextCampaignLevel()
        }

        function campaignLevelZero()
        {
            var functions = [
                        displayRagsText,
                        displayPenelopeText,
                        displayRagsText,
                        displayPenelopeText,
                        displayRagsText,
                        function(un, du) { jessicaHead.showRags = false; return displayPenelopeText(un,du) },
                        displayJessicaText,
                        displayPenelopeText,
                        function(un, du) { jessicaHead.showRags = true; return displayPenelopeText(un,du) },
                        displayRagsText,
                        displayRagsText,
                        displayPenelopeText,
                        endStory
                    ]
            var params = [
                        ["Hey Penelope, I just stopped in to see my niece before her big Birthday. You only turn 16 once.", "audio/story/Rags1.ogg"],
                        ["That is true Uncle Rags, but I haven’t thought about what I am going to do for it yet. ", "audio/story/Pen1.ogg"],
                        ["Pen, you deserve to have the nicest party this town has ever seen and I’m going to help make sure that happens. ", "audio/story/Rags2.ogg"],
                        ["O Uncle Rags, you’re the best uncle in the entire galaxy.  No even bigger, the solar system.", "audio/story/Pen2.ogg"],
                        ["Right, well go along and start letting your friends know about the party.", "audio/story/Rags3.ogg"],
                        ["Hey Jessica, I wanted to make sure you were the first person to know about my upcoming birthday party. We have always been BFFLF - Best Friends For Like Forever!", "audio/story/Pen3.ogg"],
                        ["Well Penelope, I’m tired of living in your shadow. I have decided to make a name for myself by throwing a party the same weekend and it’s gonna be the best… party… ever. ", "audio/story/Jess1.ogg"],
                        ["I hate you!  I’m going to tell my uncle.", "audio/story/Pen4.ogg"],
                        ["Uncle Rags, my friend Jessica is throwing a party the same weekend!", "audio/story/Pen5.ogg"],
                        ["Penelope, It’s time to go stud or scud. I told you I would help you have the best party this town has ever seen and I intend to.", "audio/story/Rags4.ogg"],
                        ["Now here is the plan. We need to make sure that everyone comes to your party instead of hers. To do that we have to sneak over to Jessica’s house and steal her guest list so that we can invite everyone to your party instead.", "audio/story/Rags5.ogg"],
                        ["Let's do this!", "audio/story/Pen6.ogg"],
                        []
                    ]

            if(__storyPosition < functions.length) {
                call(functions[__storyPosition], params[__storyPosition])
                ++__storyPosition
            }
        }
        function campaignLevelOne()
        {
            var functions = [
                        function(un, du) { jessicaHead.showRags = true; return displayPenelopeText(un, du) },
                        displayRagsText,
                        displayPenelopeText,
                        displayRagsText,
                        displayPenelopeText,
                        endStory
                    ]
            var params = [
                        ["Mission Accomplished Uncle Rags!", "audio/story/Pen7.ogg"],
                        ["That’s my girl, It’s not over yet though. Unfortunately, I have bad news. I recently heard that Jessica is having Justice Beaver come sing at her party.", "audio/story/Rags6.ogg"],
                        ["NOT JUSTICE BEAVER!", "audio/story/Pen8.ogg"],
                        ["You betcha! In the flesh! You have to get in her house and steal his agent’s number so we can call make sure Justice Beaver comes to your party instead.", "audio/story/Rags7.ogg"],
                        ["I’m on it!", "audio/story/Pen9.ogg"],
                        []
                    ]
            if(__storyPosition < functions.length) {
                call(functions[__storyPosition], params[__storyPosition])
                ++__storyPosition
            }
        }
        function campaignLevelTwo()
        {
            var functions = [
                        function(un, du) { jessicaHead.showRags = true; return displayPenelopeText(un, du) },
                        displayRagsText,
                        displayPenelopeText,
                        displayRagsText,
                        displayPenelopeText,
                        displayRagsText,
                        displayPenelopeText,
                        displayRagsText,
                        displayPenelopeText,
                        endStory
                    ]
            var params = [
                        ["Everyone at my party is gonna be screaming out Baby, baby, baby O! ", "audio/story/Pen10.ogg"],
                        ["Good job sweetie. I added an extra touch and had Rebecca Black sent to her party. Not everyone is gonna be enjoying Fridays anymore.", "audio/story/Rags8.ogg"],
                        ["So we did it!", "audio/story/Pen11.ogg"],
                        ["Not quite. There is one more thing.  I have heard she is planning on having her dad bring her a brand new sports car, and it could really help her party if we don’t do something.", "audio/story/Rags9.ogg"],
                        ["What’s the plan, Uncle Rags?", "audio/story/Pen12.ogg"],
                        ["We need to go change her order so she gets a bad car and you get the best one on the lot.", "audio/story/Rags10.ogg"],
                        ["Are we going to give something trashy like a BMW?", "audio/story/Pen13.ogg"],
                        ["A <i>whole lot</i> worse! A rusty old '78 Jeep truck, straight from the junkyard.", "audio/story/Rags11.ogg"],
                        ["That’s disgusting! Let’s do it.", "audio/story/Pen14.ogg"],
                        []
                    ]
            if(__storyPosition < functions.length) {
                call(functions[__storyPosition], params[__storyPosition])
                ++__storyPosition
            }
        }
        function campaignLevelThree()
        {
            var functions = [
                        displayRagsText,
                        displayPenelopeText,
                        endStory
                    ]
            var params = [
                        ["Now go enjoy your party. I'm <i>bad</i> high on ya!", "audio/story/Rags12.ogg"],
                        ["Thanks Uncle Rags. This is the best sweet sixteen a girl could ever hope for!", "audio/story/Pen15.ogg"],
                        []
                    ]
            if(__storyPosition < functions.length) {
                call(functions[__storyPosition], params[__storyPosition])
                ++__storyPosition
            }
        }

        Rectangle {
            id: jessicaRectangle
            anchors.top: storyItems.top
            anchors.right: storyItems.right
            width: 300
            height: 300
            color: "transparent"
            z: storyItems.z + 1
            Image {
                id: jessicaHead
                property bool showRags: false
                property bool doShowRags: tutorial || showRags
                anchors { top: jessicaRectangle.top; left: jessicaRectangle.left; right: jessicaRectangle.right }
                source: "../imgs/jessica_head.png"
                fillMode: Image.PreserveAspectFit
                z: jessicaRectangle.z + 1

                Behavior on doShowRags {
                    SequentialAnimation {
                        PropertyAnimation { target: jessicaHead; property: "opacity"; from: 1; to: 0; duration: 500 }
                        ScriptAction { script: { jessicaHead.source = jessicaHead.showRags ? "../imgs/rags_head.png" : "../imgs/jessica_head.png" } }
                        PropertyAnimation { target: jessicaHead; property: "opacity"; from: 0; to: 1; duration: 500 }
                    }
                }
            }
            Image {
                id: jessicaHighlight
                anchors.fill: jessicaHead
                source: "../imgs/jessica_head_highlight.png"
                fillMode: Image.PreserveAspectFit
                z: jessicaHead.z - 1
                opacity: 0

                Behavior on opacity {
                    NumberAnimation { duration: 750 }
                }

                Timer {
                    id: jessicaHighlightTimer
                    interval: 750
                    running: container.side.toLowerCase() === "north"
                    repeat: true
                    onRunningChanged: {
                        if(!running)
                            jessicaHighlight.opacity = 0
                    }
                    onTriggered: {
                            jessicaHighlight.opacity = jessicaHighlight.opacity > 0 ? 0 : 1
                    }
                }
            }
            SpeechBubble {
                id: jessicaBubble
                anchors.right: jessicaHead.left
                anchors.rightMargin: -.35 * jessicaHead.width
                anchors.bottom: jessicaHead.verticalCenter
                anchors.bottomMargin: -20
                z: jessicaHead.z + 1

                tailDirection: Qt.BottomRightCorner
            }
        }
        Rectangle {
            id: penelopeRectangle
            anchors.bottom: storyItems.bottom
            anchors.left: storyItems.left
            width: 300
            height: 300
            color: "transparent"
            z: storyItems.z + 1
            Image {
                id: penelopeHead
                anchors { bottom: penelopeRectangle.bottom; left: penelopeRectangle.left; right: penelopeRectangle.right }
                source: "../imgs/penelope_head.png"
                fillMode: Image.PreserveAspectFit
                z: penelopeRectangle.z + 1
            }
            Image {
                id: penelopeHighlight
                anchors.fill: penelopeHead
                source: "../imgs/penelope_head_highlight.png"
                fillMode: Image.PreserveAspectFit
                z: penelopeHead.z - 1
                opacity: 0

                Behavior on opacity {
                    NumberAnimation { duration: 750 }
                }

                Timer {
                    id: penelopeHighlightTimer
                    interval: 1000
                    running: container.side.toLowerCase() === "south"
                    repeat: true
                    onRunningChanged: {
                        if(!running)
                            penelopeHighlight.opacity = 0
                    }
                    onTriggered: {
                            penelopeHighlight.opacity = penelopeHighlight.opacity > 0 ? 0 : 1
                    }
                }
            }
            SpeechBubble {
                id: penelopeBubble
                anchors.left: penelopeHead.right
                anchors.leftMargin: -.35 * penelopeHead.width
                anchors.bottom: penelopeHead.verticalCenter
                anchors.bottomMargin: penelopeHead.height/-4
                z: penelopeHead.z + 1

                tailDirection: Qt.BottomLeftCorner
            }
        }

        Fadeable {
            id: storyBlackout
            anchors.fill: parent

            MouseTrap {
                id: storyBlackoutMouseTrap

                onClicked: { storyItems.endStory() }
            }

            Text {
                anchors.bottom: storyBlackout.bottom
                anchors.horizontalCenter: storyBlackout.horizontalCenter
                color: "gray"
                style: Text.Sunken
                font.pointSize: 20
                text: "<em>Click anywhere to skip story...</em>"
            }
        }
    }

    Item {
        id: menu
        anchors { top: container.top; left: container.left }
        width: boombox.width + helpButton.width + quitButton.width
        height: boombox.height
        z: board.z + 1

        MouseArea {
            z: boombox.z
            anchors.fill: boombox
            acceptedButtons: Qt.LeftButton
            onClicked: { audioManager.playPauseMusic(); console.log (log_tag + "playing or pausing music") }
        }

        WheelArea {
            anchors.fill: boombox
            z: boombox.z + 1
            onVerticalWheel: {
                if (delta > 0 && profileManager.musicVolume <= .9) {profileManager.musicVolume += .1}
                else if(delta < 0 && profileManager.musicVolume >= .1) {profileManager.musicVolume -= .1}
            }
        }

        Image {
            id: boombox
            source: "../imgs/boombox.png"
            anchors.left: parent.left
            height: 225
            fillMode: Image.PreserveAspectFit
            smooth: true
        }
        Text {
            id: musicInfoText
            anchors.top: boombox.bottom
            anchors.left: boombox.left; anchors.leftMargin: 15
            width: boombox.width
            textFormat: Text.PlainText
            elide: Text.ElideRight
            font.pointSize: 14
            color: "white"
            style: Text.Outline

            property string textBuffer: audioManager.musicInfo
            onTextBufferChanged: changeMusicInfoTextAnimation.restart()
            SequentialAnimation {
                id: changeMusicInfoTextAnimation

                PropertyAnimation {
                    target: musicInfoText
                    property: "opacity"
                    from: 1; to: 0
                }
                ScriptAction { script: {
                        console.log(log_tag + "Setting music info text to \"" + musicInfoText.textBuffer + "\"")
                        musicInfoText.text = musicInfoText.textBuffer
                    }
                }
                PropertyAnimation {
                    target: musicInfoText
                    property: "opacity"
                    from: 0; to: 1
                }
            }

            PropertyAnimation {
                id: widenAnimation
                target: musicInfoText
                property: "width"
                from: boombox.width; to: 1000
                duration: 500
            }
            PropertyAnimation {
                id: shortenAnimation
                target: musicInfoText
                property: "width"
                from: 1000; to: boombox.width
                duration: 500
            }

            MouseArea {
                anchors.fill: musicInfoText
                hoverEnabled: musicInfoText.truncated

                onHoveredChanged: {
                    if (containsMouse) {
                        shortenAnimation.stop()
                        widenAnimation.start()
                    } else {
                        widenAnimation.stop()
                        shortenAnimation.start()
                    }
                }
            }

            Component.onCompleted: text = textBuffer;
        }

        Item {
            id: boomboxButtonContainer
            anchors.top: musicInfoText.bottom;
            width: childrenRect.width
            height: childrenRect.height
            anchors.horizontalCenter: boombox.horizontalCenter

            Button {
                id: playButton
                text: ""
                width: height
                height: 7/4 * playImg.height
                anchors.top: boomboxButtonContainer.top
                anchors.left: boomboxButtonContainer.left
                onButtonClicked: { audioManager.playPauseMusic(); console.debug("play the music from boombox")}

                Image {
                    id: playImg
                    anchors.centerIn: parent
                    source: {
                        if (audioManager.paused)
                            return "../imgs/play.png"
                        else
                            return "../imgs/pause.png"
                    }
                    height: 30
                    width: height + 20
                    fillMode: Image.PreserveAspectFit
                }
            }

            Button {
                id: rewindButton
                text: ""
                width: height
                height: 7/4 * rewindImg.height
                anchors.top: boomboxButtonContainer.top
                anchors.left: playButton.right
                onButtonClicked: audioManager.previousMusic();

                Image {
                    id: rewindImg
                    anchors.centerIn: parent
                    source: "../imgs/rewind.png"
                    height: 30
                    width: height + 20
                    fillMode: Image.PreserveAspectFit
                }
            }

            Button {
                id: fastforwardButton
                text: ""
                width: height
                height: 7/4 * fastforwardImg.height
                anchors.top: boomboxButtonContainer.top
                anchors.left: rewindButton.right
                onButtonClicked: audioManager.nextMusic();

                Image {
                    id: fastforwardImg
                    anchors.centerIn: parent
                    source: "../imgs/fastforward.png"
                    height: 30
                    width: height + 20
                    fillMode: Image.PreserveAspectFit
                }
            }

            Slider {
                id: boomboxVolumeSlider
                width: 220; height: 15
                anchors.top: playButton.bottom
                anchors.left: boomboxButtonContainer.left
                anchors.right: fastforwardButton.right
                anchors.topMargin: 10
                anchors.leftMargin: 10

                value: profileManager.musicVolume
                onValueChanged: {
                    if (profileManager.musicVolume !== value) profileManager.musicVolume = value
                    value = function() { return profileManager.musicVolume; }
                }
            }
        }

        Item {
            id: buttonContainer
            anchors { left: boombox.right; verticalCenter: boombox.verticalCenter }
            width: childrenRect.width
            height: childrenRect.height

            Button {
                id: helpButton
                text: "Help"
                anchors { left: parent.left; top: parent.top }
                width: quitButton.width
                onButtonClicked: { gameHelpFadeable.show() }
            }
            Button {
                id: quitButton
                text: "Leave"
                anchors { left: parent.left; top: helpButton.bottom }
                onButtonClicked: container.userQuit()
            }
        }
    }

    GameRules {
        id: gameHelpFadeable
        z: homeRowPrompt.z +1
        onCloseClicked: {gameHelpFadeable.hide();}
    }

    ConfigurableDialog {
        id: networkWaitDialog
        z: homeRowPrompt.z - 1
        text: "Waiting for opponent to choose a home row..."
        hasButton1: false

        property bool needToShow: false
    }

    ConfigurableDialog {
        id: networkFailureDialog
        z: homeRowPrompt.z + 10
        hasButton1: true
        hasThrobber: !isFatal

        property bool isFatal: true

        text: isFatal? "Opponent has quit the game." : "Connection has failed; attempting to reconnect..."
        button1Text: isFatal? "Leave" : "Give Up"
        onButton1Clicked: {userQuit(); networkFailureDialog.hide()}
    }

    ConfigurableDialog {
        id: gameOverDialog
        z: networkFailureDialog.z + 1
        hasThrobber: false
        text: "Game over"
        button1Text: "Leave"
        onButton1Clicked: userQuit();

        onOpacityChanged: {
            if(opacity === 1) {
                container.side = "none"
            }
        }
    }

    ConfigurableDialog {
        id: campaignDialog
        z: gameOverDialog.z
        hasThrobber: false
        text: "Game over"
        button1Text: (campaignDialog.text === "Game over: South wins")? "Next Level" : "Try Again"
        hasButton2: true
        button2Text: "Leave"

        onButton1Clicked: {
            campaignDialog.hide()
            nextCampaignLevel()
        }
        onButton2Clicked: {
            campaignDialog.hide()
            userQuit()
        }
    }

    Rectangle {
        width: container.height + 10; height: container.width + 10
        anchors.centerIn: parent
        rotation: 90
        gradient: Gradient {
            GradientStop { position: 0.0; color: "white" }
            GradientStop { position: 0.2; color: container.bgColor }
            GradientStop { position: 0.8; color: container.bgColor }
            GradientStop { position: 1.0; color: "white" }
        }
    }

    Board {
        id: board
        objectName: "board"
        height: parent.height
        highlightPieces: easyGame || tutorial || networkGame || localMultiplayer

        onButtonClick: container.buttonClick(pos)

        PropertyAnimation {
            id: boardSpinner
            target: board
            property: "rotation"

            property int spins: 1

            from: 0; to: 360*spins
            duration: 2000*spins
            easing.type: Easing.OutBack
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: container
        hoverEnabled: true
        z: board.z - 1

        onMousePositionChanged: {
            if((mouseX < board.x || mouseX > board.x + board.width) && heldPiece.piece !== 0)
                container.moveCanceled()
        }
        onClicked: {
            function contains(item, point) {
                if (item.x <= point.x && point.x <= (item.x + item.width) &&
                        item.y <= point.y && point.y <= (item.y + item.height)) {
                    return true;
                }
                return false;
            }

            if (heldPiece.piece !== 0) {
                if (!easyGame &&
                        (contains(board.getPositionByName("North Goal"), mapToItem(board, mouseX, mouseY))
                        || contains(board.getPositionByName("South Goal"), mapToItem(board, mouseX, mouseY)))) {
                    console.log(log_tag + "Reprimanding user for trying to win illegally");
                    var dialogComponent = Qt.createComponent("ConfigurableDialog.qml");
                    var dialog = dialogComponent.createObject(container, {
                                                                  "hasButton1": true,
                                                                  "hasButton2": false,
                                                                  "button1Text": "I'm sorry.",
                                                                  "hasThrobber": false,
                                                                  "text": "You can't get to that goal right now, so stop trying. Thanks."
                                                              });
                    dialog.show();

                    var dialogDestroyer = Qt.createQmlObject("import QtQuick 1.1; Timer {interval: 1000; repeat: false}", dialog, "dialogDestroyer");
                    dialogDestroyer.triggered.connect(
                                function() {
                                    console.log(log_tag + "Destroying reprimand dialog");
                                    dialog.destroy();
                                });

                    dialog.button1Clicked.connect(
                                function() {
                                    dialog.hide();
                                    dialogDestroyer.start()
                                });
                }

                container.moveCanceled();
            }
        }
    }

    BoardPosition {
        id: animatedPiece
        objectName: "animatedPiece"
        piece: 0
        x: -width
        y: -height
        width: board.pieceWidth
        height: board.pieceHeight
        z: board.z + 3
        enabled: false; mouseActive: false
        rotation: board.rotation
    }

    BoardPosition {
        id: animatedReplacedPiece
        objectName: "animatedReplacedPiece"
        piece: 0
        x: -width
        y: -height
        width: board.pieceWidth
        height: board.pieceHeight
        z: board.z + 3
        enabled: false; mouseActive: false
        rotation: board.rotation
    }

    BoardPosition {
        id: heldPiece
        objectName: "heldPiece"
        piece: 0
        width: 0; height: 0
        enabled: false; mouseActive: false
        rotation: board.rotation
        x: getX(); y: getY()
        z: board.z + 1

        function getX() {
            if(mouseArea.containsMouse || typeof(board.mouseLoc) === "undefined")
                return mouseArea.mouseX
            return board.mouseLoc.x
        }
        function getY() {
            if(mouseArea.containsMouse || typeof(board.mouseLoc) === "undefined")
                return mouseArea.mouseY
            return board.mouseLoc.y
        }
    }

    Rectangle {
        id: chatContainer
        width: jessicaRectangle.width; height: parent.height - jessicaRectangle.height
        anchors { bottom: parent.bottom; right: parent.right }

        color: "#414042"
        border { color: "#414042"; width: 3}
        radius: 30

        Rectangle {
            id: chatLogContainer
            anchors {
                top: parent.top
                bottom: chatInputContainer.top
                left: parent.left
                right: parent.right
                margins: 15
            }
            width: parent.width - 40
            radius: 10
            border.color: "transparent"//"#414042"
            border.width: 30

            ListView {
                id: chatLog
                anchors { top: chatLogHeader.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }
                clip: true
                highlightFollowsCurrentItem: true
                spacing: 5

                delegate: Component {
                    id: delegateComponent

                    Rectangle {
                        id: delegateTextRect
                        color: "transparent"
                        width: chatLog.width
                        height: delegateText.height

                        Text {
                            id: delegateText
                            text: t
                            width: chatLog.width
                            wrapMode: Text.Wrap
                            horizontalAlignment: alignment
                            font.pointSize: 12
                            color: "black"

                            anchors { left: parent.left; leftMargin: 5; right: parent.right; rightMargin: 5 }
                        }

                        ListView.onAdd: SequentialAnimation {
                            PropertyAction { target: delegateText; property: "opacity"; value: 0 }
                            PropertyAnimation { target: delegateText; property: "opacity"; to: 1; duration: 300 }
                        }
                    }
                }

                model: ListModel {
                }
            }

            Rectangle {
                id: chatLogHeader
                width: parent.width
                height: headerText.height + 10
                radius: parent.radius
                border.color: parent.border.color
                color: "#B0B0B0"

                anchors { top: parent.top; horizontalCenter: parent.horizontalCenter }

                ShadowedText {
                    id: headerText
                    anchors.centerIn: parent
                    text: "Game Log"
                    color: "white"

                    font.pointSize: 25
                }
            }
        }

        Rectangle {
            id: chatInputContainer
            width: container.width - penelopeRectangle.width - sendButton.width - 20
            height: inputBackground.height + 20
            anchors { bottom: parent.bottom; right: parent.right; rightMargin: 15 }

            color: parent.color
            radius: height / 10

            Rectangle {
                id: inputBackground
                anchors { left: parent.left; right: sendButton.left; margins: 10; verticalCenter: parent.verticalCenter }
                height: chatInput.height + 10
                color: "white" //"#9fffffff"

                border.color: "black"

                Text {
                    id: chatDefaultText
                    text: "<em>Type here...</em>"
                    textFormat: Text.RichText
                    font: chatInput.font
                    anchors { left: chatInput.left; leftMargin: chatInput.anchors.leftMargin / 2; verticalCenter: chatInput.verticalCenter }
                    opacity: 0
                    color: "#a0000000"
                    z: chatInput.z + 1

                    states: [
                        State {
                            name: "shown"
                            when: chatInput.text.length < 1
                            PropertyChanges { target: chatDefaultText; opacity: 1 }
                        },
                        State {
                            name: "hidden"
                            when: chatInput.text.length > 0
                            PropertyChanges { target: chatDefaultText; opacity: 0 }
                        }
                    ]

                    transitions: Transition {
                        from: "hidden"; to: "shown"
                        NumberAnimation {
                            target: chatDefaultText
                            property: "opacity"
                            duration: 500
                        }
                    }
                }

                TextInput {
                    id: chatInput
                    anchors { left: parent.left; leftMargin: 3; right: parent.right; rightMargin: 3; verticalCenter: parent.verticalCenter }

                    font.pointSize: 15

                    maximumLength: {
                        if(homeRowPrompt.opacity > 0 ||
                                campaignDialog.opacity > 0 ||
                                gameOverDialog.opacity > 0 ||
                                networkFailureDialog.opacity > 0 ||
                                networkWaitDialog.opacity > 0 ||
                                gameHelpFadeable.opacity > 0 ||
                                storyBlackout.opacity > 0 ||
                                container.opacity < 0)
                            0
                        else
                            32767 // the default
                    }

                    onAccepted: container.doSendChat()
                }
            }

            Button {
                id: sendButton
                text: "Send"
                label.font.pointSize: 15

                anchors { right: parent.right; verticalCenter: parent.verticalCenter }

                onButtonClicked: container.doSendChat()
            }
        }
    }

    Button {
        id: aiSuggestMoveButton
        text: "Suggest Move"
        objectName: "aiSuggestMoveButton"
        anchors.right: chatContainer.left
        anchors.bottom: container.bottom
        anchors.rightMargin: 20
        anchors.bottomMargin: chatInputContainer.height + 5
        visible: easyGame && !tutorial
    }

    HomeRowPrompt {
        id: homeRowPrompt
        z: 100

        title: "Choose Starting Positions"

        onHomeRowChosen: container.homeRowChosen(homeRow)
    }
}
