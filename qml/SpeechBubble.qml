import QtQuick 1.1

Fadeable {
    id: container
    width: bubble.width
    height: bubbleTail.y + bubbleTail.height

    property int maxWidth: 450
    property int tailDirection
    signal done

    function displayMessage(text, duration) {
        bubbleText.text = text;
        hideTimer.interval = duration;

        hideTimer.restart();

        container.show();
    }

    Timer {
        id: hideTimer
        onTriggered: { container.hide(); container.done() }

        repeat: false
    }

    Rectangle {
        id: bubble
        border { color: "black"; width: 1 }
        width: bubbleText.width + 20
        height: bubbleText.height + 10
        radius: height / 8

        Text {
            id: bubbleText
            font.pointSize: 15
            wrapMode: Text.Wrap
            anchors.centerIn: bubble
            onTextChanged: {width = maxWidth; width = Math.min(maxWidth, paintedWidth)}

            Component.onCompleted: {width = Math.min(maxWidth, paintedWidth)}
        }
    }
    Image {
        id: bubbleTail
        fillMode: Image.PreserveAspectFit
        anchors.top: bubble.bottom
        anchors.right: { if (tailDirection === Qt.BottomRightCorner) { return bubble.right; } else { return undefined; } }
        anchors.rightMargin: bubble.radius / 2
        anchors.left: { if (tailDirection === Qt.BottomLeftCorner) { return bubble.left; } else { return undefined; } }
        anchors.leftMargin: bubble.radius / 2
        height: 20
        z: bubble.z + 1
        mirror: tailDirection === Qt.BottomLeftCorner
        source: "../imgs/speechTail.png"
    }
}
