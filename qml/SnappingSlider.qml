import QtQuick 1.1
import Effects 1.0
import Wheelie 1.0

Item {
    id: container
    width: itemNames.length * 100; height: 40
    property string log_tag: "SnappingSlider.qml: "

    property int labelSize: 12
    property int selected
    onSelectedChanged: {
        if (selected != location.newIndex) {
            location.newIndex = selected
            if(!wheelArea.update)
                settingsModel.update()
            wheelArea.update = false
        }
    }

    property alias background: background

    property variant itemNames: ["One", "Two", "Three", "Four"]

    Text {
        id: alert
        anchors { right: parent.left; bottom: parent.top }
        color: "black"
    }


    Item {
        id: content
        anchors.fill: parent

        Rectangle {
            id: background; smooth: true
            color: "#FFB5F4"
            border { color: "#FF464646"; width: 2 }
            radius: height / 2
            width: slider.height; height: slider.width
            rotation: -90
            anchors.centerIn: slider

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#FFEC008C" }
                GradientStop { position: 1.0; color: "#FF9E005D" }
            }

            effect: DropShadow {
                color: "#AA000000"
                blurRadius: 8
                offset: Qt.point(3,3)
            }
        }

        ListModel {
            id: settingsModel

            function update() {
                settingsModel.clear()
                for(var index = 0; index < itemNames.length; ++index) {
                    if(index == container.selected)
                        settingsModel.append({"gridId": index, "selectable": true});
                    else
                        settingsModel.append({"gridId": settingsModel.count, "selectable": false})
                }
            }

            Component.onCompleted: {
                update()
            }
        }

        Component {
            id: settingsDelegate
            Item {
                id: delegateContainer
                width: slider.cellWidth

                Rectangle {
                    id: item
                    parent: location
                    x: (delegateContainer.x + delegateContainer.width / 2) - width / 2
                    y: delegateContainer.y

                    width: selectable ? slider.cellHeight * 1.3 : slider.cellHeight - 6; height: width
                    rotation: 45
                    radius: 2
                    visible: selectable
                    border.color: selectable ? "#FFA1A1A1" : "transparent"

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "#FFFFFFFF" }
                        GradientStop { position: 1.0; color: "#FF959595" }
                    }

                    smooth: true

                    Behavior on x {
                        enabled: item.state !== "active"
                        NumberAnimation { duration: 400; easing.type: Easing.OutBounce }
                    }

                    states: State {
                        name: "active"
                        when: selectable && location.currId === gridId
                        PropertyChanges {
                            target: item
                            x: location.mouseX - item.width / 2
                            z: 10
                        }
                    }
                }
            }
        }

        GridView {
            id: slider
            width: parent.width; height: container.height / 2
            anchors { top: parent.top; horizontalCenter: parent.horizontalCenter }

            cellWidth: width / settingsModel.count; cellHeight: height

            interactive: false

            model: settingsModel
            delegate: settingsDelegate

            MouseArea {
                id: location
                anchors.fill: parent
                hoverEnabled: true

                property int currId: -1
                property int newIndex: 0
                property int index: slider.indexAt(mouseX, mouseY)

                // Debug junk
                //                onCurrIdChanged: showStats()
                //                onNewIndexChanged: showStats()
                //                onIndexChanged: showStats()

                function showStats() { alert.text = getStatText() }
                function getStatText() {
                    var text = "currId: " + currId +
                            "\nnewIndex: " + newIndex +
                            "\nindex: " + index +
                            "\nindex.gridId: " + (index > -1 ? settingsModel.get(index).gridId : "undefined") +
                            "\nindex.selectable: " + (index > -1 ? settingsModel.get(index).selectable : "undefined") +
                            "\ncontainer.selected: " + container.selected
                    return text
                }

                // </Debug junk>


                function getCurrId() {
                    if(index !== -1 && settingsModel.get(index).selectable)
                        currId = settingsModel.get(newIndex = index).gridId
                }

                function doMove() {
                    if(currId !== -1 && index !== -1 && index !== newIndex)
                        settingsModel.move(newIndex, newIndex = index, 1)
                }

                onPressed: {
                    getCurrId()
                    if(currId < 0 && index > -1) {
                        // didn't click on the selected area
                        currId = settingsModel.get(index).gridId
                        doMove()
                    }
                }
                onPressAndHold: getCurrId()
                onReleased: {
                    selected = newIndex
                    currId = -1
                }
                onMousePositionChanged: {
                    if(mouseX > width || mouseX < 0)
                        currId = -1
                    doMove()
                }
            }
            WheelArea {
                id: wheelArea
                anchors.fill: parent
                property bool update: false
                onVerticalWheel: {
                    var next = selected
                    if(delta > 0 && selected < settingsModel.count - 1)
                        ++next
                    else if(delta < 0 && selected > 0)
                        --next
                    if(next != selected) {
                        settingsModel.move(next, selected, 1)
                        update = true
                        selected = next
                    }
                }
            }
        }

        GridView {
            id: labels
            width: slider.width
            cellWidth: slider.cellWidth
            anchors { top: slider.bottom; topMargin: container.labelSize / 2; horizontalCenter: slider.horizontalCenter }

            delegate: Rectangle {
                width: labels.cellWidth
                height: itemLabel.height
                color: "transparent"
                ShadowedText {
                    id: itemLabel
                    text: title
                    font { pointSize: container.labelSize; bold: true }
                    anchors.centerIn: parent
                    color: "white"
                }
            }
            model: ListModel { id: labelModel }

            Component.onCompleted: {
                // populate the label grid with the item names
                for(var name in container.itemNames)
                    labelModel.append({title: container.itemNames[name], index: name})
            }
        }
    }
}
