import QtQuick 1.1

// game board, consists of BoardPositions
Item {
    id: container
    anchors.centerIn: parent
    width: board.width

    property string log_tag: "Board.qml: "
    property int minSize: 450
    property variant mouseLoc
    property alias pieceWidth: board.cellWidth
    property alias pieceHeight: board.cellHeight
    property bool highlightPieces: true

    signal buttonClick(string pos)

    onWidthChanged: board.width = width < minSize ? minSize : width
    onHeightChanged: board.height = height < minSize ? minSize : height

    function getPositionByName(positionName) {
        if (positionName === "North Goal")
            return northGoal
        if (positionName === "South Goal")
            return southGoal

        if (positionName === "A1")
            return a1;
        if (positionName === "A2")
            return a2;
        if (positionName === "A3")
            return a3;
        if (positionName === "A4")
            return a4;
        if (positionName === "A5")
            return a5;
        if (positionName === "A6")
            return a6;
        if (positionName === "B1")
            return b1;
        if (positionName === "B2")
            return b2;
        if (positionName === "B3")
            return b3;
        if (positionName === "B4")
            return b4;
        if (positionName === "B5")
            return b5;
        if (positionName === "B6")
            return b6;
        if (positionName === "C1")
            return c1;
        if (positionName === "C2")
            return c2;
        if (positionName === "C3")
            return c3;
        if (positionName === "C4")
            return c4;
        if (positionName === "C5")
            return c5;
        if (positionName === "C6")
            return c6;
        if (positionName === "D1")
            return d1;
        if (positionName === "D2")
            return d2;
        if (positionName === "D3")
            return d3;
        if (positionName === "D4")
            return d4;
        if (positionName === "D5")
            return d5;
        if (positionName === "D6")
            return d6;
        if (positionName === "E1")
            return e1;
        if (positionName === "E2")
            return e2;
        if (positionName === "E3")
            return e3;
        if (positionName === "E4")
            return e4;
        if (positionName === "E5")
            return e5;
        if (positionName === "E6")
            return e6;
        if (positionName === "F1")
            return f1;
        if (positionName === "F2")
            return f2;
        if (positionName === "F3")
            return f3;
        if (positionName === "F4")
            return f4;
        if (positionName === "F5")
            return f5;
        if (positionName === "F6")
            return f6;
    }

    Rectangle {
        id: board
        color: "transparent"

        width: background.width; height: container.height
        anchors.centerIn: parent

        property int cellWidth: grid.width / grid.columns
        property int cellHeight: grid.height / grid.rows

        Image {
            id: background
            asynchronous: true
            source: "../imgs/board.svg"
            smooth: true

            height: container.height
            fillMode: Image.PreserveAspectFit
            anchors { verticalCenter: board.verticalCenter; horizontalCenter: board.horizontalCenter }
        }

        BoardPosition {
            id: northGoal
            width: board.cellWidth
            height: board.cellHeight
            position: "North Goal"
            highlightEnabled: highlightPieces

            anchors { bottom: grid.top; horizontalCenter: parent.horizontalCenter }
            onButtonClick: container.buttonClick(pos)
        }

        BoardPosition {
            id: southGoal
            width: board.cellWidth
            height: board.cellHeight
            position: "South Goal"
            highlightEnabled: highlightPieces

            anchors { top: grid.bottom; margins: 7; horizontalCenter: parent.horizontalCenter }
            onButtonClick: container.buttonClick(pos)
        }

        Grid {
            id: grid
            width: background.width - (background.width / columns) * 1.8
            height: width
            columns: 6; rows: 6
            anchors.horizontalCenter: parent.horizontalCenter
            y: background.height / 2 - height / 2 + 4

            BoardPosition { id: a6; width: board.cellWidth; height: board.cellHeight; position: "A6"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: b6; width: board.cellWidth; height: board.cellHeight; position: "B6"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: c6; width: board.cellWidth; height: board.cellHeight; position: "C6"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: d6; width: board.cellWidth; height: board.cellHeight; position: "D6"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: e6; width: board.cellWidth; height: board.cellHeight; position: "E6"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: f6; width: board.cellWidth; height: board.cellHeight; position: "F6"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }

            BoardPosition { id: a5; width: board.cellWidth; height: board.cellHeight; position: "A5"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: b5; width: board.cellWidth; height: board.cellHeight; position: "B5"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: c5; width: board.cellWidth; height: board.cellHeight; position: "C5"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: d5; width: board.cellWidth; height: board.cellHeight; position: "D5"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: e5; width: board.cellWidth; height: board.cellHeight; position: "E5"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: f5; width: board.cellWidth; height: board.cellHeight; position: "F5"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }

            BoardPosition { id: a4; width: board.cellWidth; height: board.cellHeight; position: "A4"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: b4; width: board.cellWidth; height: board.cellHeight; position: "B4"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: c4; width: board.cellWidth; height: board.cellHeight; position: "C4"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: d4; width: board.cellWidth; height: board.cellHeight; position: "D4"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: e4; width: board.cellWidth; height: board.cellHeight; position: "E4"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: f4; width: board.cellWidth; height: board.cellHeight; position: "F4"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }

            BoardPosition { id: a3; width: board.cellWidth; height: board.cellHeight; position: "A3"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: b3; width: board.cellWidth; height: board.cellHeight; position: "B3"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: c3; width: board.cellWidth; height: board.cellHeight; position: "C3"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: d3; width: board.cellWidth; height: board.cellHeight; position: "D3"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: e3; width: board.cellWidth; height: board.cellHeight; position: "E3"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: f3; width: board.cellWidth; height: board.cellHeight; position: "F3"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }

            BoardPosition { id: a2; width: board.cellWidth; height: board.cellHeight; position: "A2"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: b2; width: board.cellWidth; height: board.cellHeight; position: "B2"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: c2; width: board.cellWidth; height: board.cellHeight; position: "C2"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: d2; width: board.cellWidth; height: board.cellHeight; position: "D2"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: e2; width: board.cellWidth; height: board.cellHeight; position: "E2"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: f2; width: board.cellWidth; height: board.cellHeight; position: "F2"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }

            BoardPosition { id: a1; width: board.cellWidth; height: board.cellHeight; position: "A1"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: b1; width: board.cellWidth; height: board.cellHeight; position: "B1"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: c1; width: board.cellWidth; height: board.cellHeight; position: "C1"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: d1; width: board.cellWidth; height: board.cellHeight; position: "D1"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: e1; width: board.cellWidth; height: board.cellHeight; position: "E1"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
            BoardPosition { id: f1; width: board.cellWidth; height: board.cellHeight; position: "F1"; highlightEnabled: highlightPieces; onButtonClick: container.buttonClick(pos) }
        }

        Fadeable {
            id: cancelButton
            objectName: "cancelButtonFadeable"
            anchors { right: southGoal.left; rightMargin: 15; verticalCenter: southGoal.verticalCenter }
            width: children[0].width; height: children[0].height
            Button {
                objectName: "cancelButton"
                anchors.centerIn: parent
                text: "Cancel"

                onMouseMoved: {
                    container.mouseLoc = mapToItem(container.parent, x, y)
                }
            }
            state: "hidden"
        }

        Fadeable {
            id: acceptButton
            objectName: "acceptButtonFadeable"
            anchors { right: cancelButton.left; rightMargin: 15; verticalCenter: cancelButton.verticalCenter }
            width: children[0].width; height: children[0].height
            Button {
                objectName: "acceptButton"
                anchors.centerIn: parent
                text: "Accept"
            }
            state: "hidden"
        }
    }
}
