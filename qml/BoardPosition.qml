import QtQuick 1.1

// the individual pieces of the game board
Item {
    id: container
    width: 100; height: width
    property string log_tag: "BoardPosition.qml: "

    property color color: mouseActive ? "#80FFFFFF" : "#A0FFAAFD" //"#80E1D14D"
    property color mouseOverColor: "#A0A8FFD2"
    property color disabledColor: "transparent"
    property string position
    property bool highlightEnabled: true

    property string col: position.charAt(0)
    property int row: parseInt(position.charAt(1), 10)

    property int piece: 0
    property bool mouseActive: true
    enabled: false

    objectName: position

    signal buttonClick(string pos)

    onEnabledChanged: {
        if(container.enabled && highlightEnabled)
            cell.color = container.color
        else {
            cell.color = container.disabledColor
        }
    }

    Rectangle {
        id: cell
        color: container.disabledColor
        border.color: container.enabled && highlightEnabled ? "black" : "transparent"
        anchors.fill: parent
        radius: Math.max(width / 2, height / 2)
        smooth: true

        Behavior on color {
            ColorAnimation { duration: 500 }
        }

        MouseArea {
            id: mouseArea
            enabled: container.enabled && container.mouseActive
            hoverEnabled: enabled && mouseActive && highlightEnabled
            anchors.fill: cell
            acceptedButtons: mouseActive && enabled? Qt.LeftButton : Qt.NoButton

            onClicked: { container.buttonClick(container.objectName) }
            onEntered: {
                setMouseLoc()
                if(container.enabled)
                    cell.color = container.mouseOverColor
                else
                    cell.color = container.disabledColor
            }
            onExited: {
                if(container.enabled)
                    cell.color = container.color
                else
                    cell.color = container.disabledColor
                setMouseLoc(undefined)
            }
            onMousePositionChanged: {
                setMouseLoc(mouseX, mouseY)
            }

            function setMouseLoc(x, y) {
                var dest = container.parent.parent
                if(position.indexOf("Goal") < 0)
                    dest = dest.parent
               if(typeof x === "undefined" || typeof y === "undefined")
                {
                    dest.mouseLoc = undefined
                }
                else {
                    var loc = mapToItem(dest.parent, x, y)
                    dest.mouseLoc = { x:loc.x, y:loc.y }
                }
            }
        }

        AnimatedSprite {
            id: princess1
            anchors { top: parent.verticalCenter; topMargin: -15; horizontalCenter: parent.horizontalCenter }
            source: "../imgs/princess_sprite_sheet.png"
            frameRects: [[4,0,22,40], [26,0,22,40], [48,0,22,40], [70,0,22,40], [92,0,22,40]]
            running: container.enabled && container.highlightEnabled
            frameRate: 15
            forwardBack: true

            opacity: 0
        }
        AnimatedSprite {
            id: princess2
            anchors { bottom: parent.verticalCenter; bottomMargin: -5; right: princess1.left }
            source: "../imgs/princess_sprite_sheet.png"
            frameRects: [[4,0,22,40], [26,0,22,40], [48,0,22,40], [70,0,22,40], [92,0,22,40]]
            running: container.enabled && container.highlightEnabled
            frameRate: 15
            forwardBack: true

            opacity: 0
        }
        AnimatedSprite {
            id: princess3
            anchors { bottom: parent.verticalCenter; bottomMargin: -5; left: princess1.right }
            source: "../imgs/princess_sprite_sheet.png"
            frameRects: [[4,0,22,40], [26,0,22,40], [48,0,22,40], [70,0,22,40], [92,0,22,40]]
            running: container.enabled && container.highlightEnabled
            frameRate: 15
            forwardBack: true

            opacity: 0
        }
    }

    states: [
        State {
            name: "zeroPiece"
            when: container.piece === 0
            PropertyChanges { target: princess1; opacity: 0 }
            PropertyChanges { target: princess2; opacity: 0 }
            PropertyChanges { target: princess3; opacity: 0 }
        },
        State {
            name: "onePiece"
            when: container.piece === 1
            PropertyChanges { target: princess1; opacity: 1 }
            PropertyChanges { target: princess2; opacity: 0 }
            PropertyChanges { target: princess3; opacity: 0 }

            AnchorChanges { target: princess1; anchors.verticalCenter: parent.verticalCenter; anchors.top: undefined }
        },
        State {
            name: "twoPiece"
            when: container.piece === 2
            PropertyChanges { target: princess1; opacity: 0 }
            PropertyChanges { target: princess2; opacity: 1 }
            PropertyChanges { target: princess3; opacity: 1 }

            AnchorChanges { target: princess2; anchors.bottom: undefined; anchors.right: parent.horizontalCenter; anchors.verticalCenter: parent.verticalCenter }
            AnchorChanges { target: princess3; anchors.bottom: undefined; anchors.left: parent.horizontalCenter; anchors.verticalCenter: parent.verticalCenter }
        },
        State {
            name: "threePiece"
            when: container.piece === 3
            PropertyChanges { target: princess1; opacity: 1 }
            PropertyChanges { target: princess2; opacity: 1 }
            PropertyChanges { target: princess3; opacity: 1 }
        }
    ]
}
