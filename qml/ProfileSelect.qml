import QtQuick 1.1
import Effects 1.0

Item {
    id: container
    width: 400; height: 400
    property string log_tag: "ProfileSelect.qml: "
    property int text_size: 16

    signal profileChosen(string profileName)

    function takeFocus() {
        name.focus = true;
    }

    function sendName() {
        if(name.text.length > 0)
            profileChosen(name.text)
        else
            onInvalidProfile()
    }

    function onInvalidProfile() {
        nameText.color = "red"
    }

    ShadowedText {
        id: nameText
        text: "Please enter a Profile name:"
        color: "white"
        font.pointSize: text_size
        anchors { top: parent.top; horizontalCenter: parent.horizontalCenter; bottomMargin: 10 }
        font.bold: true

        Behavior on color {
            ColorAnimation { duration: 200 }
        }
    }

    Rectangle {
        id: inputBorder
        color: "white"
        border { color: "black" }

        width: name.width + 8
        height: name.height + 4
        radius: 3

        anchors.top: nameText.bottom
        anchors.topMargin: 5
        anchors.horizontalCenter: container.horizontalCenter

        TextInput {
            id: name
            font.pointSize: text_size
            width: 200
            anchors.centerIn: parent

            onAccepted: container.sendName()
        }
    }

    ListView{
        id: oldProfiles
        anchors.top: inputBorder.bottom;
        anchors.horizontalCenter: container.horizontalCenter
        width: inputBorder.width - 8
        height: container.height/3
        z: -5
        clip: true

        model: profileManager.profileList

        delegate: Rectangle {
            width: oldProfiles.width
            height: profileNameText.height
            color: "purple"
            radius: 5
            border.color: "black"

            ShadowedText {
                id: profileNameText
                color: "white"
                style: Text.Sunken
                anchors.centerIn: parent
                font.pointSize: text_size

                text: modelData
            }

            MouseArea {
                anchors.fill: parent
                onClicked:{  name.text = modelData; sendName() }
            }
        }
    }

    Rectangle {
        id: buttonSelectContainer
        width: childrenRect.width
        height: childrenRect.height + 30

        anchors { top: oldProfiles.bottom; horizontalCenter: parent.horizontalCenter; }

        color: "transparent"

        Button {
            id: playButton
            text: "Play";

            anchors.top: buttonSelectContainer.top
            anchors.topMargin: 10

            onButtonClicked: container.sendName()
        }

        Button {
            id: quitButton
            text: "Quit"
            anchors.left: playButton.right
            anchors.top: buttonSelectContainer.top
            anchors.topMargin: 10

            onButtonClicked: Qt.quit()
        }
    }
}
