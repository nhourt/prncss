#include "tutorialmanager.h"
#include "profilemanager.h"

#include <QTimer>
#include <QEventLoop>
#include <QDeclarativeItem>

TutorialManager::TutorialManager(QObject *gameBoard, QObject *parent) :
    BoardManager(gameBoard, parent), quitRequest(false), minTextDisplayTime(gameBoard->property("minTextDisplayTime").toInt())
{
    gameBoard->setProperty("tutorial", true);
    gameBoard->setProperty("minTextDisplayTime", 4000);
    connect(gameBoard, SIGNAL(userQuit()), this, SLOT(Quit()));
    board = Board();
    UpdateBoard(board);
}

TutorialManager::~TutorialManager()
{
    setAllMouseActive();
    UpdateBoard(Board());
    gameBoard->setProperty("minTextDisplayTime", minTextDisplayTime);
    gameBoard->setProperty("tutorial", false);
}

void TutorialManager::playTutorialGame()
{
    HomeRow northHome("321321"), southHome("123123");
    BoardPosition northP, southP;
    northP.SetRow(BOARDSIZE);
    southP.SetRow(1);

    for (char col = 'A'; col <= 'F'; northP.SetColumn (++col), southP.SetColumn(col)) {
        board[northP] = northHome[col];
        board[southP] = southHome[col];
    }

    //animateMoveObject
    QVector<DialogElement> penelopesDialogText;

    //populate array of strings
    penelopesDialogText.push_back(DialogElement("Hi! I'm Penelope and I'll introduce you to the PRNCSS Gyges game!", ""));
    penelopesDialogText.push_back(DialogElement("I want to have the best sweet sixteen party so I need to sabotage Jessica's party.", ""));
    penelopesDialogText.push_back(DialogElement("There are three types of pieces: 1's, 2's and 3's that can be used by <strong>either player</strong>.", ""));
    penelopesDialogText.push_back(DialogElement("First, each player selects their home row.", ""));
    penelopesDialogText.push_back(DialogElement("You can drag the pieces to change the starting order. Click \"Confirm\" when you are finished.", ""));
    penelopesDialogText.push_back(DialogElement("For now, we will just use a default home row to practice.", ""));
    penelopesDialogText.push_back(DialogElement("You need to get to Jessica's mansion on the other side of the board so that you can sabotage her sweet sixteen party,", ""));
    penelopesDialogText.push_back(DialogElement("but you can only move the pieces in <strong>the row closest to your goal</strong>.", ""));
    penelopesDialogText.push_back(DialogElement("You can do this by moving pieces or landing on other pieces.", ""));
    penelopesDialogText.push_back(DialogElement("Choose a piece from your back row", ""));
    penelopesDialogText.push_back(DialogElement("and land on another piece, but <strong>don't actually click the piece</strong>. You can only land on another piece <strong>using your last move.</strong>", ""));
    penelopesDialogText.push_back(DialogElement("When you land on a piece, you have the choice to move that many additional spaces or to replace the piece you have landed on.", ""));
    penelopesDialogText.push_back(DialogElement("Let's move additional spaces.", ""));
    penelopesDialogText.push_back(DialogElement("Since it's a 2 piece we can move 2 spaces.", ""));
    penelopesDialogText.push_back(DialogElement("Let's place the piece here.", ""));
    penelopesDialogText.push_back(DialogElement("Now it's the other player's turn.", ""));
    penelopesDialogText.push_back(DialogElement("This time, when we land on a piece, let's replace the piece that we landed on.", ""));
    penelopesDialogText.push_back(DialogElement("Choose a piece from your back row", ""));
    penelopesDialogText.push_back(DialogElement("and click on a piece we can land on to replace it.", ""));
    penelopesDialogText.push_back(DialogElement("We can replace the 1 piece that we landed on to any open space on the board", ""));
    penelopesDialogText.push_back(DialogElement("other than the winning spaces located by the mansions at each end of the board.", ""));
    penelopesDialogText.push_back(DialogElement("Let's place the piece here.", ""));
    penelopesDialogText.push_back(DialogElement("Now it's the other player's turn.", ""));
    penelopesDialogText.push_back(DialogElement("You can win by finding a path from a piece in your back row to the winning space on the opposite end of the board.", ""));
    penelopesDialogText.push_back(DialogElement("You must land exactly in the winning space, you cannot have extra moves left over.", ""));
    penelopesDialogText.push_back(DialogElement("Land on other pieces to move all the way across the board.", ""));
    penelopesDialogText.push_back(DialogElement("Select a piece from the back row.", ""));
    penelopesDialogText.push_back(DialogElement("Land on a piece and move that many additional spaces.", ""));
    penelopesDialogText.push_back(DialogElement("If we can land on another piece we can move additional spaces again.", ""));
    penelopesDialogText.push_back(DialogElement("We can move 3 spaces.", ""));
    penelopesDialogText.push_back(DialogElement("We can win using the extra moves from the piece that we landed on.", ""));
    penelopesDialogText.push_back(DialogElement("We have won, we can sabotage Jessica's party! Mine will definitely be the best!", ""));
    penelopesDialogText.push_back(DialogElement("Click the \"Leave\" button at the top left of the screen (next to the boom box) when you are ready to go back to the main menu.", ""));
    int i = 0;

    //game start welcome message
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    if(quitRequest)
        return;

    QEventLoop el;
    connect(gameBoard, SIGNAL(homeRowChosen(QString)), &el, SLOT(quit()));
    connect(gameBoard, SIGNAL(userQuit()), &el, SLOT(quit()));
    emit UIChooseStart(ProfileManager::instance()->profileName());
    el.exec();
    if(quitRequest)
        return;

    UpdateBoard(board);

    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    SpeechWithoutWait(penelopesDialogText[i++]);
    if(quitRequest)
        return;

    //TODO: highlight the piece on A1
    highlightPosition(BoardPosition("A1"));
    connect(gameBoard, SIGNAL(buttonClick(QString)), &el, SLOT(quit()));
    el.exec();
    if(quitRequest)
        return;
    DisableAll();

    //TODO: wait until the user clicks it then display
    WaitForSpeech(penelopesDialogText[i++]);

    //TODO: highlight the B1 space
    quickHighlightPosition(BoardPosition("B1"), 1000);

    //TODO: wait until the user has seen this space highlighted then display
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);

    //TODO: highlight all available move moves (that are 2 spaces away)
    QVector<BoardPosition> vec(3);
    vec.clear();
    vec.append(BoardPosition("A2"));
    vec.append(BoardPosition("B3"));
    vec.append(BoardPosition("C2"));
    quickHighlightPositions(vec, 2000);

    SpeechWithoutWait(penelopesDialogText[i++]);

    if(quitRequest)
        return;

    //TODO: unhilight all but B3
    highlightPosition(BoardPosition("B3"));
    el.exec();
    if(quitRequest)
        return;
    DisableAll();

    //TODO: wait until user has dragged the piece to B3 then display
    WaitForSpeech(penelopesDialogText[i++]);
    if(quitRequest)
        return;

    //TODO: Have opponent move piece from C6 to D6 to D3
    AnimateMove(Move::Deserialize("C6-ESSS"));
    WaitForSpeech(penelopesDialogText[i++]);
    if(quitRequest)
        return;

    SpeechWithoutWait(penelopesDialogText[i++]);

    //TODO: highlight B1
    highlightPosition(BoardPosition("B1"));
    el.exec();
    if(quitRequest)
        return;
    DisableAll();

    //TODO: wait until the user has clicked B1 then display
    SpeechWithoutWait(penelopesDialogText[i++]);
    if(quitRequest)
        return;

    //TODO: highlight B3 to do a replacement
    highlightPosition(BoardPosition("B3"));
    el.exec();
    if(quitRequest)
        return;
    DisableAll();

    //TODO: wait until the user has clicked B3 then display
    WaitForSpeech(penelopesDialogText[i++]);

    //TODO: highlight all available spaces to put the replaced piece    

    WaitForSpeech(penelopesDialogText[i++]);
    SpeechWithoutWait(penelopesDialogText[i++]);
    if(quitRequest)
        return;

    //TODO: highlight C6
    highlightPosition(BoardPosition("C6"));
    el.exec();
    if(quitRequest)
        return;
    DisableAll();

    WaitForSpeech(penelopesDialogText[i++]);

    //TODO: Have opponent move piece from C6 to D6 R B5
    AnimateMove(Move::Deserialize("C6-E-B5"));

    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);
    //WaitForSpeech(penelopesDialogText[i++]);
    SpeechWithoutWait(penelopesDialogText[i++]);
    if(quitRequest)
        return;

    //TODO: highlight C1
    highlightPosition(BoardPosition("C1"));
    el.exec();
    if(quitRequest)
        return;
    DisableAll();

    //TODO: wait until the user clicks C1
    WaitForSpeech(penelopesDialogText[i++]);

    //TODO: highlight B3
    quickHighlightPosition(BoardPosition("B3"), 1500);

    WaitForSpeech(penelopesDialogText[i++]);

    //TODO: highlight B5
    quickHighlightPosition(BoardPosition("B5"), 1500);

    WaitForSpeech(penelopesDialogText[i++]);

    //TODO: highlight all possible options for moving 3 spaces

    //WaitForSpeech(penelopesDialogText[i++]);
    SpeechWithoutWait(penelopesDialogText[i++]);

    //TODO: highlight the winning space
    highlightPosition(BoardPosition(NorthSide));
    el.exec();
    if(quitRequest)
        return;
    DisableAll();

    //TODO: wait until user has dragged to/clicked on the winning space the display
    WaitForSpeech(penelopesDialogText[i++]);
    WaitForSpeech(penelopesDialogText[i++]);

    connect(gameBoard, SIGNAL(userQuit()), &el, SLOT(quit()));
    el.exec();
    if(quitRequest)
        return;

    setAllMouseActive();
    UpdateBoard(Board());
}

void TutorialManager::highlightPosition(BoardPosition pos)
{
    if(quitRequest)
        return;
    DisableAll();
    QDeclarativeItem* position = gameBoard->findChild<QDeclarativeItem*>(QString(pos));
    position->setProperty("mouseActive", true);
    if(position)
        position->setEnabled(true);
}

void TutorialManager::quickHighlightPosition(BoardPosition pos, int duration)
{
    if(quitRequest)
        return;
    DisableAll();
    QDeclarativeItem* position = gameBoard->findChild<QDeclarativeItem*>(QString(pos));
    position->setProperty("mouseActive", false);
    if(position) {
        position->setEnabled(true);

        QEventLoop delayLoop;
        QTimer::singleShot(duration, &delayLoop, SLOT(quit()));
        connect(gameBoard, SIGNAL(userQuit()), &delayLoop, SLOT(quit()));
        delayLoop.exec();
    }
}

void TutorialManager::quickHighlightPositions(QVector<BoardPosition> positions, int duration)
{
    if(quitRequest)
        return;
    DisableAll();

    foreach (BoardPosition pos, positions)
    {
        QDeclarativeItem* position = gameBoard->findChild<QDeclarativeItem*>(QString(pos));
        position->setProperty("mouseActive", false);
        if(position) {
            position->setEnabled(true);
        }
    }

    QEventLoop delayLoop;
    QTimer::singleShot(duration, &delayLoop, SLOT(quit()));
    connect(gameBoard, SIGNAL(userQuit()), &delayLoop, SLOT(quit()));
    delayLoop.exec();
}

void TutorialManager::WaitForSpeech(DialogElement content)
{
    if(quitRequest)
        return;
    QVariant duration;
    QMetaObject::invokeMethod(gameBoard, "displayPenelopeText", Q_RETURN_ARG(QVariant, duration), Q_ARG(QVariant, content.text), Q_ARG(QVariant, content.soundUrl));

    QEventLoop delayLoop;
    QTimer::singleShot(duration.toInt(), &delayLoop, SLOT(quit()));
    connect(gameBoard, SIGNAL(userQuit()), &delayLoop, SLOT(quit()));
    delayLoop.exec();
}

void TutorialManager::SpeechWithoutWait(TutorialManager::DialogElement content)
{
    QMetaObject::invokeMethod(gameBoard, "displayPenelopeText", Q_ARG(QVariant, content.text), Q_ARG(QVariant, content.soundUrl));
}

void TutorialManager::setAllMouseActive()
{
    foreach (QObject *current, boardPositions) {
        current->setProperty("mouseActive", true);
    }
}

void TutorialManager::UIPositionClicked(QString pos)
{
    QDeclarativeItem* boardPos = gameBoard->findChild<QDeclarativeItem*>(pos);
    int swap = heldPiece->property("piece").toInt();
    heldPiece->setProperty("piece", boardPos->property("piece").toInt());
    boardPos->setProperty("piece", swap);
}

void TutorialManager::Quit()
{
    quitRequest = true;
}
