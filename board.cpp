#include "board.h"

#include <vector>
#include <string>
#include <algorithm>
#include <qqueue.h>
#include <QDate>

Board::Board() : northGoal (NoPiece), southGoal (NoPiece), gameState (InProgress)
{
    for (short i = 0; i < BOARDSIZE; ++i)
        for (short j = 0; j < BOARDSIZE; ++j)
            board[i][j] = NoPiece;
}

Board::Board (const Board& other) : QObject(other.parent()), northGoal (other.northGoal), southGoal (other.southGoal), gameState (other.gameState)
{
    for (short i = 0; i < BOARDSIZE; ++i)
        for (short j = 0; j < BOARDSIZE; ++j)
            board[i][j] = other.board[i][j];
}

bool Board::operator== (const Board& other) const
{
    for (short i = 0; i < BOARDSIZE; ++i)
        for (short j = 0; j < BOARDSIZE; ++j)
            if (board[i][j] != other.board[i][j])
                return false;
    if (northGoal != other.northGoal || southGoal != other.southGoal || gameState != other.gameState)
        return false;
    return true;
}

bool Board::operator!= (const Board& other) const
{
    return ! (operator== (other));
}

Board& Board::operator= (const Board& other)
{
    for (short i = 0; i < BOARDSIZE; ++i)
        for (short j = 0; j < BOARDSIZE; ++j)
            board[i][j] = other.board[i][j];

    northGoal = other.northGoal;
    southGoal = other.southGoal;
    gameState = other.gameState;

    return *this;
}

BadMoveReason Board::CanMakeMove (PlayerSide player, const Move& move) const
{
    return Board (*this).ApplyMove (player, move);
}

BadMoveReason Board::ApplyMove (PlayerSide player, const Move& move)
{
    if (gameState != InProgress)
        return BMRGameOver;

    std::vector<BreadCrumb> movesMade;

    //Check that there is a piece at the start position, and that the start position is on this player's home row
    BoardPosition pos = move.GetStartPosition();
    if (OutOfBounds (pos))
        return BMROutOfBounds;
    if ( At(pos) == NoPiece)
        return BMRMissingStartPiece;
    if (pos.row != FindHomeRowIndex (player))
        return BMRNotOnHomeRow;

    Board workBoard (*this);
    GamePiece movedPiece = workBoard[pos];
    workBoard[pos] = NoPiece;
    GamePiece currentPiece = movedPiece;

    move.ResetGetNextDirection();

    try {
        while (move.GetNextDirectionCount() < move.GetDirectionCount()) {
            MoveDirection d;

            //Switch on type of piece to move. Depending on what kind of piece is moved, this will execute one, two, or all three cases.
            switch (currentPiece) {
            case TriplePiece:
                d = move.GetNextDirection();

                if (OutOfBounds (pos.PeekDirection (d, true)))
                    return BMRMoveOffBoard;

                if (std::find (movesMade.begin(), movesMade.end(), BreadCrumb (pos, pos.PeekDirection (d))) != movesMade.end())
                    return BMRHitBreadcrumb;
                movesMade.push_back (BreadCrumb (pos, pos.PeekDirection (d)));

                pos.MovePosition (d);
                if (workBoard[pos] != NoPiece)
                    return BMRCollision;
            case DoublePiece:
                d = move.GetNextDirection();

                if (OutOfBounds (pos.PeekDirection (d, true)))
                    return BMRMoveOffBoard;

                if (std::find (movesMade.begin(), movesMade.end(), BreadCrumb (pos, pos.PeekDirection (d))) != movesMade.end())
                    return BMRHitBreadcrumb;
                movesMade.push_back (BreadCrumb (pos, pos.PeekDirection (d)));

                pos.MovePosition (d);
                if (workBoard[pos] != NoPiece)
                    return BMRCollision;
            case SinglePiece:
                d = move.GetNextDirection();

                //Check for win
                if ( (player == SouthSide && d == Up && pos.row == 0) ||
                        (player == NorthSide && d == Down && pos.row == BOARDSIZE - 1)) {
                    if (move.GetNextDirectionCount() != move.GetDirectionCount())
                        return BMRMoveOffBoard;

                    if (player == NorthSide) {
                        workBoard.gameState = NorthWin;
                        workBoard.southGoal = movedPiece;
                    } else {
                        workBoard.gameState = SouthWin;
                        workBoard.northGoal = movedPiece;
                    }

                    (*this) = workBoard;
                    return BMRValidMove;
                }

                if (OutOfBounds (pos.PeekDirection (d, true)))
                    return BMRMoveOffBoard;

                if (std::find (movesMade.begin(), movesMade.end(), BreadCrumb (pos, pos.PeekDirection (d))) != movesMade.end())
                    return BMRHitBreadcrumb;
                movesMade.push_back (BreadCrumb (pos, pos.PeekDirection (d)));

                pos.MovePosition (d);
                break;
            default:
                return BMRTooManyDirections;
            }
            currentPiece = workBoard[pos];
        }
    } catch (Move::OutOfDirectionsException e) {
        return BMRIncompleteMove;
    }

    if (move.IsReplacement()) {
        BoardPosition repTo = move.GetReplacePosition();
        if (OutOfBounds (repTo))
            return BMRReplaceOutOfBounds;
        if ( (player == NorthSide && repTo.row > FindHomeRowIndex (SouthSide)) ||
                (player == SouthSide && repTo.row < FindHomeRowIndex (NorthSide)))
            return BMRReplaceOutOfBounds;
        if (workBoard[pos] == NoPiece)
            return BMRMissingReplacePiece;
        if (workBoard[repTo] != NoPiece)
            return BMRReplaceCollision;

        GamePiece repPiece = workBoard[pos];
        workBoard[pos] = movedPiece;
        workBoard[repTo] = repPiece;
    } else if (workBoard[pos] != NoPiece) {
        return BMRIncompleteMove;
    } else
        workBoard[pos] = movedPiece;

    if (workBoard == (*this))
        return BMRSameBoard;

    (*this) = workBoard;
    return BMRValidMove;
}

GamePiece& Board::operator[] (const BoardPosition& position)
{
    if (position.inGoal)
        return (position.whichGoal == NorthSide) ? northGoal : southGoal;

    return board[position.row][position.column];
}

const GamePiece& Board::operator[] (const BoardPosition& position) const
{
    if (position.inGoal)
        return (position.whichGoal == NorthSide) ? northGoal : southGoal;

    return board[position.row][position.column];
}

GameOverState Board::GameState()
{
    return gameState;
}

QList< Move > Board::FindMoves (BoardPosition begin, BoardPosition end) const
{
    QList<Move> moves = FindAllMoves(begin);

    int longestMoveLength = 0;
    Move longestMove;

    foreach (Move m, moves) {
        if (m.GetEndPosition() != end)
            moves.removeAll(m);
        else if (m.GetDirectionCount() > longestMoveLength)
            longestMove = m;
    }

    moves.prepend(longestMove);
    
    return moves;
}

QList<Move> Board::FindAllMoves (BoardPosition startPos) const
{
    Board workBoard(*this);
    
    PlayerSide ps = UnknownSide;
    if (startPos.row == FindHomeRowIndex (SouthSide))
        ps = SouthSide;
    else if (startPos.row == FindHomeRowIndex (NorthSide))
        ps = NorthSide;
    
    QQueue<SearchNode> nodes;
    nodes.enqueue (SearchNode (startPos, workBoard[startPos]));
    QList<Move> found;
    
    workBoard[startPos] = NoPiece;

    while (!nodes.empty()) {
        SearchNode n = nodes.dequeue();

        //If we are out of moves, see if we are on a piece and can get more
        if (n.movesRemaining == 0) {
            n.movesRemaining = workBoard[n.endpos];
        }
        //If still no moves, this is the end of the road; push and find another path
        //Also check that we are not in our own goal
        //Also, check that the move is legal
        if (n.movesRemaining == 0) {
            if (!(n.endpos.InGoal() && n.endpos.WhichGoal() == ps) && CanMakeMove(ps, n.move) == BMRValidMove)
                found.push_back (n.move);
            continue;
        } else if (workBoard[n.endpos] != NoPiece) {
            //And add the current move to the list (it's a potential replace)
            found.append(n.move);
        }
        
        //If we are in a goal, we can go no further
        if (n.endpos.InGoal())
            continue;
        
        //Moves remaining; see which directions we can go
        //We can only go a given direction if the board position in that direction is empty
        // or we have only one move left
        SearchNode m = n;
        if (!OutOfBounds(m.endpos.MovePosition(Left, true), true) &&
            (workBoard[m.endpos] == NoPiece || m.movesRemaining == 1) &&
            !m.breadcrumbs.contains(BreadCrumb(m.endpos, n.endpos))) {
            m.breadcrumbs.append(BreadCrumb(m.endpos, n.endpos));
            m.move.AddDirection (Left);
            --m.movesRemaining;
            nodes.enqueue (m);
        }
        m = n;
        if (!OutOfBounds(m.endpos.MovePosition(Right, true), true) &&
            (workBoard[m.endpos] == NoPiece || m.movesRemaining == 1) &&
            !m.breadcrumbs.contains(BreadCrumb(m.endpos, n.endpos))) {
            m.breadcrumbs.append(BreadCrumb(m.endpos, n.endpos));
            m.move.AddDirection (Right);
            --m.movesRemaining;
            nodes.enqueue (m);
        }
        m = n;
        if (!OutOfBounds(m.endpos.MovePosition(Up, true), true) &&
            (workBoard[m.endpos] == NoPiece || m.movesRemaining == 1) &&
            !m.breadcrumbs.contains(BreadCrumb(m.endpos, n.endpos))) {
            m.breadcrumbs.append(BreadCrumb(m.endpos, n.endpos));
            m.move.AddDirection (Up);
            --m.movesRemaining;
            nodes.enqueue (m);
        }
        m = n;
        if (!OutOfBounds(m.endpos.MovePosition(Down, true), true) &&
            (workBoard[m.endpos] == NoPiece || m.movesRemaining == 1) &&
            !m.breadcrumbs.contains(BreadCrumb(m.endpos, n.endpos))) {
            m.breadcrumbs.append(BreadCrumb(m.endpos, n.endpos));
            m.move.AddDirection (Down);
            --m.movesRemaining;
            nodes.enqueue (m);
        }
    }

    return found;
}

bool Board::HasMove (const BoardPosition& startPos) const
{
    //A single piece can ALWAYS make a move
    if ((*this)[startPos] == SinglePiece)
        return true;
    
    QQueue<SearchNode> nodes;
    nodes.enqueue (SearchNode (startPos, (*this)[startPos]));
    
    while (!nodes.empty()) {
        SearchNode n = nodes.dequeue();
        
        //If we are in a goal, we can go no further
        if (n.endpos.InGoal())
            continue;
        
        //If a piece actually gets to having no remaining moves, then it definitely has a move
        if (n.movesRemaining == 0) {
            return true;
        }
        
        //Moves remaining; see which directions we can go
        //We can only go a given direction if the board position in that direction is empty
        //or we have only one move left
        SearchNode m = n;
        if (!OutOfBounds(m.endpos.MovePosition(Left, true), true) &&
            ((*this)[m.endpos] == NoPiece || m.movesRemaining == 1) &&
            !m.breadcrumbs.contains(BreadCrumb(m.endpos, n.endpos))) {
                m.breadcrumbs.append(BreadCrumb(m.endpos, n.endpos));
                m.move.AddDirection (Left);
                --m.movesRemaining;
                nodes.enqueue (m);
        }
        m = n;
        if (!OutOfBounds(m.endpos.MovePosition(Right, true), true) &&
            ((*this)[m.endpos] == NoPiece || m.movesRemaining == 1) &&
            !m.breadcrumbs.contains(BreadCrumb(m.endpos, n.endpos))) {
                m.breadcrumbs.append(BreadCrumb(m.endpos, n.endpos));
                m.move.AddDirection (Right);
                --m.movesRemaining;
                nodes.enqueue (m);
        }
        m = n;
        if (!OutOfBounds(m.endpos.MovePosition(Up, true), true) &&
            ((*this)[m.endpos] == NoPiece || m.movesRemaining == 1) &&
            !m.breadcrumbs.contains(BreadCrumb(m.endpos, n.endpos))) {
                m.breadcrumbs.append(BreadCrumb(m.endpos, n.endpos));
                m.move.AddDirection (Up);
                --m.movesRemaining;
                nodes.enqueue (m);
        }
        m = n;
        if (!OutOfBounds(m.endpos.MovePosition(Down, true), true) &&
            ((*this)[m.endpos] == NoPiece || m.movesRemaining == 1) &&
            !m.breadcrumbs.contains(BreadCrumb(m.endpos, n.endpos))) {
                m.breadcrumbs.append(BreadCrumb(m.endpos, n.endpos));
                m.move.AddDirection (Down);
                --m.movesRemaining;
                nodes.enqueue (m);
        }
    }
    
    //All paths have been checked, and no moves have been found
    return false;
}

void Board::Rotate()
{
    for (short row = 0; row < BOARDSIZE / 2; ++row)
        for (short col = 0; col < BOARDSIZE; ++col)
            std::swap (board[row][col], board[BOARDSIZE - 1 - row][BOARDSIZE - 1 - col]);
    std::swap (northGoal, southGoal);
}

short int Board::FindHomeRow (PlayerSide player) const
{
    return BOARDSIZE - FindHomeRowIndex (player);
}

HomeRow Board::GetHomeRow (PlayerSide player) const
{
    HomeRow homeRow;
    short row = FindHomeRowIndex (player);
    
    for (short i = 0; i < BOARDSIZE; ++i)
        homeRow['A'+i] = board[row][i];
    
    return homeRow;
}

void Board::QuitGame()
{
    gameState = OpponentQuit;
}

short int Board::FindHomeRowIndex (PlayerSide player) const
{
    //For north player, start at row 0 and work down the board searching for a piece
    if (player == NorthSide)
        for (short row = 0; row < BOARDSIZE; ++row)
            for (short col = 0; col < BOARDSIZE; ++col) {
                BoardPosition p;
                p.row = row;
                p.column = col;
                if (board[row][col] != NoPiece && HasMove(p))
                    return row;
            }
    //For south player, start at last row and work up the board searching for a piece
    for (short row = BOARDSIZE - 1; row >= 0; --row)
        for (short col = 0; col < BOARDSIZE; ++col) {
            BoardPosition p;
            p.row = row;
            p.column = col;
            if (board[row][col] != NoPiece && HasMove(p))
                return row;
        }

    //Should never get here...
    return 0;
}

bool Board::OutOfBounds (BoardPosition position, bool includeGoalsInBounds) const
{
    if (includeGoalsInBounds && position.inGoal)
        return false;
    if (position.row < 0 || position.row > BOARDSIZE - 1 || position.column < 0 || position.column > BOARDSIZE - 1)
        return true;
    return false;
}

QDebug operator<< (QDebug dbg, const Board& b)
{
    BoardPosition bp (NorthSide);
    dbg.nospace() << "        " << b[bp];

    for (short i = 0; i < BOARDSIZE; ++i) {
        bp.MovePosition (Down);
        bp.SetColumn ('A');
        dbg.nospace() << "\n";
        for (short j = 0; j < BOARDSIZE; ++j) {
            dbg.space() << b[bp];
            bp.MovePosition (Right);
        }
    }

    bp.MovePosition (Down);
    dbg.nospace() << "\n         " << b[bp];

    return dbg.space();
}
QDebug operator<< (QDebug dbg, BadMoveReason r)
{
    switch (r) {
    case BMRValidMove:
        dbg.nospace() << "Valid move";
        break;
    case BMROutOfBounds:
        dbg.nospace() << "Move out of bounds";
        break;
    case BMRReplaceOutOfBounds:
        dbg.nospace() << "Replacement out of bounds";
        break;
    case BMRMissingStartPiece:
        dbg.nospace() << "No piece to move";
        break;
    case BMRNotOnHomeRow:
        dbg.nospace() << "Piece not on home row";
        break;
    case BMRMoveOffBoard:
        dbg.nospace() << "Piece moved off of board";
        break;
    case BMRCollision:
        dbg.nospace() << "Piece moved into occupied space";
        break;
    case BMRHitBreadcrumb:
        dbg.nospace() << "Piece crossed its own path";
        break;
    case BMRIncompleteMove:
        dbg.nospace() << "Insufficient directions to complete move";
        break;
    case BMRTooManyDirections:
        dbg.nospace() << "Too many directions for piece to move";
        break;
    case BMRMissingReplacePiece:
        dbg.nospace() << "No piece to replace";
        break;
    case BMRReplaceCollision:
        dbg.nospace() << "Piece replaced to occupied space";
        break;
    case BMRSameBoard:
        dbg.nospace() << "Board unchanged by move";
        break;
    case BMRGameOver:
        dbg.nospace() << "Game already over";
        break;
    }

    return dbg.space();
}

QDebug operator<< (QDebug dbg, BoardPosition p)
{
    dbg.nospace() << p.GetColumn() << p.GetRow();
    return dbg.space();
}

#include "moc_board.cpp"
