#include "stubplayer.h"

#include <QInputDialog>
#include <QApplication>

HomeRow StubPlayer::ChooseStartingPositions (const Board& board)
{
    QString rowString = QInputDialog::getText(0, tr("Choose home row"), tr("Please choose your home row."));
    HomeRow row (rowString);
    return row;
}

Move StubPlayer::MakeMove (const Board& board)
{
    Move m;
    QString boardString;
    QDebug (&boardString) << side << ", please make a move on this board:\n" << board;
    boardString.replace (" ,", ",");
    QString moveText = QInputDialog::getText (NULL, "Make a move", boardString);

    QStringList brokenString = moveText.split (" ", QString::SkipEmptyParts);

    if (brokenString.length() == 1) {
        if (side == SouthSide)
            m = board.FindMoves(brokenString[0], NorthSide).first();
        else
            m = board.FindMoves(brokenString[0], SouthSide).first();
    } else {
        m = board.FindMoves(brokenString[0], brokenString[1]).first();

        if (brokenString.length() == 3) {
            m.AddReplacement (brokenString[2]);
        }
    }

    return m;
}
