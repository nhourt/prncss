#ifndef GAMEENGINE_H
#define GAMEENGINE_H
#include "board.h"
#include "types.h"
#include "player.h"

#include <cstdlib>
#include <QThread>
#include <QApplication>

/**
 * Singleton Game Engine class
 */
class GameEngine : public QObject
{
    Q_OBJECT
public:
    class UserAbortedGameException{};

    /**
     * An exception class which indicates that a game was started with one or two missing players
     */
    struct MissingPlayerException : public PRNCSSException {
        MissingPlayerException (const char* reason) : PRNCSSException (reason) {}
    };

    /**
     * @brief Retrieve a pointer to the singleton GameEngine object
     */
    static GameEngine* GetEngine();

    /**
     * @brief Tells whose turn it currently is in the game
     * @return The side whose turn it is
     */
    PlayerSide CurrentTurn();

public slots:
    /**
     * @brief Set the players
     */
    void SetPlayers (Player* northPlayer, Player* southPlayer);

    /**
     * @brief Get the board for the game currently being played
     * Note that this method returns a copy of the board; not the original
     * @return The game board
     */
    Board GetBoard() const;

    /**
     * @brief Plays the game
     * Note that SetPlayers must be called before this!
     */
    void PlayGame();
    
signals:
    void BoardUpdated(const Board &board);
    void MoveMade(QVariant moveString);
    void GameOver(GameOverState state);
    
private slots:
    void EndGame();

private:
    static GameEngine* engine;
    static QThread *engineThread;
    static bool terminationRequested;
    static bool inGame;
    Board gameBoard;
    Player* north;
    Player* south;
    Player* currentPlayer;
    PlayerSide currentPlayerSide;

    void DeletePlayers();

    GameEngine() : north (NULL), south (NULL), currentPlayer (NULL) {
        connect(QApplication::instance(), SIGNAL(aboutToQuit()), this, SLOT(deleteLater()));
    }
    virtual ~GameEngine();

    void SetInitialHomeRow (HomeRow row, PlayerSide side);
    void NextTurn();
    
    const static short MAXBADMOVES;
};

#endif // GAMEENGINE_H
