#include "audiomanager.h"
#include "profilemanager.h"

#include <QFile>
#include <QTimer>
#include <QFileInfo>
#include <QStringList>
#include <QPauseAnimation>
#include <QPropertyAnimation>

const int FADE_DURATION = 750;

AudioManager* AudioManager::instance()
{
    if (theManager == NULL)
        theManager = new AudioManager;
    
    return theManager;
}

void AudioManager::setMusic (const QString &file)
{
    music->setCurrentSource(Phonon::MediaSource(file));
    playList.clear();
    playList += file;
}

void AudioManager::addNextMusic (const QString& file)
{
    music->enqueue(file);
    playList += file;
}

void AudioManager::setPlaylist (const QStringList &files)
{
    playList.clear();
    foreach (const QString &file, files)
        playList += file;

    music->setQueue(playList);
}

void AudioManager::playMusic()
{
    if (playList.empty())
        return;

    qDebug() << __FILE__ << __LINE__ << "Starting music";
    music->clear();
    music->setQueue(playList);
    music->play();
}

void AudioManager::playPauseMusic()
{
    if (playList.empty())
        return;

    if (music->state() == Phonon::PlayingState)
        music->pause();
    else if (music->state() == Phonon::PausedState)
        music->play();
}

void AudioManager::stopMusic(int fadeOutTime)
{
    QPropertyAnimation *musicFadeOut = new QPropertyAnimation(musicOutput, "volume");
    musicFadeOut->setStartValue(musicOutput->volume());
    musicFadeOut->setEndValue(0);
    musicFadeOut->setDuration(fadeOutTime);

    connect(musicFadeOut, SIGNAL(finished()), music, SLOT(stop()));
    musicFadeOut->start(QAbstractAnimation::DeleteWhenStopped);
}

void AudioManager::nextMusic()
{
    if (!ProfileManager::instance()->musicEnabled() || playList.empty())
        return;

    while (playList.back().fileName() != music->currentSource().fileName()) {
        playList.push_back(playList.front());
        playList.pop_front();
    }
    playList.push_back(playList.front());
    playList.pop_front();

    music->clear();
    music->setCurrentSource(playList.last());
    music->play();
    music->setQueue(playList);
}

void AudioManager::previousMusic()
{
    if (!ProfileManager::instance()->musicEnabled() || playList.empty())
        return;

    if (music->currentTime() < 5000) {
        while (playList[1].fileName() != music->currentSource().fileName()) {
            playList.push_back(playList.front());
            playList.pop_front();
        }
        playList.push_back(playList.front());
        playList.pop_front();

        music->clear();
        music->setCurrentSource(playList.last());
        music->play();
        music->setQueue(playList);
    } else {
        music->seek(0);
    }
}

QString AudioManager::musicInfo()
{
    return QFileInfo(music->currentSource().fileName()).completeBaseName();
}

qint64 AudioManager::playEffect(const QString &file)
{
    qDebug() << __FILE__ << __LINE__ << "Playing effect" << file << QFile(file).exists();

    if (ProfileManager::instance()->effectsEnabled() == false)
        return 0;
    
    if (effects->state() != Phonon::StoppedState && effects->state() != Phonon::PausedState)
        effects->clear();

    QEventLoop timeWaiter;
    connect(effects, SIGNAL(totalTimeChanged(qint64)), &timeWaiter, SLOT(quit()));
    effects->setCurrentSource(file);
    effects->play();

    timeWaiter.exec();
    return effects->totalTime();
}

qint64 AudioManager::playVoice(const QString& file)
{
    if (ProfileManager::instance()->voiceEnabled() == false)
        return 0;

    if (!QFile(file).exists()) {
        qWarning() << "Refusing to play nonexistent voice file:" << file;
        return 0;
    }

    voice->stop();

    if(!QFile::exists(file))
    {
        qDebug() << __FILE__ << __LINE__ << file << "does not exist";
        return 0;
    }

    QEventLoop timeWaiter;
    voice->clear();
    qreal volumeBackup = voiceOutput->volume();
    voiceOutput->setVolume(0);
    connect(voice, SIGNAL(totalTimeChanged(qint64)), &timeWaiter, SLOT(quit()));
    voice->setCurrentSource(file);
    voice->play();
    timeWaiter.exec();
    int duration = voice->totalTime();
    voice->clear();
    voice->setCurrentSource(file);
    voiceOutput->setVolume(volumeBackup);


    if (ProfileManager::instance()->musicEnabled()) {
        qreal fadedVolume = .1;

        if (voicePlayerAnimation->state() == QAbstractAnimation::Running)
            voicePlayerAnimation->stop();

        voicePlayerAnimation->animationAt(0)->setProperty("startValue", musicOutput->volume());
        voicePlayerAnimation->animationAt(0)->setProperty("endValue", fadedVolume);
        voicePlayerAnimation->animationAt(1)->setProperty("duration", duration);
        voicePlayerAnimation->animationAt(2)->setProperty("startValue", fadedVolume);
        voicePlayerAnimation->animationAt(2)->setProperty("endValue", ProfileManager::instance()->musicVolume());
        voicePlayerAnimation->start();
    } else {
        voice->play();
    }

    qDebug() << __FILE__ << __LINE__ << "Playing voice file" << file << "of duration" << duration;

    return duration + FADE_DURATION;
}

void AudioManager::stopVoice()
{
    //Stop the music faders and the voice player
    voicePlayerAnimation->stop();
    voice->stop();

    //Fade the music back in
    voicePlayerAnimation->animationAt(2)->setProperty("startValue", musicOutput->volume());
    voicePlayerAnimation->animationAt(2)->start();
}

bool AudioManager::paused()
{
    return music->state() == Phonon::PausedState;
}

void AudioManager::profileChanged()
{
    ProfileManager *profile = ProfileManager::instance();

    effectsOutput->setVolume(profile->effectsVolume());
    voiceOutput->setVolume(profile->voiceVolume());

    musicEnabledChanged(profile->musicEnabled());

    QPropertyAnimation *volumeAnim = new QPropertyAnimation(musicOutput, "volume", this);
    volumeAnim->setStartValue(musicOutput->volume());
    volumeAnim->setEndValue(profile->musicVolume());
    volumeAnim->setDuration(300);
    volumeAnim->start(QAbstractAnimation::DeleteWhenStopped);
}

void AudioManager::musicEnabledChanged(bool enabled)
{
    if (enabled == false)
        music->stop();
    else
        music->play();
}

void AudioManager::musicVolumeChanged(qreal volume)
{
    if (qAbs(volume - musicOutput->volume()) > .2) {
        QPropertyAnimation *musicFader = new QPropertyAnimation(musicOutput, "volume");
        musicFader->setStartValue(musicOutput->volume());
        musicFader->setEndValue(volume);
        musicFader->setDuration(300);
        musicFader->start(QAbstractAnimation::DeleteWhenStopped);
    } else {
        musicOutput->setVolume(volume);
    }
}

void AudioManager::restartPlaylist()
{
    qDebug() << __FILE__ << __LINE__ << "Music about to finish; restarting playlist.";
    music->setQueue(playList);
}

void AudioManager::musicStateChanged(Phonon::State newState, Phonon::State oldState)
{
    if (newState == Phonon::PausedState && oldState != Phonon::PausedState)
        emit pauseChanged(true);
    else if (newState != Phonon::PausedState && oldState == Phonon::PausedState)
        emit pauseChanged(false);
}

void AudioManager::musicInfoChanged()
{
    emit musicChanged(musicInfo());
}

void AudioManager::initializeAudioObjects()
{
    ProfileManager *profile = ProfileManager::instance();

    //Create the sinks
    musicOutput = new Phonon::AudioOutput(Phonon::MusicCategory, this);
    musicOutput->setVolume(profile->musicVolume());
    effectsOutput = new Phonon::AudioOutput(Phonon::GameCategory, this);
    effectsOutput->setVolume(profile->effectsVolume());
    voiceOutput = new Phonon::AudioOutput(Phonon::GameCategory, this);
    voiceOutput->setVolume(profile->voiceVolume());
    
    //Create the actual media players
    music = new Phonon::MediaObject(this);
    effects = new Phonon::MediaObject(this);
    voice = new Phonon::MediaObject(this);
    
    //Connect the players to the sinks
    Phonon::createPath(music, musicOutput);
    Phonon::createPath(effects, effectsOutput);
    Phonon::createPath(voice, voiceOutput);
    
    //Make the music loop
    connect(music, SIGNAL(aboutToFinish()), this, SLOT(restartPlaylist()));
    connect(music, SIGNAL(finished()), this, SLOT(playMusic()));

    connect(effects, SIGNAL(finished()), this, SIGNAL(effectFinished()));
    connect(voice, SIGNAL(finished()), this, SIGNAL(voiceFinished()));

    connect(music, SIGNAL(stateChanged(Phonon::State,Phonon::State)), this, SLOT(musicStateChanged(Phonon::State, Phonon::State)));
    connect(music, SIGNAL(currentSourceChanged(Phonon::MediaSource)), SLOT(musicInfoChanged()));
    connect(music, SIGNAL(metaDataChanged()), SLOT(musicInfoChanged()));
}

AudioManager::AudioManager(QObject* parent) : QObject(parent), voicePlayerAnimation(new QSequentialAnimationGroup(this))
{
    initializeAudioObjects();
    ProfileManager *profile = ProfileManager::instance();
    
    connect(profile, SIGNAL(profileChanged(QString)), this, SLOT(profileChanged()));
    connect(profile, SIGNAL(musicVolumeChanged(qreal)), this, SLOT(musicVolumeChanged(qreal)));
    connect(profile, SIGNAL(musicEnabledChanged(bool)), this, SLOT(musicEnabledChanged(bool)));
    connect(profile, SIGNAL(effectsVolumeChanged(qreal)), effectsOutput, SLOT(setVolume(qreal)));
    connect(profile, SIGNAL(voiceVolumeChanged(qreal)), voiceOutput, SLOT(setVolume(qreal)));

    connect(QApplication::instance(), SIGNAL(aboutToQuit()), this, SLOT(deleteLater()));

    //Set up the animations for the music fading when playing voices
    QPropertyAnimation *musicFadeOut = new QPropertyAnimation(musicOutput, "volume", voicePlayerAnimation);
    musicFadeOut->setDuration(FADE_DURATION);
    connect(musicFadeOut, SIGNAL(finished()), voice, SLOT(play()));

    QPropertyAnimation *musicFadeIn = new QPropertyAnimation(musicOutput, "volume", voicePlayerAnimation);
    musicFadeIn->setDuration(FADE_DURATION);

    voicePlayerAnimation->addAnimation(musicFadeOut);
    voicePlayerAnimation->addAnimation(new QPauseAnimation(voicePlayerAnimation));
    voicePlayerAnimation->addAnimation(musicFadeIn);
}

AudioManager::~AudioManager()
{
    qDebug() << __FILE__ << __LINE__ << "Destroying AudioManager";
    voicePlayerAnimation->stop();
    voicePlayerAnimation->deleteLater();
    stopMusic();
    theManager = NULL;
}

AudioManager *AudioManager::theManager = NULL;
#include "moc_audiomanager.cpp"
#include <QFile>
#include <QFileInfo>
