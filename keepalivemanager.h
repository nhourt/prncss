#ifndef KEEPALIVEMANAGER_H
#define KEEPALIVEMANAGER_H

#include <QtNetwork>

/**
 * This class manages the keep-alives in the Gridding-Princess network protocol.
 * 
 * Keep-alives should be sent and received every INTERVAL seconds. If after TIMEOUT seconds,
 * no keep-alive has been received, LostConnection is emitted. When keep-alives are received
 * again after a timeout has occurred, RecoveredConnection is emitted.
 */
class KeepAliveManager : public QObject
{
    Q_OBJECT
public:
    /**
     * @param peer The IP of the peer with whom to exchange keep-alives
     * @param port The port to exchange keep-alives on
     */
    explicit KeepAliveManager (QHostAddress peer, quint16 port, QObject* parent = 0);
    
    const static short TIMEOUT;
    const static short INTERVAL;
    
public slots:
    /**
     * Causes the KeepAliveManager to begin sending and receiving keep-alives
     */
    void Start();
    /**
     * Causes the KeepAliveManager to stop sending and receiving keep-alives
     */
    void Stop();
    
signals:
    /**
     * This signal will be emitted when a keep-alive has not been received in the last
     * TIMEOUT seconds.
     */
    void LostConnection();
    /**
     * This signal will be emitted when keep-alives are received again after a timeout
     * has occurred.
     */
    void RecoveredConnection();
    
private slots:
    void SendKeepAlive();
    void ReceivedDatagram();
    void Timeout();
    
private:
    QUdpSocket *udpSocket;
    QHostAddress peer;
    quint16 port;
    QTimer sendTimer;
    QTimer receiveTimer;
    
    enum ConnectionState {disconnected, connected};
    ConnectionState connectionState;
    
    const static QByteArray KEEPALIVE;
};

#endif // KEEPALIVEMANAGER_H
