#ifndef AIPLAYER_H
#define AIPLAYER_H

#include "scylla.h"
#include "player.h"
#include "profilemanager.h"

const quint8 AI_EASY = 2;
const quint8 AI_MEDIUM = 4;
const quint8 AI_HARD = 5;

class AIPlayer : public Player
{
    Q_OBJECT
public:
    AIPlayer (ProfileManager::CatfightDifficulty difficulty, QObject *parent = 0);
    virtual ~AIPlayer();
    
    virtual HomeRow ChooseStartingPositions (const Board &board);
    ///This method does nothing in AIPlayer
    virtual void InformOfStartingPositions (const HomeRow &homeRow){}
    ///This method does nothing in AIPlayer
    virtual void AcknowledgeOpponentMove (const Move& opponentMove){}
    virtual Move MakeMove (const Board &board);
    
private:
    Scylla *scylla;
    int difficulty;

    Move ToPrncssMove(const Scylla::ScyllaMove& sMove, const Board &board);

    vector<int> ToScyllaBoard(const Board &pBoard);
};

#endif // AIPLAYER_H
