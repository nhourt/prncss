#include "boardmanager.h"
#include "gameengine.h"
#include "aiplayer.h"
#include "move.h"

#include <QList>
#include <QObject>
#include <QVariant>
#include <QTextStream>
#include <QFutureWatcher>
#include <QtConcurrentRun>
#include <QParallelAnimationGroup>
#include <QSequentialAnimationGroup>

const int MOVE_TIME = 400;
const int REPLACE_TIME = 1500;

BoardManager::BoardManager(QObject* rootObject, bool noAnimations, QObject *parent)
    : QObject(parent),
      heldPiece(rootObject->findChild<QDeclarativeItem*>("heldPiece")),
      animatedPiece(rootObject->findChild<QDeclarativeItem*>("animatedPiece")),
      gameBoard(rootObject),
      needMove(false),
      gotFirstPos(false),
      needReplacement(false),
      disableAnimations(noAnimations),
      beastMode(false)
{
    QObject* obj = rootObject->findChild<QObject*>("acceptButtonFadeable");
    connect(this, SIGNAL(ShowAcceptButton()), obj, SLOT(show()));
    connect(this, SIGNAL(HideAcceptButton()), obj, SLOT(hide()));

    obj = rootObject->findChild<QObject*>("acceptButton");
    connect(obj, SIGNAL(buttonClicked()), this, SLOT(AcceptMove()));

    obj = rootObject->findChild<QObject*>("cancelButtonFadeable");
    connect(this, SIGNAL(ShowCancelButton()), obj, SLOT(show()));
    connect(this, SIGNAL(HideCancelButton()), obj, SLOT(hide()));

    obj = rootObject->findChild<QObject*>("cancelButton");
    connect(obj, SIGNAL(buttonClicked()), this, SLOT(CancelMove()));

    obj = rootObject->findChild<QObject*>(NORTH_GOAL_STRING);
    if(obj)
    {
        boardPositions.push_back(obj);
    }

    obj = rootObject->findChild<QObject*>(SOUTH_GOAL_STRING);
    if(obj)
    {
        boardPositions.push_back(obj);
    }

    obj = rootObject->findChild<QObject*>("aiSuggestMoveButton");
    connect(obj, SIGNAL(buttonClicked()), this, SLOT(AiSuggestMove()));

    QString text("");
    QTextStream stream(&text);
    for(char col = 'A'; col <= 'F'; ++col)
    {
        for(short row = 1; row <= 6; ++row)
        {
            stream << col << row;
            obj = rootObject->findChild<QObject*>(text);

            if(obj)
            {
                boardPositions.push_back(obj);
            }
            text.clear();
        }
    }
}

/* ******** SLOTS ******** */
void BoardManager::UpdateBoard(const Board &board)
{
    DisableAll();
    this->board = board;

    foreach(QObject* obj, boardPositions)
    {
        BoardPosition pos = BoardPosition(obj->property("position").toString());
        obj->setProperty("piece", QVariant((int)board[pos]));
    }
}

void BoardManager::UIPositionClicked(QString pos)
{
//    qDebug() << __FILE__ << __LINE__ << "User clicks" << pos;

    DisableAll();

    if(needMove)
    {
        if(!gotFirstPos)
        {
            firstPos = BoardPosition(pos);
            gotFirstPos = true;
            QList<Move> moves = board.FindAllMoves(firstPos);
            QList<BoardPosition> availablePos;

            foreach(Move m, moves)
            {
                availablePos.push_back(m.GetEndPosition());
            }

            foreach(QObject* obj, boardPositions)
            {
                BoardPosition p = BoardPosition(obj->property("position").toString());
                obj->setProperty("enabled", QVariant(availablePos.contains(p)));

                if(p == firstPos)
                    obj->setProperty("piece", 0);
            }

            heldPiece->setProperty("piece", board[firstPos]);

            emit ShowCancelButton();
        }
        else if(!needReplacement)
        {
            secondPos = BoardPosition(pos);
            heldPiece->setProperty("piece", board[secondPos]);
            QList<Move> moves = board.FindMoves(firstPos, secondPos);
            if(!moves.empty())
            {
                move = moves[0];

                if(board[secondPos] == NoPiece)
                {
                    DisableAll();

                    Board tempBoard = board;
                    tempBoard.ApplyMove(side, move);
                    UpdateBoard(tempBoard);

                    emit ShowAcceptButton();
                }

                else
                {
                    // we moved to a spot with a piece
                    needReplacement = true;
                    short backRow = board.FindHomeRow(side == NorthSide ? SouthSide : NorthSide);

                    foreach(QObject* obj, boardPositions)
                    {
                        BoardPosition p = BoardPosition(obj->property("position").toString());
                        bool enabled = false;
                        if(p == firstPos && board[firstPos] != board[secondPos])
                            enabled = true;
                        else if(p.InGoal())
                            enabled = false;
                        else
                        {
                            if(board[p] == NoPiece)
                            {
                                if(side == NorthSide) // North is playing
                                    enabled = p.GetRow() >= backRow;
                                else // South is playing
                                    enabled = p.GetRow() <= backRow;
                            }
                        }

                        obj->setProperty("enabled", enabled);

                        if(p == secondPos)
                            obj->setProperty("piece", board[firstPos]);
                    }
                }
            }
        }
        else if(needReplacement)
        {
            replacement = BoardPosition(pos);            
            heldPiece->setProperty("piece", 0);
            move.AddReplacement(replacement);

            board.ApplyMove(side, move);
            UpdateBoard(board);

            emit ShowAcceptButton();
            DisableAll();
        }
    }
}

void BoardManager::AcceptMove()
{
    emit HideCancelButton();
    emit HideAcceptButton();
    heldPiece->setProperty("piece", 0);
    needMove = gotFirstPos = needReplacement = false;
    emit MoveMade(move);
    ShowCurrentTurn(side == NorthSide ? SouthSide : NorthSide);
}

void BoardManager::AiSuggestMove()
{
    if (!needMove)
        return;

    DisableAll();
    if (gotFirstPos)
        CancelMove(false);

    Move aiMove = GetAiMove(board, side);
    AnimateMove(aiMove);
    board.ApplyMove(side, aiMove);
    UpdateBoard(board);

    gotFirstPos = true;
    move = aiMove;
    emit ShowAcceptButton();
    emit ShowCancelButton();
}

void BoardManager::ShowCurrentTurn(PlayerSide side)
{
    if(side == NorthSide)
        gameBoard->setProperty("side", "North");
    else if(side == SouthSide)
        gameBoard->setProperty("side", "South");
}

void BoardManager::CancelMove(bool getMove)
{
    if(needMove)
    {
        board = GameEngine::GetEngine()->GetBoard();
        gotFirstPos = needReplacement = false;
        emit HideCancelButton();
        emit HideAcceptButton();
        heldPiece->setProperty("piece", 0);
        gotFirstPos = needReplacement = false;
        UpdateBoard(board);

        if (getMove)
            GetMove(board, side);
    }
}

void BoardManager::ChooseStart(PlayerSide side)
{
    DisableAll();
    ShowCurrentTurn(side);
    emit UIChooseStart(QVariant(side == NorthSide ? "North" : "South"));
}

void BoardManager::EnableSide(const Board &board, PlayerSide side)
{
    DisableAll();
    short backRow = board.FindHomeRow(side);

    foreach(QObject* obj, boardPositions)
    {
        BoardPosition p = BoardPosition(obj->property("position").toString());
        bool enabled = false;

        if(p.InGoal())
            enabled = false;
        else
        {
            if(board[p] != NoPiece)
            {
                if(side == NorthSide)
                    enabled = p.GetRow() >= backRow;
                else
                    enabled = p.GetRow() <= backRow;
            }
        }

        obj->setProperty("enabled", enabled);
    }
}

QPropertyAnimation * BoardManager::GeneratePartialAnimation(BoardPosition previousPosition, BoardPosition currentPosition)
{
    QPropertyAnimation *partialAnimation = new QPropertyAnimation(animatedPiece, "pos");
    QVariant coordinates;
    QMetaObject::invokeMethod(gameBoard, "getCoordinatesByName",
                              Q_RETURN_ARG(QVariant, coordinates), Q_ARG(QVariant, QString(previousPosition)));
    partialAnimation->setStartValue(coordinates);
    QMetaObject::invokeMethod(gameBoard, "getCoordinatesByName",
                              Q_RETURN_ARG(QVariant, coordinates), Q_ARG(QVariant, QString(currentPosition)));
    partialAnimation->setEndValue(coordinates);
    partialAnimation->setDuration(MOVE_TIME);
    partialAnimation->setEasingCurve(QEasingCurve::InOutQuad);

    return partialAnimation;
}

void BoardManager::AnimateMove(Move move)
{
    if (disableAnimations) {
        emit AnimationEnds();
        return;
    }

    if (gotFirstPos)
        CancelMove(false);
    DisableAll();
    move.ResetGetNextDirection();

    QSequentialAnimationGroup *fullAnimation = new QSequentialAnimationGroup(this);
    BoardPosition previousPosition = move.GetStartPosition();
    BoardPosition currentPosition = previousPosition.PeekDirection(move.GetNextDirection());

    QPropertyAnimation *partialAnimation;
    while (move.GetNextDirectionCount() < move.GetDirectionCount()) {
        partialAnimation = GeneratePartialAnimation(previousPosition, currentPosition);
        partialAnimation->setParent(fullAnimation);
        fullAnimation->addAnimation(partialAnimation);

        previousPosition = currentPosition;
        currentPosition.MovePosition(move.GetNextDirection());
    }
    partialAnimation = GeneratePartialAnimation(previousPosition, currentPosition);
    partialAnimation->setParent(fullAnimation);
    fullAnimation->addAnimation(partialAnimation);

    Q_ASSERT(currentPosition == move.GetEndPosition());

    animatedPiece->setProperty("piece", board[move.GetStartPosition()]);
    gameBoard->findChild<QDeclarativeItem*>(QString(move.GetStartPosition()))->setProperty("piece", 0);

    QEventLoop el;
    connect(fullAnimation, SIGNAL(finished()), &el, SLOT(quit()));
    fullAnimation->start(QAbstractAnimation::DeleteWhenStopped);
    el.exec();

    BoardPosition finalRestingPlace;
    short finalRestingPiece;
    if (move.IsReplacement()) {
        finalRestingPlace = move.GetReplacePosition();
        finalRestingPiece = board[currentPosition];
        QDeclarativeItem *replacedAnimatedPiece = gameBoard->findChild<QDeclarativeItem*>("animatedReplacedPiece");
        QDeclarativeItem *replacedStaticPiece = gameBoard->findChild<QDeclarativeItem*>(QString(currentPosition));

        QParallelAnimationGroup *shiftAnimation = new QParallelAnimationGroup();
        connect(this, SIGNAL(AnimationEnds()), shiftAnimation, SLOT(deleteLater()));
        QPropertyAnimation *animatedPieceShift = new QPropertyAnimation(animatedPiece, "pos");
        connect(this, SIGNAL(AnimationEnds()), animatedPieceShift, SLOT(deleteLater()));
        QPointF endPoint = animatedPiece->pos();
        QPointF shiftPoint = endPoint;
        shiftPoint.rx() -= animatedPiece->width() / 2;
        shiftPoint.ry() -= animatedPiece->height() / 2;
        shiftAnimation->addAnimation(animatedPieceShift);

        animatedPieceShift->setStartValue(endPoint);
        animatedPieceShift->setEndValue(shiftPoint);
        animatedPieceShift->setDuration(REPLACE_TIME / 2);
        animatedPieceShift->setEasingCurve(QEasingCurve::InOutQuad);

        QPropertyAnimation *animatedPieceScale = new QPropertyAnimation(animatedPiece, "scale");
        connect(this, SIGNAL(AnimationEnds()), animatedPieceScale, SLOT(deleteLater()));
        animatedPieceScale->setStartValue(1.0);
        animatedPieceScale->setEndValue(1.5);
        animatedPieceScale->setDuration(REPLACE_TIME / 2);
        animatedPieceScale->setEasingCurve(QEasingCurve::InOutQuad);
        shiftAnimation->addAnimation(animatedPieceScale);

        connect(shiftAnimation, SIGNAL(finished()), &el, SLOT(quit()));
        shiftAnimation->start();
        el.exec();

        QParallelAnimationGroup *replacementAnimation = new QParallelAnimationGroup();
        connect(this, SIGNAL(AnimationEnds()), replacementAnimation, SLOT(deleteLater()));
        animatedPieceShift->setStartValue(shiftPoint);
        animatedPieceShift->setEndValue(endPoint);
        replacementAnimation->addAnimation(animatedPieceShift);

        animatedPieceScale->setStartValue(1.5);
        animatedPieceScale->setEndValue(1.0);
        replacementAnimation->addAnimation(animatedPieceScale);

        QPropertyAnimation *replaceAnimation = GeneratePartialAnimation(currentPosition, finalRestingPlace);
        connect(this, SIGNAL(AnimationEnds()), replaceAnimation, SLOT(deleteLater()));
        replaceAnimation->setTargetObject(replacedAnimatedPiece);
        replaceAnimation->setEasingCurve(QEasingCurve::OutBack);
        replaceAnimation->setDuration(REPLACE_TIME);
        replacementAnimation->addAnimation(replaceAnimation);

        replacedAnimatedPiece->setPos(animatedPiece->pos());
        replacedAnimatedPiece->setProperty("piece", finalRestingPiece);
        replacedStaticPiece->setProperty("piece", 0);

        connect(replacementAnimation, SIGNAL(finished()), &el, SLOT(quit()));
        replacementAnimation->start(QAbstractAnimation::DeleteWhenStopped);
        el.exec();

        gameBoard->findChild<QDeclarativeItem*>(QString(finalRestingPlace))->setProperty("piece", finalRestingPiece);
        replacedAnimatedPiece->setProperty("piece", 0);
        replacedStaticPiece->setProperty("piece", board[move.GetStartPosition()]);
        animatedPiece->setProperty("piece", 0);

        replacedAnimatedPiece->setPos(-replacedAnimatedPiece->width(),-replacedAnimatedPiece->height());
    } else {
        finalRestingPlace = currentPosition;
        finalRestingPiece = board[move.GetStartPosition()];

        gameBoard->findChild<QDeclarativeItem*>(QString(currentPosition))->setProperty("piece", finalRestingPiece);
        animatedPiece->setProperty("piece", 0);

        qDebug() << __FILE__ << __LINE__ << "Animated move of" << finalRestingPiece;
    }

    animatedPiece->setPos(-animatedPiece->width(), -animatedPiece->height());
    emit AnimationEnds();
}

void BoardManager::BeastMode()
{
    beastMode = true;

    if (needMove == true) {
        emit HideCancelButton();
        emit HideAcceptButton();
        AiSuggestMove();
        AcceptMove();
    }
}

Move BoardManager::GetAiMove(Board moveBoard, PlayerSide aiSide)
{
    ShowCurrentTurn(aiSide);
    QEventLoop el;
    AIPlayer ai(ProfileManager::MediumCat);
    ai.SetSide(aiSide);
    QFutureWatcher<Move> aiWatcher;
    connect(&aiWatcher, SIGNAL(finished()), &el, SLOT(quit()));
    QFuture<Move> aiMoveFuture = QtConcurrent::run(&ai, &AIPlayer::MakeMove, moveBoard);
    aiWatcher.setFuture(aiMoveFuture);
    el.exec();
    Move aiMove = aiMoveFuture.result();

    return aiMove;
}

void BoardManager::DoIDie()
{
    const QString name("Ghost of Sweet Sixteens Past");

    if (!needMove) {
        SendChat("It's not your turn, lulz.", name);
        return;
    }
    if (gameBoard->findChild<QObject*>("acceptButtonFadeable")->property("state").toString() == "hidden") {
        SendChat("You haven't selected a move yet; how am I supposed to know if it kills you?", name);
        return;
    }

    Board moveBoard = GameEngine::GetEngine()->GetBoard();
    moveBoard.ApplyMove(side, move);
    Move aiMove = GetAiMove(moveBoard, (side == NorthSide)? SouthSide : NorthSide);

    if (aiMove.GetEndPosition() == BoardPosition(NorthSide) || aiMove.GetEndPosition() == BoardPosition(SouthSide))
        SendChat("Yep. If you make that move, you gonna die.", name);
    else
        SendChat("No, that move looks pretty safe to me.", name);
}

void BoardManager::GetMove(const Board &board, PlayerSide side)
{
    ShowCurrentTurn(side);
    DisableAll();
    emit HideCancelButton();
    emit HideAcceptButton();
    this->board = board;
    this->side = side;
    short row = board.FindHomeRow(side);

    needMove = true;
    gotFirstPos = needReplacement = false;

    if (beastMode) {
        AiSuggestMove();
        AcceptMove();
        return;
    }

    QMetaObject::invokeMethod(gameBoard, "hideNetworkWait");

    foreach(QObject* obj, boardPositions)
    {
        QString posS = obj->property("position").toString();
        BoardPosition pos(posS);
        if(obj->property("row").toInt() == row && !board.FindAllMoves(pos).empty() && obj->property("piece").toInt() > 0)
        {
            obj->setProperty("enabled", QVariant(true));
        }
        else
            obj->setProperty("enabled", QVariant(false));
    }
}

void BoardManager::UIGotStart(QString row)
{
    emit StartChosen(HomeRow(row)); // goes to HumanPlayer StartChosen
}

void BoardManager::DisableAll()
{
    foreach(QObject* obj, boardPositions)
        obj->setProperty("enabled", false);
}
