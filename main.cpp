#include "prncssmainwindow.h"
#include "profilemanager.h"
#include "audiomanager.h"
#include "gameengine.h"
#include "wheelarea.h"
#include "aiplayer.h"
#include "board.h"

#include <QThread>
#include <QMainWindow>
#include <QApplication>
#include <QtDeclarative>
#include <QGraphicsObject>
#include <QDeclarativeView>
#include <QDeclarativeEngine>

const QString MUSIC_PATH("audio/music/");

QStringList allMusicFiles() {
    QStringList musics = QDir(MUSIC_PATH).entryList(QStringList("*.ogg"), QDir::Files | QDir::Readable, QDir::Name);

    for (int i = 0; i < musics.length(); ++i)
        musics[i].prepend(MUSIC_PATH);

    return musics;
}

int main (int argc, char** argv)
{
    QApplication app (argc, argv);
    app.setOrganizationName("Incendiary Projects");
    app.setApplicationName("PRNCSS");
    app.setWindowIcon(QIcon("prncss.ico"));

    qRegisterMetaType<Move>("Move");
    qRegisterMetaType<Board>("Board");
    qRegisterMetaType<Player*>("Player*");
    qRegisterMetaType<HomeRow>("HomeRow");
    qRegisterMetaType<PlayerSide>("PlayerSide");
    qRegisterMetaType<GameOverState>("GameOverState");
    qRegisterMetaType<Phonon::MediaSource>("MediaSource");

    qmlRegisterType<AudioManager>();
    qmlRegisterType<ProfileManager>();
    qmlRegisterType<WheelArea>("Wheelie", 1, 0, "WheelArea");
    qmlRegisterType<QGraphicsBlurEffect>("Effects", 1, 0, "Blur");
    qmlRegisterType<QGraphicsColorizeEffect>("Effects", 1, 0, "Colorize");
    qmlRegisterType<QGraphicsDropShadowEffect>("Effects", 1, 0, "DropShadow");

    AudioManager::instance()->setPlaylist(allMusicFiles());
    AudioManager::instance()->playMusic();

    QPalette palette;
    palette.setColor(QPalette::Base, Qt::transparent);

    QDeclarativeView* view = new QDeclarativeView(QUrl());
    view->setAttribute(Qt::WA_TranslucentBackground);
    view->setPalette(palette);

    QString error("");
    view->engine()->importPlugin("qmlparticlesplugin.dll", "Qt.labs.particles", &error);
    if (!error.isEmpty()) {
        qDebug() << __FILE__ << __LINE__ << "Failed to load particles:" << error;
    }
    view->rootContext()->setContextProperty("audioManager", AudioManager::instance());
    view->rootContext()->setContextProperty("profileManager", ProfileManager::instance());
    view->setSource(QString("qrc:/qml/main.qml"));
    view->setResizeMode(QDeclarativeView::SizeRootObjectToView);

    PRNCSSMainWindow *window = new PRNCSSMainWindow(view, &app);
    window->setStyleSheet("background:transparent;");
    window->setWindowFlags(Qt::FramelessWindowHint);
    window->setAttribute(Qt::WA_TranslucentBackground);
    window->setCentralWidget(view);
    window->showFullScreen();
    window->connect(&app, SIGNAL(aboutToQuit()), SLOT(deleteLater()));

    app.thread()->setPriority(QThread::HighestPriority);
    return app.exec();
}
