#include "screensnapper.h"

#include "QApplication"
#include "QDesktopWidget"

ScreenSnapper::ScreenSnapper()
    : QDeclarativeImageProvider(QDeclarativeImageProvider::Pixmap)
{
}

QPixmap ScreenSnapper::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
{
    *size = QApplication::desktop()->size();
    return QPixmap::grabWindow(QApplication::desktop()->winId());
}
