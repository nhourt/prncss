#ifndef PLAYER_H
#define PLAYER_H
#include "board.h"
#include "types.h"
#include "move.h"

#include <QObject>

class Player : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Player ctor
     * A player must have a side to be created.
     * @param side The side for this player; either North or South
     */
    Player (QObject *parent = 0) : QObject(parent) {}
    virtual ~Player() {qDebug() << __FILE__ << __LINE__ << "A player is destroyed.";}

    /**
     * @brief Tell this player what side he is on
     * @param side The side of the game this player is on
     * You should call this method first!
     */
    virtual void SetSide (PlayerSide side) {this->side = side;}

    /**
     * @brief Choose the initial pieces to populate the home row on the given board
     * The board may be empty, or may already have the opponent's home row set
     * @param board The board to select a home row on
     * @return The home row this player selected
     */
    virtual HomeRow ChooseStartingPositions (const Board& board) = 0;
    /**
     * @brief Inform the player of his opponent's choice of home row
     */
    virtual void InformOfStartingPositions (const HomeRow &homeRow) = 0;
    
    /**
     * @brief Take note of the opponent's move
     * This method should be called immediately after the opponent makes a move, with that move.
     * It allows the player to take note of his opponent's move before making one of his own.
     * @param opponentMove The move the opponent just made
     */
    virtual void AcknowledgeOpponentMove (const Move &opponentMove) = 0;

    /**
     * @brief Make a move on the given board
     * @param board The board to make a move on
     * @return The move this player selected
     */
    virtual Move MakeMove (const Board& board) = 0;

    /**
     * @brief Returns the side of the board this player is on
     * @return The player's side
     */
    PlayerSide GetSide();

protected:
    virtual void AbortGame();
    PlayerSide side;
};

#endif // PLAYER_H
