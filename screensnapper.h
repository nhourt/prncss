#ifndef SCREENSNAPPER_H
#define SCREENSNAPPER_H

#include <QDeclarativeImageProvider>

class ScreenSnapper : public QDeclarativeImageProvider
{
public:
    ScreenSnapper();

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);
};

#endif // SCREENSNAPPER_H
