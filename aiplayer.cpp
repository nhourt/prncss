#include "aiplayer.h"
#include <QTime>
#include <QThread>

AIPlayer::AIPlayer (ProfileManager::CatfightDifficulty difficulty, QObject* parent) : Player(parent)
{
    switch (difficulty) {
    case ProfileManager::EasyCat:
        this->difficulty = AI_EASY;
        break;
    case ProfileManager::MediumCat:
        this->difficulty = AI_MEDIUM;
        break;
    case ProfileManager::HardCat:
        this->difficulty = AI_HARD;
        break;
    }

    scylla = new Scylla();
}

AIPlayer::~AIPlayer()
{
    delete scylla;
}

HomeRow AIPlayer::ChooseStartingPositions (const Board& board)
{
    int index = 0;
    QStringList row;
    row << QString("321231") << QString("213312") << QString("123321") << QString("323121") << QString("332121") << QString("213321");

    if (difficulty == AI_EASY)
        std::random_shuffle(row[0].begin(), row[0].end());
    else
    {
        QTime midnight(0, 0, 0);
        qsrand(midnight.secsTo(QTime::currentTime()));
        index = qrand() % row.length();
    }

    return row[index];
}

Move AIPlayer::MakeMove (const Board &board)
{
    if (thread()->priority() != QThread::LowPriority)
        thread()->setPriority(QThread::LowPriority);

    Scylla::ScyllaMove move;
    QTime timer;
    timer.start();
    
    if (difficulty == AI_HARD) {
        Scylla *outerScylla = new Scylla(scylla);
        move = outerScylla->chooseMove(ToScyllaBoard(board), side == SouthSide, AI_HARD);
        delete outerScylla;
    } else {
        move = scylla->chooseMove(ToScyllaBoard(board), side == SouthSide, difficulty);
    }
    
    qDebug() << "Scylla finds move with" << move.note << "note in" << timer.elapsed() << "msecs";
    return ToPrncssMove(move, board);
}

Move AIPlayer::ToPrncssMove(const Scylla::ScyllaMove &sMove, const Board &board)
{
    Move pMove = board.FindMoves(sMove.x1, sMove.x2).first();

    if (sMove.x3 > 0)
        pMove.AddReplacement(sMove.x3);

    return pMove;
}

vector<int> AIPlayer::ToScyllaBoard(const Board &pBoard)
{
    std::vector<int> sBoard(67, 0);

    for (BoardPosition bp('A', 1); bp.GetRow() <= 6; bp.MovePosition(Up, true)) {
        for (; bp.GetColumn() <= 'F'; bp.MovePosition(Right, true))
            sBoard[bp] = pBoard[bp];
        bp.SetColumn('A');
    }

    return sBoard;
}

#include "moc_aiplayer.cpp"
#include "profilemanager.h"
