#ifndef MOVE_H
#define MOVE_H
#include "types.h"
#include <list>

class Move : public QObject
{
    Q_OBJECT
public:
    /**
     * An exception class which indicates that GetNextDirection was called when no directions remained
     */
    class OutOfDirectionsException : public PRNCSSException
    {
    public:
        OutOfDirectionsException (const char* reason) : PRNCSSException (reason) {}
    };

    Move() : pathPos (path.begin()),
        pathDepth (0),
        replace (false) {}
    
    Move(BoardPosition startPos) : startPos(startPos), pathPos(path.begin()), pathDepth(0), replace(false) {}

    Move(const Move& other);

    Move& operator =(const Move& other);
    bool operator ==(const Move& other) const;

    operator QString();

    /**
     * @brief Set the position of the piece that is moved
     * @param startPosition The row and column of the piece to move
     */
    void SetStartPosition (BoardPosition startPosition);
    /**
     * @brief Get the position of the piece that is moved
     * @return A std::pair containing the row and column of the piece to move
     */
    BoardPosition GetStartPosition() const;
    /**
     * @brief Get the ending position of the move
     * This method does not care if the move is a replacement; it will only ever return the last board position
     * that the directions take it to.
     * @return The position that the moved piece landed on
     */
    BoardPosition GetEndPosition () const;
    /**
     * @brief Add a direction to move piece in
     * @param d The direction to move
     */
    void AddDirection (MoveDirection d);
    /**
     * @brief Delete last direction in this move
     * Note that calling this method results in resetting the GetNextDirection position to the beginning of the move
     */
    void DeleteLastDirection();
    /**
     * @brief Get the next direction in this move
     * With each call, this method returns the next direction that a piece was moved. To start again at the beginning of the move, use the ResetGetNextDirection() method.
     * @throws OutOfDirectionsException When there are no more directions in this move
     */
    MoveDirection GetNextDirection() const;
    /**
     * @brief Reset direction retrieval to beginning of move
     */
    void ResetGetNextDirection() const;
    /**
     * @brief Gets the number of directions that have been read since the last reset
     */
    short GetNextDirectionCount() const;
    /**
     * @brief Get the number of directions in this move
     */
    int GetDirectionCount() const;
    /**
     * @brief Add a replacement to the end of this move
     * @param replaceTo The row and column on the board to move the replaced piece to
     */
    void AddReplacement (BoardPosition replaceTo);
    /**
     * @brief Remove any replacement this move may have
     */
    void DeleteReplacement();
    /**
     * @brief Determine whether this move has a replacement or not
     * @return True if move involves a replacement
     */
    bool IsReplacement() const;
    /**
     * @brief Get the location a move is replaced to
     * Only use this if IsReplacement returns true
     * @return The row and column a replaced piece is moved to
     */
    BoardPosition GetReplacePosition() const;
    
    /**
     * @brief Serialize the move to the network standard notation
     * @return A string containing the serialized move
     */
    QString Serialize() const;

    /**
     * @brief Deserialize the move from a string of the network standard notation
     * @return A Move
     */
    static Move Deserialize(QString serialMove);
    
    /**
     * @brief Rotate this move 180 degrees, so that it would be the exact same move on a rotated board
     */
    void Rotate();
    
    /**
     * @brief Delete everything in this move
     */
    void Clear();
    /**
     * @brief Remove all directions from this move
     */
    void DeleteAllDirections();

private:
    BoardPosition startPos;
    QList<MoveDirection> path;
    mutable QList<MoveDirection>::const_iterator pathPos;
    mutable short pathDepth;
    bool replace;
    BoardPosition repPos;
};

QDebug operator<< (QDebug dbg, const Move& m);

#endif // MOVE_H
