#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H

#include <QString>
#include <QObject>
#include <phonon/AudioOutput>
#include <phonon/MediaObject>
#include <QSequentialAnimationGroup>

const QString CHATSOUND = "audio/pop.wav";
const QString NETWORKSOUND = "audio/network event.wav";

class AudioManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool paused READ paused NOTIFY pauseChanged)
    Q_PROPERTY(QString musicInfo READ musicInfo NOTIFY musicChanged)

public:
    static AudioManager *instance();
    
public slots:
    void setMusic(const QString &file);
    void addNextMusic(const QString &file);
    void setPlaylist(const QStringList &files);
    
    void playMusic();
    void playPauseMusic();
    void stopMusic(int fadeOutTime = 1000);
    void nextMusic();
    void previousMusic();
    QString musicInfo();
    
    qint64 playEffect(const QString &file);
    qint64 playVoice(const QString &file);
    void stopVoice();

    bool paused();

signals:
    void effectFinished();
    void voiceFinished();
    void pauseChanged(bool);
    void musicChanged(QString musicInfo);
    
private slots:
    void profileChanged();
    void musicEnabledChanged(bool enabled);
    void musicVolumeChanged(qreal volume);
    void restartPlaylist();
    void musicStateChanged(Phonon::State newState, Phonon::State oldState);
    void musicInfoChanged();
    
private:
    static AudioManager *theManager;
    
    Phonon::AudioOutput *musicOutput;
    Phonon::AudioOutput *effectsOutput;
    Phonon::AudioOutput *voiceOutput;
    Phonon::MediaObject *music;
    Phonon::MediaObject *effects;
    Phonon::MediaObject *voice;

    QSequentialAnimationGroup *voicePlayerAnimation;
    
    QList<Phonon::MediaSource> playList;
    
    void initializeAudioObjects();
    AudioManager(QObject* parent = 0);
    virtual ~AudioManager();
};

#endif // AUDIOMANAGER_H
