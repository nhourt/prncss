#include "networkmanager.h"
#include "audiomanager.h"

#include <QMessageBox>
#include <QEventLoop>

const char NetworkManager::GAME_PREFIX = 'G';
const char NetworkManager::CHAT_PREFIX = 'C';

NetworkManager::NetworkManager (QString name, QObject* parent) : QObject(parent), name(name),
    udpSocket(NULL), gameSocket(new QTcpSocket(this)),
    gameServer(new QTcpServer(this)), keepAliveManager(NULL)
{}

NetworkManager::~NetworkManager()
{
    LeaveRoom();
    gameSocket->disconnectFromHost();

    qDebug() << __FILE__ << __LINE__ << "Destroying NetworkManager";
}

void NetworkManager::EnterRoom()
{
    qDebug() << __FILE__ << __LINE__ << "Entering room";

    namesToAddresses.clear();
    emit PeersUpdated(PeersFound());

    QByteArray datagram;
    datagram.append(WHO);
    datagram.append(name);
    
    if (udpSocket != NULL) {
        udpSocket->deleteLater();
        QCoreApplication::processEvents();
    }
    udpSocket = new QUdpSocket(this);

    connect(udpSocket, SIGNAL(readyRead()), this, SLOT(DataGramReady()));

    udpSocket->bind(UDPPORT, QUdpSocket::ShareAddress);
    udpSocket->writeDatagram(datagram, datagram.length(), QHostAddress::Broadcast, UDPPORT);
}

void NetworkManager::SendPlayRequest(const QString &opponent){
    QByteArray datagram;

    qDebug() << __FILE__ << __LINE__ << "Sending play request to " << opponent;

    datagram.append(PLAY);
    datagram.append(name);

    this->opponentName = opponent;
    this->opponentAddress = namesToAddresses[opponent];
    udpSocket->writeDatagram(datagram, datagram.length(), opponentAddress, UDPPORT);
}

void NetworkManager::RespondToPlayRequest(bool acceptance)
{
    QByteArray datagram;

    if (acceptance)
    {
        qDebug() << __FILE__ << __LINE__ << "Saying yes to" << opponentName;
        datagram.append(YES);
    }
    else
    {
        qDebug() << __FILE__ << __LINE__ << "Saying no to" << opponentName;
        datagram.append(NO);
    }

    udpSocket->writeDatagram(datagram, datagram.length(), opponentAddress, UDPPORT);
}

void NetworkManager::SendGameMessage (const QString& message)
{
    QByteArray messageBlock;
    messageBlock.append(GAME_PREFIX);
    messageBlock.append(message);
    messageBlock.append('\n');
    
    gameSocket->write (messageBlock);
    gameSocket->flush();
}

void NetworkManager::resign()
{
    gameMessageQueue.enqueue(QString::null);
    gameMessages.release();
}

void NetworkManager::SendChatMessage (const QString& message)
{
    QByteArray messageBlock;
    messageBlock.append(CHAT_PREFIX);
    messageBlock.append(message);
    messageBlock.append('\n');
    
    gameSocket->write(messageBlock);
    gameSocket->flush();
}

QStringList NetworkManager::PeersFound()
{
    return namesToAddresses.keys();
}

QString NetworkManager::GetGameMessage()
{
    gameMessages.acquire();
    return gameMessageQueue.dequeue();
}

void NetworkManager::ConnectToServer (QHostAddress serverIP)
{
    qDebug() << __FILE__ << __LINE__ << "Connecting to server" << serverIP;

    gameSocket->connectToHost(serverIP, GAMEPORT);
    gameSocket->waitForConnected();
    
    ConnectSocketSignals();
    StartKeepAliveManager();
}

void NetworkManager::ListenForClients()
{
    qDebug() << __FILE__ << __LINE__ << "Listening for clients";
    
    gameServer->listen(QHostAddress::Any, GAMEPORT);
}

void NetworkManager::WaitForConnection()
{
    qDebug() << __FILE__ << __LINE__ << "Waiting for client to connect";

    if (!gameServer->hasPendingConnections())
        gameServer->waitForNewConnection(-1);
    gameSocket = gameServer->nextPendingConnection();
    
    gameServer->close();
    
    ConnectSocketSignals();
    StartKeepAliveManager();
}

void NetworkManager::LostConnection()
{
    emit ConnectionDropped();
    AudioManager::instance()->playEffect(NETWORKSOUND);
    qDebug() << __FILE__ << __LINE__ << "Connection has dropped";
}

void NetworkManager::RecoveredConnection()
{
    gameSocket->write(RAMROD);
    AudioManager::instance()->playEffect(NETWORKSOUND);
    qDebug() << __FILE__ << __LINE__ << "Keep-alives return";
}

void NetworkManager::TCPError(QAbstractSocket::SocketError error)
{
    qDebug() << "TCP error:" << error;
    gameSocket->close();
    qDebug() << "Game socket state:" << gameSocket->state();

    resign();

    keepAliveManager->Stop();

    emit ConnectionLost();
}

void NetworkManager::DataGramReady()
{
    while (udpSocket->hasPendingDatagrams()) {
        QHostAddress sender;
        QByteArray dgram(udpSocket->pendingDatagramSize(), ' ');
        quint16 port;
        
        udpSocket->readDatagram(dgram.data(), dgram.size(), &sender, &port);
        
        if (dgram.indexOf(ME) == 0) {
            dgram.remove(0, ME.length());
            
            qDebug() << __FILE__ << __LINE__ << "Got peer name and address:" << dgram << sender;
            
            AddPeer(dgram, sender);
        }
        else if (dgram.indexOf(WHO) == 0){
            dgram.remove(0, WHO.length());

            //Ignore a WHO from myself
            if (dgram == name)
                continue;

            qDebug() << __FILE__ << __LINE__ << "Got WHO from" << dgram << ", sending ME...";
            AddPeer(dgram, sender);

            dgram.clear();
            dgram.append(ME);
            dgram.append(name);

            udpSocket->writeDatagram(dgram, dgram.size(), sender, port);
        }
        else if (dgram.indexOf(GONE) == 0){
            dgram.remove(0, GONE.length());

            qDebug() << __FILE__ << __LINE__ << "Peer left:" << dgram;
            namesToAddresses.remove(dgram);
            emit PeersUpdated(QVariant::fromValue(PeersFound()));
        }
        else if (dgram.indexOf(PLAY) == 0)
        {
            dgram.remove(0, PLAY.length());

            qDebug() << __FILE__ << __LINE__ << "Play request received from" << dgram;

            opponentName = dgram;
            opponentAddress = sender;
            emit PlayRequest(QVariant::fromValue(dgram));
        }
        else if (dgram.indexOf(YES) == 0)
        {
            qDebug() << __FILE__ << __LINE__ << opponentName << "accepted play request.";
            emit ChooseSide();
        }
        else if (dgram.indexOf(NO) == 0)
        {
            qDebug() << __FILE__ << __LINE__ << opponentName << "rejected play request.";
            emit PlayRequestRejected();
        }
        else if (dgram.indexOf(OK) == 0)
        {
            PlayerSide side;
            dgram.remove(0, OK.length());

            if (dgram.indexOf(NORTH) == 0)
                side = SouthSide;
            else if (dgram.indexOf(SOUTH) == 0)
                side = NorthSide;

            LeaveRoom();
            ConnectToServer(opponentAddress);

            qDebug() << __FILE__ << __LINE__ << "Client emits ConnectionMade(" << side << ")";
            emit ConnectionMade(side);
            
            //We're done receiving datagrams in NetworkManager
            break;
        }
    }
}

void NetworkManager::SideChosen(QString side)
{
    ListenForClients();
    QByteArray dgram;

    dgram.append(OK);
    dgram.append(side.contains("North", Qt::CaseInsensitive) ? NORTH : SOUTH);

    udpSocket->writeDatagram(dgram, dgram.size(), opponentAddress, UDPPORT);

    LeaveRoom();
    WaitForConnection();

    qDebug() << __FILE__ << __LINE__ << "Server emits ConnectionMade(" << side << ")";
    emit ConnectionMade(side.contains("North", Qt::CaseInsensitive) ? NorthSide : SouthSide);
}

void NetworkManager::AddPeer(const QString &peerName, const QHostAddress &peerAddress)
{
    namesToAddresses[peerName] = peerAddress;
    QVariant peers = QVariant::fromValue(PeersFound());
    emit PeersUpdated(QVariant::fromValue(PeersFound()));
}

void NetworkManager::ConnectSocketSignals()
{
    connect(gameSocket, SIGNAL(readyRead()), this, SLOT(GameSocketReadyRead()));
    connect(gameSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(TCPError(QAbstractSocket::SocketError)));
}

void NetworkManager::StartKeepAliveManager()
{
    keepAliveManager = new KeepAliveManager(opponentAddress, UDPPORT, this);
    
    connect(keepAliveManager, SIGNAL(LostConnection()), this, SLOT(LostConnection()));
    connect(keepAliveManager, SIGNAL(RecoveredConnection()), this, SLOT(RecoveredConnection()));
    
    QMetaObject::invokeMethod(keepAliveManager, "Start");
}

void NetworkManager::LeaveRoom() {
    if (udpSocket == NULL)
        return;

    qDebug() << __FILE__ << __LINE__ << "Leaving room";
    
    QByteArray dgram;
    dgram.append(GONE);
    dgram.append(name);
    udpSocket->writeDatagram(dgram, QHostAddress::Broadcast, UDPPORT);
    
    //We are done with this udpSocket; KeepAliveManager will take it from here.
    disconnect(udpSocket, SIGNAL(readyRead()), this, SLOT(DataGramReady()));
    udpSocket->close();
    udpSocket->deleteLater();
    udpSocket = NULL;
}

void NetworkManager::GameSocketReadyRead()
{
    QByteArray message;
    
    while (gameSocket->canReadLine()) {
        message = gameSocket->readLine(gameSocket->bytesAvailable() + 10);
        qDebug() << __FILE__ << __LINE__ << "Got game message:" << message;

        if (message == RAMROD) {
            qDebug() << __FILE__ << __LINE__ << "Connection recovered!";
            emit ConnectionRecovered();
            return;
        } else {
            switch (message[0]) {
                case GAME_PREFIX:
                    AudioManager::instance()->playEffect(NETWORKSOUND);
                    gameMessageQueue.enqueue(message.mid(1));
                    gameMessages.release();
                    break;
                case CHAT_PREFIX:
                    AudioManager::instance()->playEffect(CHATSOUND);
                    emit NewChatMessage(message.mid(1).trimmed(), opponentName);
                    break;
            }
        }
    }
}

const QString NetworkManager::WHO = "WHO-";
const QString NetworkManager::PLAY = "PLAY-";
const QString NetworkManager::ME = "ME-";
const QString NetworkManager::NO = "NO";
const QString NetworkManager::YES = "YES";
const QString NetworkManager::OK = "OK-";
const QString NetworkManager::NORTH = "N";
const QString NetworkManager::SOUTH = "S";
const QString NetworkManager::GONE = "GONE-";
const QString NetworkManager::LOST = "LOST";
const char* NetworkManager::RAMROD = "\a\n";

#include "moc_networkmanager.cpp"
#include "audiomanager.h"
